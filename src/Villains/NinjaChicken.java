package Villains;

public class NinjaChicken extends Chicken {

    public NinjaChicken(int x, int y) {
        super(x, y);
        setDexterity(25);
        setHealth(15);
    }

}
