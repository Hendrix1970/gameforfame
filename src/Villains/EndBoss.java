package Villains;

import java.util.ArrayList;
import java.util.List;

import Rooms.Spawner;
import dataStructure.FightingAttributesDts;
import framework.AbstractFightable;
import framework.Direction;
import framework.TextTile;

public class EndBoss extends AbstractFightable {

    public EndBoss(int x, int y) {
        super(x, y, Spawner.LARGE_STANDARD_SIZE, Spawner.LARGE_STANDARD_SIZE, getStats(), 10,
                10, 10, 3);
        setDirection(Direction.NORTH);
        setXpValue(15);

    }

    private static FightingAttributesDts getStats() {
        return new FightingAttributesDts(50, 16, 2);
    }

    @Override
    protected void init() {
        List<VillainSkill> skills = new ArrayList<>();
        setSkills(skills);
    }

    @Override
    public String retrieveText(TextTile textTile) {

        switch (textTile) {
        case VILLAIN_OPTION:
            return "Der Boss pupst dich an!";
        case DAMAGE_DEALT:
            return "Der Boss macht ";
        case DAMAGE_MISSED:
            if (Math.random() < 0.5) {
                return "Ich werde dich wegficken du Homofürst!";
            }
            return "Ich werde deine Mutter wegficken, du Arschprinz!";
        case END_FIGHT:
            return "Ihr habt einen Gott geschlachtet! Ihr habt gewonnen.";
        }

        return null;
    }

    @Override
    public List<VillainSkill> getSkills() {
        ArrayList<VillainSkill> skills = new ArrayList<>();
        skills.add(new Pecking(this));
        return skills;
    }

    @Override
    public void tick() {
        MovementUtils.randomMovement(this);
    }
}
