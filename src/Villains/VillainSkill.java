package Villains;

import framework.AbstractFightable;
import heroStuff.characterClasses.Hero;

public abstract class VillainSkill extends Skill {

	private AbstractFightable villain;

	public VillainSkill(AbstractFightable villain) {
		this.setVillain(villain);
	}

	public abstract void executeSkill(Hero hero);

	public AbstractFightable getVillain() {
		return villain;
	}

	public void setVillain(AbstractFightable hero) {
		this.villain = hero;
	}

}
