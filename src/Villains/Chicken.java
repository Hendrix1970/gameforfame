package Villains;

import java.util.ArrayList;
import java.util.List;

import Rooms.Spawner;
import dataStructure.FightingAttributesDts;
import framework.AbstractFightable;
import framework.Direction;
import framework.TextTile;
import quests.EnemyType;

public class Chicken extends AbstractFightable {

	public Chicken(int x, int y) {
		super(x, y, Spawner.SMALL_STANDARD_SIZE, Spawner.SMALL_STANDARD_SIZE, getStats(), 4, 2, 3, 5);
		this.enemyType = EnemyType.chicken;
		setDirection(Direction.EAST);
		setXpValue(50);
	}

	private static FightingAttributesDts getStats() {
		return new FightingAttributesDts(20, 10, 5);
		// return new FightingAttributesDts(25, 10, 5);
	}

	@Override
	public void tick() {
		MovementUtils.randomMovement(this);
	}

	@Override
	protected void init() {
		List<VillainSkill> skills = new ArrayList<>();
		setSkills(skills);

	}

	@Override
	public String retrieveText(TextTile textTile) {
		switch (textTile) {
		case VILLAIN_OPTION:
			return "Das Huhn setzt zum Angriff an und...";
		case DAMAGE_DEALT:
			return "Das Huhn macht ";
		case DAMAGE_MISSED:
			if (Math.random() < 0.5) {
				return "Kickeriki";
			}
			return "Quark quark quark!";
		case END_FIGHT:
			return "Ihr habt in einem heldenhaften geplänkel ein Huhn besiegt.";
		default:
			return null;
		}
	}

	@Override
	public List<VillainSkill> getSkills() {
		ArrayList<VillainSkill> skills = new ArrayList<>();
		skills.add(new Pecking(this));
		return skills;
	}
}
