package Villains;

import framework.AbstractMoveableGameObject;
import framework.Direction;

public class MovementUtils {

	/**
	 * 
	 * @param moveable
	 */
	public static void randomMovement(AbstractMoveableGameObject moveable) {
		Direction direction = calcDirection();
		if (moveable.getContinousMovement() == 0) {
			if (direction == Direction.EAST && isXInSquare(moveable, direction)) {
				moveable.setDirection(Direction.EAST);
			} else if (direction == Direction.WEST && isXInSquare(moveable, direction)) {
				moveable.setDirection(Direction.WEST);
			} else if (direction == Direction.NORTH && isYInSquare(moveable, direction)) {
				moveable.setDirection(Direction.NORTH);
			} else if (direction == Direction.SOUTH && isYInSquare(moveable, direction)) {
				moveable.setDirection(Direction.SOUTH);
			} else {
				moveable.setDirection(Direction.NONE);
			}
			moveable.setContinousMovement(10);
		} else {
			moveable.move();
			moveable.reduceContinousMovement();
		}
	}

	private static Direction calcDirection() {
		double rdm = Math.random();
		Direction direction;
		if (rdm < 0.2) {
			direction = Direction.EAST;
		} else if (0.2 <= rdm && rdm < 0.4) {
			direction = Direction.WEST;
		} else if (0.4 <= rdm && rdm < 0.6) {
			direction = Direction.NORTH;
		} else if (0.6 <= rdm && rdm < 0.8) {
			direction = Direction.SOUTH;
		} else {
			// in else case the mob stands still for one iteration
			direction = Direction.NONE;
		}
		return direction;
	}

	private static boolean isXInSquare(AbstractMoveableGameObject moveable, Direction direction) {
		if (!moveable.isMovementBox()) {
			return true;
		}

		boolean inX = false;
		if (moveable.getSquareX() - moveable.getMovementDistance() < moveable.getX() && direction == Direction.WEST) {
			inX = true;
		} else if (direction == Direction.EAST
				&& moveable.getX() < moveable.getSquareX() + moveable.getMovementDistance()) {
			inX = true;
		}
		return inX;
	}

	private static boolean isYInSquare(AbstractMoveableGameObject moveable, Direction direction) {
		if (!moveable.isMovementBox()) {
			return true;
		}

		boolean inY = false;
		if (moveable.getSquareY() - moveable.getMovementDistance() < moveable.getY() && direction == Direction.NORTH) {
			inY = true;
		} else if (direction == Direction.SOUTH
				&& moveable.getY() < moveable.getSquareY() + moveable.getMovementDistance()) {
			inY = true;
		}
		return inY;
	}
}
