package Villains;

public abstract class Skill {

    private String name;
    protected boolean cooldownActive = false;
    protected final double DEFAULT_HIT_PERCENTAGE = 0.80;
    protected int cooldownCounter = 0;
    protected int cooldownValue = 0;
    private int damage;
    private boolean hit;
    private boolean cooldownSkill;

    protected void initCooldown(int value) {
        this.cooldownSkill = true;
        this.cooldownValue = value;
    }

    public void updateCooldown() {
        if (cooldownCounter > 1) {
            cooldownCounter--;
        }
        else {
            cooldownActive = false;
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isCooldownActive() {
        return cooldownActive;
    }

    public void setCooldown(boolean cooldown) {
        this.cooldownActive = cooldown;
    }

    public int getDamage() {
        return damage;
    }

    public void setDamage(int damage) {
        this.damage = damage;
    }

    public boolean isHit() {
        return hit;
    }

    public void setHit(boolean hit) {
        this.hit = hit;
    }

    boolean isCooldownSkill() {
        return cooldownSkill;
    }

    public void activateCooldown() {
        if (cooldownSkill) {
            cooldownActive = true;
            cooldownCounter = cooldownValue;
        }
    }
}
