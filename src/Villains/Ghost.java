package Villains;

import java.util.ArrayList;
import java.util.List;

import Rooms.Spawner;
import dataStructure.FightingAttributesDts;
import framework.AbstractFightable;
import framework.Direction;
import framework.TextTile;

public class Ghost extends AbstractFightable {

    int visibilityCounter = 0;

    public Ghost(int x, int y) {
        super(x, y, Spawner.HUMAN_STANDARD_SIZE, Spawner.HUMAN_STANDARD_SIZE, getStats(), 10,
                10, 10, 10);
        setDirection(Direction.NORTH);
        setXpValue(15);

    }

    private static FightingAttributesDts getStats() {
        return new FightingAttributesDts(30, 10, 8);
    }

    @Override
    protected void init() {
        List<VillainSkill> skills = new ArrayList<>();
        setSkills(skills);
    }

    @Override
    public String retrieveText(TextTile textTile) {

        switch (textTile) {
        case VILLAIN_OPTION:
            return "Der Geist ummantelt dich mit kälte";
        case DAMAGE_DEALT:
            return "Der Ghost macht ";
        case DAMAGE_MISSED:
            if (Math.random() < 0.5) {
                return "Buhuhuhuhuhu alle dabei, der Geist schlägt vorbei!";
            }
            return "Nicht gewesen außer Spesen.";
        case END_FIGHT:
            return "Ihr habt einen imaginären Gegner besiegt, wer kann euch jetzt noch aufhalten?";
        default:
            break;
        }

        return null;
    }

    @Override
    public List<VillainSkill> getSkills() {
        ArrayList<VillainSkill> skills = new ArrayList<>();
        skills.add(new Pecking(this));
        return skills;
    }

    @Override
    public void tick() {
        MovementUtils.randomMovement(this);
        visibilityCounter += 1;
    }

    public boolean isVisible() {
        boolean visibil = true;
        if (visibilityCounter < 50) {
            visibil = true;
        }
        else if (visibilityCounter < 250) {
            visibil = false;
        }
        else {
            visibilityCounter = 0;
        }
        return visibil;
    }
}
