package Villains;

import framework.AbstractFightable;
import heroStuff.characterClasses.Hero;

public class Pecking extends VillainSkill {

	public Pecking(AbstractFightable villain) {
		super(villain);
		setName("Picken");
	}

	@Override
	public void executeSkill(Hero hero) {

		double hitChance = calculateHitChance(hero);

		if (Math.random() < hitChance) {
			setDamage((int) (calculateDamage(hero) * Math.random() * 2));
			if (getDamage() - hero.getDefense() > 0) {
				hero.setHealth(hero.getHealth() - getDamage());
			}
		}
	}

	private double calculateHitChance(Hero hero) {
		// hit chance is calculated from a constant part and a part relaying in
		// both the heros and the villains skill.
		return (double) DEFAULT_HIT_PERCENTAGE + (double) (getVillain().getDexterity() - hero.getDexterity()) / 100;
	}

	private double calculateDamage(Hero hero) {
		double variance = Math.random() / 2 + 0.5;
		return variance * (getVillain().getDamage() - hero.getDefense());
	}
}
