package engine;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.Window;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.PrintWriter;

import javax.swing.ImageIcon;
import javax.swing.JFrame;

public class WindowFullScreen {

	private GraphicsDevice vc;

	public WindowFullScreen(Game game) {

		GraphicsEnvironment env = GraphicsEnvironment.getLocalGraphicsEnvironment();
		vc = env.getDefaultScreenDevice();

		JFrame frame = new JFrame();
		frame.setUndecorated(true);
		frame.setResizable(false);
		frame.setBackground(Color.BLACK);
		frame.setForeground(Color.BLACK);
		frame.add(game);
		frame.setIconImage(new ImageIcon(getClass().getResource("/images/blood_splat_01.png")).getImage());
		frame.setTitle("Game For Fame");
		vc.setFullScreenWindow(frame);
		setCursorToBlank(frame);

		printResolution();
	}

	private void printResolution() {
		try {
			PrintWriter print = new PrintWriter(new File("resolution.txt"));
			print.println(getWindowWidth());
			print.println(getWindowHeight());
			print.close();
		} catch (Exception e) {
			System.out.println("No file found, doesn't matter that you just created it... seems smart.");
			e.printStackTrace();
		}
	}

	public void setCursorToBlank(JFrame frame) {
		// Transparent 16 x 16 pixel cursor image.
		BufferedImage cursorImg = new BufferedImage(16, 16, BufferedImage.TYPE_INT_ARGB);

		// Create a new blank cursor.
		Cursor blankCursor = Toolkit.getDefaultToolkit().createCustomCursor(cursorImg, new Point(0, 0), "blank cursor");

		// Set the blank cursor to the JFrame.
		frame.getContentPane().setCursor(blankCursor);
	}

	public Window getFullScreenWindow() {
		return vc.getFullScreenWindow();
	}

	public void restoreScreen() {
		Window w = getFullScreenWindow();
		if (w != null) {
			w.dispose();
		}
		vc.setFullScreenWindow(null);
	}

	public int getWindowWidth() {
		return getFullScreenWindow().getWidth();
	}

	public int getWindowHeight() {
		return getFullScreenWindow().getHeight();
	}
}
