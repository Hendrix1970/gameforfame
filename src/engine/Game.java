package engine;

/* TEST aus VS Code */

import java.awt.Canvas;
import java.awt.Graphics;
import java.awt.image.BufferStrategy;

import Rooms.Spawner;
import framework.ScreenStates;
import guiFrameWork.RenderingLevel;
import guiFrameWork.TheRenderingMachine;
import guiFrameWork.renderer.HUDRenderer;
import guiFrameWork.renderer.HeroRenderer;
import guiFrameWork.renderer.LoadingScreenRenderer;
import heroStuff.Inventory;
import heroStuff.LevelManager;
import heroStuff.characterClasses.Hero;
import heroStuff.characterClasses.Nerd;
import keys.KeyInput;

public class Game extends Canvas implements Runnable {

	private static final long serialVersionUID = 324149703534406593L;

	private LoadingScreenRenderer loadingRenderer;
	private Thread thread;
	private GameHandler handler;
	private Spawner spawner;
	private TheRenderingMachine theRenderingMachine;
	private InteractionHandler interactionHandler;
	private ScreenStateApadter screenStateApadter;

	private Hero hero;

	public Game() {
		screenStateApadter = new ScreenStateApadter();
		theRenderingMachine = new TheRenderingMachine();
		new WindowFullScreen(this);

		hero = new Nerd(450, 300);
		new Inventory(hero);
		new LevelManager(hero, screenStateApadter);

		theRenderingMachine.registerRenderer(new HeroRenderer(hero), RenderingLevel.HERO);
		theRenderingMachine.registerRenderer(new HUDRenderer(hero), RenderingLevel.HERO);

		interactionHandler = new InteractionHandler(theRenderingMachine, screenStateApadter, hero);

		handler = new GameHandler(theRenderingMachine, hero, interactionHandler, screenStateApadter);
		addKeyListener(new KeyInput(hero, screenStateApadter, theRenderingMachine, interactionHandler));

		loadingRenderer = new LoadingScreenRenderer(this, screenStateApadter);

		spawner = new Spawner(handler, theRenderingMachine, this, screenStateApadter);
		interactionHandler.setSpawner(spawner);
		spawner.spawn();

		start();
	}

	/**
	 * Startpunkt: Neues Game wird erstellt
	 */
	public static void main(String[] args) {
		new Game();
	}

	public synchronized void start() {
		thread = new Thread(this);
		thread.start();
	}

	public synchronized void stop() {
		try {
			thread.join();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void tick() {
		ScreenStates screenState = screenStateApadter.getScreenState();
		if (screenState == ScreenStates.loading) {
			loadingRenderer.tick();
		} else {
			handler.tick();
		}
	}

	private void render() {
		BufferStrategy bs = this.getBufferStrategy();
		if (bs == null) {
			this.createBufferStrategy(3);
			return;
		}

		Graphics g = bs.getDrawGraphics();

		theRenderingMachine.render(g);

		if (screenStateApadter.getScreenState() == ScreenStates.loading) {
			loadingRenderer.render(g);
		}

		g.dispose();
		bs.show();
	}

	@Override
	public void run() {

		this.requestFocus();
		long lastTime = System.nanoTime();
		double amountOfTicks = 60.0;
		double ns = 1000000000 / amountOfTicks;
		double delta = 0;
		long timer = System.currentTimeMillis();
		while (true) {
			// Rendert durchgehend und zählt so lange das delta hoch, bis eine
			// 60stel Sekunde vergangen ist.
			// Dann wird getickt und gerendert.
			// Wenn zwischen 2 Durchläufen mehr als 2 60tel Sekunden vergehen,
			// wird mehrmals getickt und erst dann wieder gerendert.
			long now = System.nanoTime();
			delta += (now - lastTime) / ns;
			lastTime = now;
			while (delta >= 1) {
				tick();
				delta--;
			}
			render();

			if (System.currentTimeMillis() - timer > 1000) {
				timer += 1000;
				// Hier passiert nichts. Bietet aber die Möglichkeit sekündlich
				// z.B. eine Konsolen ausgabe zu machen.
				// Oder die Spieldauer anzuzeigen oder so.
			}
		}
	}

	public void nextLevelIgnoringScreenState() {
		spawner.spawn();
	}
}
