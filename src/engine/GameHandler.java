package engine;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import dataStructure.RoomDts;
import framework.AbstractFightable;
import framework.AbstractGameObject;
import framework.AbstractMoveableGameObject;
import framework.Direction;
import framework.HorizontalVertical;
import framework.ScreenStates;
import guiFrameWork.FontUtils;
import guiFrameWork.RenderingLevel;
import guiFrameWork.TheRenderingMachine;
import guiFrameWork.renderer.BloodAndGutsRenderer;
import guiFrameWork.renderer.LevelUpRenderer;
import guiFrameWork.renderer.Renderer;
import heroStuff.characterClasses.Hero;
import interactiveMenues.Consumable;

public class GameHandler {

	Hero hero;

	Map<AbstractGameObject, Renderer> gameObjects = new HashMap<>();
	List<BloodAndGutsRenderer> bloodAndGuts = new LinkedList<>();
	List<AbstractMoveableGameObject> moveableObjects = new LinkedList<>();

	private InteractionHandler interactionHandler;
	private TheRenderingMachine theRenderingMachine;

	private ScreenStateApadter screenStateAdapter;

	public GameHandler(TheRenderingMachine theRenderingMachine, Hero hero, InteractionHandler interactionHandler,
			ScreenStateApadter screenState) {
		this.theRenderingMachine = theRenderingMachine;
		this.hero = hero;
		this.interactionHandler = interactionHandler;
		this.screenStateAdapter = screenState;
	}

	/**
	 * Ruft die Tick-Methode aller Listenelemente auf
	 */
	public void tick() {

		ScreenStates screenState = screenStateAdapter.getScreenState();
		removeUnusedObjects();

		if (screenState == ScreenStates.FIGHT) {
			interactionHandler.fight();
		} else if (screenState == ScreenStates.INTERACTIVE_MENU) {
			interactionHandler.checkInteractionStateAndEndInteractionIfNecessary();
		} else if (screenState == ScreenStates.dead) {
			// Wenn tot, dann erstmal garnichts machen.
		} else {
			calculateCollisionsAndInteractions();
			for (AbstractGameObject abstractGameObject : gameObjects.keySet()) {
				abstractGameObject.tick();
			}
			resetCollisions();
		}
		manageAnimations();
	}

	private void manageAnimations() {
		if (screenStateAdapter.isLevelUp()) {
			theRenderingMachine.registerRenderer(new LevelUpRenderer(hero), RenderingLevel.INTERFACES);
			screenStateAdapter.setLevelUp(false);
		}
		theRenderingMachine.clearTemporaryRenderers();
	}

	/**
	 * Bewegungs-Kollision berechnen die in eine Liste gefüllt werden, die später
	 * verarbeitet werden kann.
	 */
	private void calculateCollisionsAndInteractions() {
		interactionHandler.unregisterInteractions(null);
		for (AbstractMoveableGameObject moveable : moveableObjects) {
			for (AbstractGameObject object : gameObjects.keySet()) {
				if (moveable != object) {
					calculateCollisions(moveable, object);
					if (moveable instanceof Hero) {
						if (hero.getExtendedBounds().intersects(object.getBounds())) {
							interactionHandler.updateInteractions(hero, object);
						}
					}
				}
			}
		}
	}

	/**
	 * calculates if an object and an movable object intersect in tick-instance in
	 * either a vertical or horizontal way
	 * 
	 * @param moveable
	 * @param object
	 */
	private void calculateCollisions(AbstractMoveableGameObject moveable, AbstractGameObject object) {
		Rectangle rectangleVertical = new Rectangle(moveable.getX(), moveable.getY() + moveable.getSpeedY(),
				moveable.getWidth(), moveable.getHeight());
		Rectangle rectangleHorizontal = new Rectangle(moveable.getX() + moveable.getSpeedX(), moveable.getY(),
				moveable.getWidth(), moveable.getHeight());
		if (rectangleVertical.intersects(object.getBounds())) {
			moveable.collide(HorizontalVertical.VERTICAL);
		}
		if (rectangleHorizontal.intersects(object.getBounds())) {
			moveable.collide(HorizontalVertical.HORIZONTAL);
		}
	}

	/**
	 * Makes every collided object movable again.
	 */
	private void resetCollisions() {
		for (AbstractMoveableGameObject moveable : moveableObjects) {
			moveable.resetCollisions();
		}
	}

	/**
	 * Clears all game objects and adds objects of new room. the hero stays the
	 * same.
	 * 
	 * @param dts
	 */
	public void newRoom(RoomDts dts) {
		// Zwischenspeichern
		Renderer heroRenderer = gameObjects.get(hero);

		// New Background
		theRenderingMachine.changeBackground(dts.getBackgroundRenderer());

		// Blood and Guts
		for (BloodAndGutsRenderer blood : bloodAndGuts) {
			theRenderingMachine.unregisterRenderer(blood, RenderingLevel.BLOOD_AND_GUTS);
		}

		bloodAndGuts = dts.getBoodAndGuts();

		for (BloodAndGutsRenderer blood : bloodAndGuts) {
			theRenderingMachine.registerRenderer(blood, RenderingLevel.BLOOD_AND_GUTS);
		}

		// Game objects
		for (AbstractGameObject abstractGameObject : gameObjects.keySet()) {
			// theRenderingMachine.unregisterRenderer(gameObjects.get(abstractGameObject),
			// RenderingLevel.GAME_OBJECTS);
			Renderer renderer = gameObjects.get(abstractGameObject);
			if (renderer != null) {
				theRenderingMachine.unregisterRenderer(renderer,
						renderer.getRenderingLevel() != null ? renderer.getRenderingLevel()
								: RenderingLevel.GAME_OBJECTS);
			}
		}

		gameObjects = dts.getObjects();

		for (AbstractGameObject abstractGameObject : gameObjects.keySet()) {
			Renderer renderer = gameObjects.get(abstractGameObject);
			if (renderer != null) {
				theRenderingMachine.registerRenderer(renderer,
						renderer.getRenderingLevel() != null ? renderer.getRenderingLevel()
								: RenderingLevel.GAME_OBJECTS);
			}
		}

		// Add hero
		gameObjects.put(hero, heroRenderer);

		// Handle moveable objects
		moveableObjects.clear();
		for (AbstractGameObject obj : gameObjects.keySet()) {
			if (obj instanceof AbstractMoveableGameObject) {
				moveableObjects.add((AbstractMoveableGameObject) obj);
			}
		}
	}

	public void setHeroKoordinates(int x, int y) {
		hero.setX(x);
		hero.setY(y);
		hero.setDirection(Direction.NONE);
	}

	/**
	 * Überprüft den Zustand aller Objekte und löscht alles was nicht mehr auf dem
	 * Bildschirm existieren soll.
	 */
	private void removeUnusedObjects() {
		AbstractGameObject objectToRemove = null;
		for (AbstractGameObject object : gameObjects.keySet()) {
			if (object instanceof AbstractFightable) {
				if (((AbstractFightable) object).isAlive() == false) {
					objectToRemove = object;
					updateRendererAfterKill(object);
				}
			} else if (object instanceof Consumable) {
				if (((Consumable) object).isOnMap() == false) {
					objectToRemove = object;
				}
			}
		}
		if (hero.isAlive() == false) {
			gameOver();
		}
		remove(objectToRemove);
	}

	private void gameOver() {
		screenStateAdapter.setScreenState(ScreenStates.dead);
		theRenderingMachine.registerRenderer(new Renderer() {

			@Override
			public void renderAll(Graphics g) {
				g.setFont(FontUtils.getStandardFontInSize(100));
				g.setColor(Color.red);
				g.drawString("Ihr seid gestorben!", 600, 350);

			}

			@Override
			public RenderingLevel getRenderingLevel() {
				return RenderingLevel.INTERFACES;
			}

			@Override
			public Object getRenderedObject() {
				return null;
			}
		});
	}

	/**
	 * Bereinigt getötete Gegner und hinterlässt eine Blutlache
	 *
	 * @param der Gegner der zu töten ist
	 */
	private void updateRendererAfterKill(AbstractGameObject object) {
		// render a nice bloody puddle.
		BloodAndGutsRenderer blood = new BloodAndGutsRenderer(object.getX(), object.getY());
		bloodAndGuts.add(blood);

		// stop rendering the origin object
		theRenderingMachine.unregisterRenderer(gameObjects.get(object), RenderingLevel.GAME_OBJECTS);
		theRenderingMachine.registerRenderer(blood, RenderingLevel.BLOOD_AND_GUTS);
	}

	/**
	 * Bereinigt Objekte die verschwinden
	 *
	 * @param das Objekt das verschwinden soll
	 */
	private void remove(AbstractGameObject object) {
		gameObjects.remove(object);
	}
}
