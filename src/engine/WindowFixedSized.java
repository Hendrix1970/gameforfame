package engine;

import java.awt.Dimension;

import javax.swing.JFrame;

public class WindowFixedSized {

	public WindowFixedSized(String title, int width, int height, Game game) {
		
		JFrame frame = new JFrame(title);
		
		Dimension size = new Dimension(width, height);
		
		frame.setPreferredSize(size);
		frame.setMaximumSize(size);
		frame.setMinimumSize(size);
		
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setResizable(false);
		frame.setLocationRelativeTo(null);
		frame.add(game);
		frame.setVisible(true);
		game.start();
		
	}
	
}
