package engine;

import java.util.ArrayList;
import java.util.List;

import Interaction.ChestInteraction;
import Interaction.DoorInteraction;
import Interaction.FightingInteraction;
import Interaction.HealingPotionInteraction;
import Interaction.Interaction;
import Interaction.QuestgiverInteraction;
import NPCs.AbstractQuestGiver;
import Rooms.Spawner;
import framework.AbstractFightable;
import framework.AbstractGameObject;
import gameObjects.Chest;
import gameObjects.Door;
import gameObjects.HealingPotion;
import guiFrameWork.RenderingLevel;
import guiFrameWork.TheRenderingMachine;
import heroStuff.characterClasses.Hero;
import heroStuff.skills.HeroSkill;
import interactiveMenues.AbstractOptionManager;
import interactiveMenues.AbstractScrollableOptionManager;
import interactiveMenues.InventoryManager;
import interactiveMenues.QuestManager;
import interactiveMenues.SkillManager;
import quests.QuestLog;

public class InteractionHandler {

	private Hero hero;
	private QuestLog questLog;
	private TheRenderingMachine theRenderingMachine;
	private AbstractOptionManager<?> optionManager;
	private Spawner spawner;

	// this might be needed to provide the possibility to tab through different
	// interactions
	private List<Interaction> interactionList = new ArrayList<>();
	private Interaction activeInteraction;
	private ScreenStateApadter screenStateApadter;

	private int index = 0;

	public InteractionHandler(TheRenderingMachine theRenderingMachine, ScreenStateApadter screenStateApadter,
			Hero hero) {
		this.theRenderingMachine = theRenderingMachine;
		this.screenStateApadter = screenStateApadter;
		this.hero = hero;

		questLog = new QuestLog();
	}

	public void registerInteraction(Interaction interaction) {
		interactionList.add(interaction);
		activeInteraction = interaction;

		if (activeInteraction instanceof FightingInteraction) {
			startFight();
		}
	}

	private void startFight() {
		AbstractOptionManager<List<HeroSkill>> fightingManager = ((FightingInteraction) activeInteraction)
				.getOptionManager();

		registerOptionManager(fightingManager);
		theRenderingMachine.registerRenderer(fightingManager, RenderingLevel.INTERFACES);
	}

	public void interact() {
		if (activeInteraction != null) {
			activeInteraction.interact(theRenderingMachine);
			if (activeInteraction instanceof QuestgiverInteraction) {
				registerOptionManager(((QuestgiverInteraction) activeInteraction).getOptionManager());
			} else {
				interactionList.remove(activeInteraction);
				resetActiveInteraction();
			}
		}
	}

	/**
	 * registers all interactions for each interactive object within reach. Note
	 * that each object can't provide more than one option.
	 * 
	 * @param hero
	 * @param object
	 */
	protected void updateInteractions(Hero hero, AbstractGameObject object) {
		if (!doublicate(object)) {
			if (object instanceof AbstractQuestGiver) {
				registerInteraction(
						new QuestgiverInteraction((AbstractQuestGiver) object, questLog, hero, screenStateApadter));
			} else if (object instanceof Door) {
				registerInteraction(new DoorInteraction((Door) object, hero, spawner, screenStateApadter));
			} else if (object instanceof HealingPotion) {
				registerInteraction(new HealingPotionInteraction((HealingPotion) object, hero, hero.getInventory()));
			} else if (object instanceof Chest) {
				registerInteraction(new ChestInteraction((Chest) object, hero, hero.getInventory()));
			} else if (object instanceof AbstractFightable) {
				registerInteraction(new FightingInteraction((AbstractFightable) object, hero, screenStateApadter));
			}
		}
	}

	private boolean doublicate(AbstractGameObject object) {
		return interactionList.stream().anyMatch(interaction -> interaction.getInteractive().equals(object));
	}

	public void checkInteractionStateAndEndInteractionIfNecessary() {
		List<Interaction> toRemove = new ArrayList<>();

		if (activeInteraction != null && activeInteraction.isInteractionDone()) {
			activeInteraction.endInteraction(theRenderingMachine);
			toRemove.add(activeInteraction);
			resetActiveInteraction();
		}
		unregisterInteractions(toRemove);
	}

	private Interaction resetActiveInteraction() {
		return activeInteraction = null;
	}

	/**
	 * removes all interactions that are out of range and if any is gevien any
	 * specific interaction "toRemove".
	 * 
	 * @param toRemove
	 */
	public void unregisterInteractions(List<Interaction> toRemove) {
		List<Interaction> remove = new ArrayList<>();
		if (toRemove != null) {
			remove = toRemove;
		}
		// checks if still in range
		for (Interaction interaction : interactionList) {
			if (interaction.isOutOfRange()) {
				remove.add(interaction);
			}
		}
		// actually removes the interactions and refreshes the activeInteraction
		if (!remove.isEmpty()) {
			interactionList.removeAll(remove);
			if (remove.contains(activeInteraction)) {
				activeInteraction = interactionList.iterator().hasNext() ? interactionList.iterator().next() : null;
			}
		}
	}

	public void registerOptionManager(AbstractOptionManager<?> optionManager) {
		this.optionManager = optionManager;
	}

	public AbstractOptionManager<?> getOptionManager() {
		return optionManager;
	}

	public void setSpawner(Spawner spawner) {
		this.spawner = spawner;
	}

	public void openInventory() {
		AbstractScrollableOptionManager inventar = new InventoryManager(hero.getInventory());
		registerOptionManager(inventar);
		theRenderingMachine.registerRenderer(inventar, RenderingLevel.INTERFACES);
	}

	public void openSkillScreen() {
		AbstractScrollableOptionManager skillscreen = new SkillManager(hero.getClassSpecificSkills(),
				hero.getActiveSkills());
		registerOptionManager(skillscreen);
		theRenderingMachine.registerRenderer(skillscreen, RenderingLevel.INTERFACES);
	}

	public void openQuestLog() {
		AbstractScrollableOptionManager questManager = new QuestManager(questLog);
		registerOptionManager(questManager);
		theRenderingMachine.registerRenderer(questManager, RenderingLevel.INTERFACES);
	}

	public void nextInteraction() {
		if (index < interactionList.size()) {
			index = +1;
			activeInteraction = interactionList.get(index);
		} else if (interactionList.size() > 0) {
			index = 0;
			activeInteraction = interactionList.get(index);
		} else {
			index = 0;
			resetActiveInteraction();
		}
	}

	public void fight() {
		if (activeInteraction instanceof FightingInteraction) {
			FightingInteraction fightingInteraction = (FightingInteraction) activeInteraction;
			fightingInteraction.tick();
			if (fightingInteraction.isInteractionDone()) {
				endFight(fightingInteraction);
			}
		}

	}

	private void endFight(FightingInteraction fightingInteraction) {
		if (hero.isAlive()) {
			AbstractFightable enemy = (AbstractFightable) fightingInteraction.getInteractive();
			questLog.registerKill(enemy.getEnemyType());
			fightingInteraction.reward();
		}
		checkInteractionStateAndEndInteractionIfNecessary();
	}

}
