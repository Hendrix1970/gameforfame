package engine;

import framework.ScreenStates;

public class ScreenStateApadter {

    private ScreenStates screenState;
    private boolean levelUp = false;

    public ScreenStateApadter() {
        screenState = ScreenStates.inGame;
    }

    public ScreenStates getScreenState() {
        return this.screenState;
    }

    public void setScreenState(ScreenStates screenState) {
        this.screenState = screenState;
    }

    public boolean isLevelUp() {
        return levelUp;
    }

    public void setLevelUp(boolean levelUp) {
        this.levelUp = levelUp;
    }
}
