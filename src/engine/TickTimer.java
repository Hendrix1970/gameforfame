package engine;

public class TickTimer {

	long lastTime = System.nanoTime();
	double amountOfTicks = 60.0;
	double ns = 1000000000 / amountOfTicks;
	double delta = 0;
	long timer = System.currentTimeMillis();

	public TickTimer(double amountOfTicks) {
		this.amountOfTicks = amountOfTicks;
	}

	public boolean isTick() {
		long now = System.nanoTime();
		delta += (now - lastTime) / ns;
		lastTime = now;
		while (delta >= 1) {
			delta--;
			return true;
		}
		return false;
	}
}
