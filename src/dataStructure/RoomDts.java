package dataStructure;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import framework.AbstractGameObject;
import framework.BackroundRenderer;
import framework.LevelName;
import guiFrameWork.renderer.BloodAndGutsRenderer;
import guiFrameWork.renderer.Renderer;

public class RoomDts {

	private LevelName name;

	private Map<AbstractGameObject, Renderer> objects;

	// provides an empty List of Blood and Guts
	private List<BloodAndGutsRenderer> boodAndGuts = new ArrayList<>();

	// provides a defaulted BackroundRederer
	private Renderer backgroundRenderer = new BackroundRenderer();

	public Map<AbstractGameObject, Renderer> getObjects() {
		return objects;
	}

	public void setObjects(Map<AbstractGameObject, Renderer> objects) {
		this.objects = objects;
	}

	public LevelName getName() {
		return name;
	}

	public void setName(LevelName name) {
		this.name = name;
	}

	public List<BloodAndGutsRenderer> getBoodAndGuts() {
		return boodAndGuts;
	}

	public void setBoodAndGuts(List<BloodAndGutsRenderer> boodAndGuts) {
		this.boodAndGuts = boodAndGuts;
	}

	public Renderer getBackgroundRenderer() {
		return backgroundRenderer;
	}

	public void setBackgroundRenderer(Renderer backgroundRenderer) {
		this.backgroundRenderer = backgroundRenderer;
	}

}
