package dataStructure;

public class FightingAttributesDts {
    int health;
    int damage;
    int defense;
    int maxHealth;

    public FightingAttributesDts(int health, int damage, int defense) {
        this.health = health;
        this.damage = damage;
        this.defense = defense;
    }

    /**
     * 
     * @param health
     * @param damage
     * @param defense
     * @param maxHealth
     */
    public FightingAttributesDts(int health, int damage, int defense, int maxHealth) {
        this.health = health;
        this.damage = damage;
        this.defense = defense;
        this.maxHealth = maxHealth;
    }

    public int getMaxHealth() {
        return maxHealth;
    }

    public void setMaxHealth(int maxHealth) {
        this.maxHealth = maxHealth;
    }

    public int getHealth() {
        return health;
    }

    public void setHealth(int health) {
        this.health = health;
    }

    public int getDamage() {
        return damage;
    }

    public void setStrength(int strength) {
        this.damage = strength;
    }

    public int getDefense() {
        return defense;
    }

    public void setDefense(int defense) {
        this.defense = defense;
    }

}
