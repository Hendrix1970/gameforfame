package dataStructure;

import interactiveMenues.Answer;

import java.util.List;

public class DialogDts {
	
	/**
	 * the name of the talking npc
	 */
	String name;
	
	/**
	 * the text the npc says
	 */
	String npcText;
	
	/**
	 * a list of possible answers
	 */
	List<Answer> answers;
	
	/**
	 * the number identifier of the dialog tree for this part of the dialog to be selected
	 */
	int vertice;
	
	public DialogDts(String name, String npcText, List<Answer> answers, int vertice) {
		this.name = name;
		this.npcText = npcText;
		this.answers = answers;
		this.vertice = vertice;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getNpcText() {
		return npcText;
	}

	public void setNpcText(String npcText) {
		this.npcText = npcText;
	}

	public List<Answer> getAnswers() {
		return answers;
	}

	public void setAnswers(List<Answer> answers) {
		this.answers = answers;
	}

	public int getVertice() {
		return vertice;
	}

	public void setVertice(int vertice) {
		this.vertice = vertice;
	}
}
