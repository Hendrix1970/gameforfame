package dataStructure;

import Villains.Skill;
import framework.AbstractMoveableGameObject;
import interactiveMenues.Option;

public class SkillModelBean implements Option {
	private Skill skill;
	private AbstractMoveableGameObject villain;
	int position;

	@Override
	public void execute() {
		// nothing to do here
	}

	public boolean isSelectable() {
		return !skill.isCooldownActive();
	}

	@Override
	public int getPosition() {
		return position;
	}

	@Override
	public String getText() {
		return skill.getName();
	}

	public Skill getSkill() {
		return skill;
	}

	public void setSkill(Skill skill) {
		this.skill = skill;
	}

	public AbstractMoveableGameObject getVillain() {
		return villain;
	}

	public void setVillain(AbstractMoveableGameObject villain) {
		this.villain = villain;
	}

	public void setPosition(int position) {
		this.position = position;
	}

}
