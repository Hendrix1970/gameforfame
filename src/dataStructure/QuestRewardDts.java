package dataStructure;

import java.util.List;

import gameObjects.Items.Item;

public class QuestRewardDts {

	private int xp;
	private int gold;
	private List<Item> items;

	public QuestRewardDts(int xp, int gold, List<Item> items) {
		this.xp = xp;
		this.gold = gold;
		this.setItems(items);
	}

	public int getXp() {
		return xp;
	}

	public void setXp(int xp) {
		this.xp = xp;
	}

	public int getGold() {
		return gold;
	}

	public void setGold(int gold) {
		this.gold = gold;
	}

	public List<Item> getItems() {
		return items;
	}

	public void setItems(List<Item> items) {
		this.items = items;
	}

}
