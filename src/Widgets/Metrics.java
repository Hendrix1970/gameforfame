package Widgets;

import java.awt.Font;

public class Metrics {

	protected Metrics(Font font) {
	}

	public int charsWidth(char[] data, int off, int len) {
		int length = off;
		for (char c : data) {

			switch (c) {
			case '1':
				length += 12;
				break;
			case '2':
				length += 19;
				break;
			case '3':
				length += 16;
				break;
			case '4':
				length += 19;
				break;
			case '5':
				length += 16;
				break;
			case '6':
				length += 17;
				break;
			case '7':
				length += 17;
				break;
			case '8':
				length += 18;
				break;
			case '9':
				length += 18;
				break;
			case '0':
				length += 17;
				break;
			}

		}
		return length;
	}
}
