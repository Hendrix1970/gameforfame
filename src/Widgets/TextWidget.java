package Widgets;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;

import guiFrameWork.RenderingTool;
import guiFrameWork.FontUtils;
import guiFrameWork.ResolutionUtil;

public class TextWidget extends RenderingTool {

	private String text;
	private int x;
	private int y;
	private int fontSize;
	private TextBound textBound;
	private TextLength width;
	private Color color;
	private boolean visible;
	private boolean framed;

	private Font font;

	public TextWidget(String string, int x, int y) {
		super();

		this.text = string;
		this.x = x;
		this.y = y;

		defaultValues();
	}

	public TextWidget(int x, int y) {
		this.x = x;
		this.y = y;
	}

	private void defaultValues() {
		// default values
		textBound = TextBound.RIGHT;
		fontSize = 42;
		font = FontUtils.getStandardFontInSize(fontSize);
		width = TextLength.MEDIUM;
		color = Color.WHITE;
		visible = true;
		framed = false;
	}

	public void draw(Graphics g) {
		int height = 25;

		if (!visible) {
			return;
		}
		g.setFont(font);
		g.setColor(color);

		if (textBound == TextBound.LEFT) {
			drawResponsiveString(text, x, y, g);
			g.drawChars(text.toCharArray(), 0, text.length(), x, y);
		} else if (textBound == TextBound.RIGHT) {
			drawRightBoundResponsiveString(text, x, y, width.length(), g);
		} else if (textBound == TextBound.CENTER) {
			drawCenteredResponsiveString(text, x, y, width.length(), g);
		}

		if (framed) {
			g.drawRect(ResolutionUtil.recalculateX(x, getResolution()),
					ResolutionUtil.recalculateY(y - height, getResolution()),
					ResolutionUtil.recalculateX(width.length(), getResolution()),
					ResolutionUtil.recalculateY(height, getResolution()));
		}
	}

	public TextWidget withTextBound(TextBound bound) {
		setTextBound(bound);
		return this;
	}

	public TextWidget withFontSize(int size) {
		setFontSize(size);
		return this;
	}

	public TextWidget withFont(Font font) {
		setFont(font);
		return this;
	}

	public TextWidget withLengt(TextLength length) {
		setLength(length);
		return this;
	}

	public TextWidget withColor(Color color) {
		setColor(color);
		return this;
	}

	public TextWidget withVisible(boolean visible) {
		setVisible(visible);
		return this;
	}

	public TextWidget withFramed(boolean framed) {
		setFramed(framed);
		return this;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public int getFontSize() {
		return fontSize;
	}

	public void setFontSize(int fontSize) {
		this.fontSize = fontSize;
	}

	public TextBound getTextBound() {
		return textBound;
	}

	public void setTextBound(TextBound textBound) {
		this.textBound = textBound;
	}

	public TextLength getLength() {
		return width;
	}

	public void setLength(TextLength length) {
		this.width = length;
	}

	public Color getColor() {
		return color;
	}

	public void setColor(Color color) {
		this.color = color;
	}

	public boolean isVisible() {
		return visible;
	}

	public void setVisible(boolean visible) {
		this.visible = visible;
	}

	public boolean isFramed() {
		return framed;
	}

	public void setFramed(boolean framed) {
		this.framed = framed;
	}

	public Font getFont() {
		return font;
	}

	public void setFont(Font font) {
		this.font = font;
	}
}
