package Widgets;

public enum TextLength {
	SHORT(50), MEDIUM(75), LONG(100);

	private int length;

	private TextLength(int length) {
		this.length = length;
	}

	public int length() {
		return length;
	}
}
