package heroStuff.characterClasses;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import Rooms.Spawner;
import dataStructure.QuestRewardDts;
import framework.AbstractMoveableGameObject;
import heroStuff.Inventory;
import heroStuff.LevelManager;
import heroStuff.skills.HeroSkill;

public abstract class Hero extends AbstractMoveableGameObject {

    private int xp = 0;
    private int level = 1;

    // Primary Attributes
    private int strength;
    private int intelligence;
    private int dexterit;

    // Secondary Attributes
    private int damage;
    private int defense;
    private int health;
    private int maxHealth;
    private int baseStrength;
    private int baseIntelligence;
    private int baseDexterit;
    private int baseDefense;

    private boolean alive = true;

    private LevelManager levelManager;
    protected List<HeroSkill> activeSkills = new ArrayList<>();
    private List<HeroSkill> classSpecificSkills = new ArrayList<>();
    private Inventory inventory;

    abstract void setupSkills();

    abstract void setupStats();

    public Hero(int x, int y) {
        super(x, y, Spawner.HUMAN_STANDARD_SIZE, Spawner.HUMAN_STANDARD_SIZE, 5);

        setupSkills();
        updateSkills();
        setupStats();
    }

    public void updateSkills() {
        List<HeroSkill> skills =
                classSpecificSkills.stream().filter(s -> s.getLevelRequirement() == getLevel()).peek(s -> s.setActive(true)).collect(Collectors.toList());
        if (skills.size() + activeSkills.size() <= 4) {
            activeSkills.addAll(skills);
        }
    }

    @Override
    public void tick() {
        move();
    }

    public void gainXP(int xpGain) {
        this.xp += xpGain;
        levelManager.levelUpIfAppropriate();
    }

    public List<HeroSkill> getActiveSkills() {
        return activeSkills;
    }

    public void heal(int ammount) {
        if (health + ammount > maxHealth) {
            health = maxHealth;
        }
        else {
            health += ammount;
        }
    }

    public void retrieveQuestRewards(QuestRewardDts reward) {
        if (reward != null) {
            inventory.addGold(reward.getGold());
            inventory.addItems(reward.getItems());
            gainXP(reward.getXp());
        }
    }

    public int getHealth() {
        return health;
    }

    public void setHealth(int health) {
        this.health = health;
    }

    public int getDamage() {
        return damage;
    }

    public void setDamage(int damage) {
        this.damage = damage;
    }

    public int getDefense() {
        return defense;
    }

    public void setDefense(int defense) {
        this.defense = defense;
    }

    public int getXp() {
        return xp;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public int getMaxHealth() {
        return maxHealth;
    }

    public void setMaxHealth(int maxHealth) {
        this.maxHealth = maxHealth;
    }

    public LevelManager getLevelManager() {
        return levelManager;
    }

    public void setLevelManager(LevelManager levelManager) {
        this.levelManager = levelManager;
    }

    public int getStrength() {
        return strength;
    }

    public void setStrength(int strength) {
        this.strength = strength;
    }

    public int getIntelligence() {
        return intelligence;
    }

    public void setIntelligence(int intelligence) {
        this.intelligence = intelligence;
    }

    public int getDexterity() {
        return dexterit;
    }

    public void setDexterit(int dexterit) {
        this.dexterit = dexterit;
    }

    public int getBaseStrength() {
        return baseStrength;
    }

    public void setBaseStrength(int baseStrength) {
        this.baseStrength = baseStrength;
    }

    public int getBaseIntelligence() {
        return baseIntelligence;
    }

    public void setBaseIntelligence(int baseIntelligence) {
        this.baseIntelligence = baseIntelligence;
    }

    public int getBaseDexterit() {
        return baseDexterit;
    }

    public void setBaseDexterit(int baseDexterit) {
        this.baseDexterit = baseDexterit;
    }

    public int getBaseDefense() {
        return baseDefense;
    }

    public void setBaseDefense(int baseDefense) {
        this.baseDefense = baseDefense;
    }

    public void die() {
        alive = false;
    }

    public boolean isAlive() {
        return alive;
    }

    public Inventory getInventory() {
        return inventory;
    }

    public void setInventory(Inventory inventory) {
        this.inventory = inventory;
    }

    public List<HeroSkill> getClassSpecificSkills() {
        return classSpecificSkills;
    }

    void setClassSpecificSkills(List<HeroSkill> classSpecificSkills) {
        this.classSpecificSkills = classSpecificSkills;
    }
}
