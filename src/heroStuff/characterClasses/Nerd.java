package heroStuff.characterClasses;

import java.util.List;

import heroStuff.skills.HeroSkill;
import heroStuff.skills.NerdischesPiksen;
import heroStuff.skills.Verkriechen;
import heroStuff.skills.Zerstoerung;

public class Nerd extends Hero {

	public Nerd(int x, int y) {
		super(x, y);
	}

	void setupSkills() {
		List<HeroSkill> skills = getClassSpecificSkills();
		skills.add(new NerdischesPiksen(this));
		skills.add(new Zerstoerung(this));
		skills.add(new Verkriechen(this));
	}

	void setupStats() {
		setStrength(5);
		setDexterit(5);
		setIntelligence(10);

		setMaxHealth(50);
		setHealth(getMaxHealth());
		setDamage(getStrength());
		setDefense(2);

		setBaseStrength(getStrength());
		setBaseDexterit(getDexterity());
		setBaseIntelligence(getIntelligence());
		setBaseDefense(getDefense());
	}

}
