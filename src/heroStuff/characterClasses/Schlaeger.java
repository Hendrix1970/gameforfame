package heroStuff.characterClasses;

import java.util.List;

import heroStuff.skills.HeroSkill;
import heroStuff.skills.NerdischesPiksen;
import heroStuff.skills.Verkriechen;
import heroStuff.skills.Zerstoerung;

public class Schlaeger extends Hero {

	public Schlaeger(int x, int y) {
		super(x, y);
	}

	void setupSkills() {
		List<HeroSkill> skills = getClassSpecificSkills();
		skills.add(new NerdischesPiksen(this));
		skills.add(new Zerstoerung(this));
		skills.add(new Verkriechen(this));
	}

	void setupStats() {
		setStrength(10);
		setDexterit(5);
		setIntelligence(2);

		setMaxHealth(70);
		setHealth(getMaxHealth());
		setDamage(getStrength());
		setDefense(4);

		setBaseStrength(getStrength());
		setBaseDexterit(getDexterity());
		setBaseIntelligence(getIntelligence());
		setBaseDefense(getDefense());
	}

}
