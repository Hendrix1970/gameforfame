package heroStuff.skills;

import framework.AbstractFightable;
import heroStuff.characterClasses.Hero;

public class Zerstoerung extends HeroSkill {

    public Zerstoerung(Hero hero) {
        super(hero);
        setName("Durchs Auge ins Hirn");
        setMiniGame(true);
        initCooldown(3);
    }

    public void executeSkill(AbstractFightable villain, double miniGameResult) {
        super.executeSkill(villain, miniGameResult);
    }

    public double calculateDamage(AbstractFightable villain, double miniGameResult) {
        return miniGameResult * (getHero().getDamage() - villain.getDefense()) * 3;
    }

    @Override
    public int getLevelRequirement() {
        return 2;
    }

    @Override
    public String getDescription() {
        return "Wenn man ins Auge Zielt tuts meist mehr weh, als wenn man nur die Schulter erwischt.";
    }
}
