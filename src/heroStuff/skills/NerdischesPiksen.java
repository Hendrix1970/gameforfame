package heroStuff.skills;

import framework.AbstractFightable;
import heroStuff.characterClasses.Hero;

public class NerdischesPiksen extends HeroSkill {

	public NerdischesPiksen(Hero hero) {
		super(hero);
		setName("Nerdisches Piksen");
		setMiniGame(true);
	}

	@Override
	public int getLevelRequirement() {
		return 1;
	}

	public double calculateDamage(AbstractFightable villain, double miniGameResult) {
		return miniGameResult * (getHero().getDamage() - villain.getDefense());
	}

	@Override
	public String getDescription() {
		return "Ein leichter Piks, den nur ein echter Nerd als ernstzunehmenden Angriff erachten könnte.";
	}
}
