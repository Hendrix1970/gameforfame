package heroStuff.skills;

import java.awt.Image;

import javax.swing.ImageIcon;

import Villains.Skill;
import framework.AbstractFightable;
import heroStuff.characterClasses.Hero;

public abstract class HeroSkill extends Skill {

    private Hero hero;
    private boolean miniGame = false;
    private boolean unlocked = false;
    Image portraitForInventar = new ImageIcon(getClass().getResource("/images/stift.png")).getImage();

    public HeroSkill(Hero hero) {
        this.setHero(hero);
    }

    public abstract double calculateDamage(AbstractFightable villain, double miniGameResult);

    public abstract int getLevelRequirement();

    public Image getPortraitForInventar() {
        return portraitForInventar;
    }

    public void calculateAndSetHit(AbstractFightable villain) {
        // hit chance is calculated from a constant part and a part relaying in
        // both the heros and the villains dexterity. Which is the difference between both time two in percent.
        double hitchance = (double) DEFAULT_HIT_PERCENTAGE + (double) (getHero().getDexterity() - villain.getDexterity()) / 50;
        double random = Math.random();
        setHit(random < hitchance);
    }

    public void executeSkill(AbstractFightable villain, double miniGameResult) {
        setDamage((int) calculateDamage(villain, miniGameResult));
        if (getDamage() > 0) {
            villain.setHealth(villain.getHealth() - getDamage());
        }
    }

    public String getSuccessText() {
        if (getDamage() != 0) {
            return getName() + " hat " + getDamage() + " Schaden gemacht!";
        }
        else {
            return getName() + " hat zwar getroffen, konte Aber die Rüstung des Gegners nicht durchdringen.";
        }
    }

    public String getFailureText() {
        return "Leider hat " + getName() + " nicht getroffen.";
    }

    public Hero getHero() {
        return hero;
    }

    public void setHero(Hero hero) {
        this.hero = hero;
    }

    public boolean isMiniGame() {
        return miniGame;
    }

    public void setMiniGame(boolean isMiniGame) {
        this.miniGame = isMiniGame;
    }

    public boolean isUnlocked() {
        return unlocked;
    }

    public void setActive(boolean active) {
        this.unlocked = active;
    }

    public String getDescription() {
        return "Ein Skill der seine Beschreibung sucht";
    }
}
