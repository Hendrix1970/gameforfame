package heroStuff.skills;

import framework.AbstractFightable;
import heroStuff.characterClasses.Hero;

public class Verkriechen extends HeroSkill {

    int value = 10;

    public Verkriechen(Hero hero) {
        super(hero);
        setName("Verkriechen");
        setMiniGame(false);
        initCooldown(3);
    }

    @Override
    public void calculateAndSetHit(AbstractFightable villain) {
        setHit(true);
    }

    @Override
    public int getLevelRequirement() {
        return 3;
    }

    @Override
    public void executeSkill(AbstractFightable villain, double miniGameResult) {
        getHero().heal(value);
    }

    @Override
    public String getSuccessText() {
        return "Ihr habt euch kurz versteckt und um " + value + " LP geheilt.";
    }

    public double calculateDamage(AbstractFightable villain, double miniGameResult) {
        return 0;
    }
}
