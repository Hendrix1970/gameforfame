package heroStuff;

import engine.ScreenStateApadter;
import heroStuff.characterClasses.Hero;

public class LevelManager {

    int nextLevelXp = 50;
    int previousLevelXp = 0;
    int xp;

    private Hero hero;
    private ScreenStateApadter screenStateApadter;

    public LevelManager(Hero hero, ScreenStateApadter screenStateApadter) {
        this.hero = hero;
        this.screenStateApadter = screenStateApadter;
        hero.setLevelManager(this);
    }

    public void levelUpIfAppropriate() {
        if (hero.getXp() >= 40 && hero.getLevel() < 2) {
            levelUp(2, 100);
        }
        if (hero.getXp() >= 100 && hero.getLevel() < 3) {
            levelUp(3, 200);
        }
        if (hero.getXp() >= 200 && hero.getLevel() < 4) {
            levelUp(4, 400);
        }
        if (hero.getXp() >= 400 && hero.getLevel() < 5) {
            levelUp(5, 800);
        }
        if (hero.getXp() >= 800 && hero.getLevel() < 6) {
            levelUp(6, 1600);
        }
        if (hero.getXp() >= 1600 && hero.getLevel() < 7) {
            levelUp(7, 3000);
        }
        if (hero.getXp() >= 3000 && hero.getLevel() < 8) {
            levelUp(8, 6000);
        }
        if (hero.getXp() >= 6000 && hero.getLevel() < 9) {
            levelUp(9, 12000);
        }
        if (hero.getXp() >= 12000 && hero.getLevel() < 10) {
            levelUp(10, 12000);
        }
    }

    private void levelUp(int level, int nextLevel) {
        hero.setLevel(level);
        previousLevelXp = nextLevelXp;
        nextLevelXp = nextLevel;
        hero.updateSkills();
        screenStateApadter.setLevelUp(true);
    }

    public int getNextLevelXp() {
        return nextLevelXp;
    }

    public int getPreviousLevelXp() {
        return previousLevelXp;
    }
}
