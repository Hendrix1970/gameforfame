package heroStuff;

import java.awt.Image;
import java.util.ArrayList;
import java.util.List;

import javax.swing.ImageIcon;

import gameObjects.ChickenWing;
import gameObjects.HealingPotion;
import gameObjects.Items.Armor;
import gameObjects.Items.Helmet;
import gameObjects.Items.Item;
import gameObjects.Items.Shield;
import gameObjects.Items.Shoe;
import gameObjects.Items.Weapon;
import heroStuff.characterClasses.Hero;
import interactiveMenues.Consumable;

public class Inventory {

	private List<Item> itemList;
	private Weapon currentlyEquippedWeapon;
	private Shield currentlyEquippedShield;
	private Armor currentlyEquippedArmor;
	private Helmet currentlyEquippedHelmet;
	private Shoe currentlyEquippedLeftShoe;
	private Shoe currentlyEquippedRightShoe;
	private int gold = 0;

	private Hero hero;

	public Inventory(Hero hero) {
		this.hero = hero;
		hero.setInventory(this);
		itemList = new ArrayList<>();
		setupItems();
	}

	/**
	 * Just filling the Inventory with some default Items.
	 */
	private void setupItems() {
		Image imageBleistift = new ImageIcon(getClass().getResource("/images/item-stift.png")).getImage();
		Weapon bleistift = new Weapon("Bleistift", 7, imageBleistift);
		currentlyEquippedWeapon = bleistift;

		itemList.add(new HealingPotion());
		itemList.add(new ChickenWing());

		Image imageLineal = new ImageIcon(getClass().getResource("/images/item-lineal.png")).getImage();
		Weapon lineal = new Weapon("Lineal", 4, imageLineal);
		lineal.setDexterityModifier(10);
		itemList.add(lineal);

		Image imageHemd = new ImageIcon(getClass().getResource("/images/item_armor_hemd.png")).getImage();
		Armor armor = new Armor("Hemd", 1, imageHemd);
		armor.setIntellegenceModifier(1);
		currentlyEquippedArmor = armor;

		updateHero();
	}

	/**
	 * Consumes Consumables, and equips any other Item, putting the prior equipped
	 * item back to the itemList.
	 * 
	 * @param the item to use
	 */
	public void useItem(Item item) {
		if (item instanceof Consumable) {
			if (!((Consumable) item).canBeConsumed(hero)) {
				return;
			}
			((Consumable) item).consume(hero);
		}
		if (item instanceof Weapon) {
			addItem(currentlyEquippedWeapon);
			currentlyEquippedWeapon = (Weapon) item;
		}
		if (item instanceof Shield) {
			addItem(currentlyEquippedShield);
			currentlyEquippedShield = (Shield) item;
		}
		if (item instanceof Armor) {
			addItem(currentlyEquippedArmor);
			currentlyEquippedArmor = (Armor) item;
		}
		if (item instanceof Helmet) {
			addItem(currentlyEquippedHelmet);
			currentlyEquippedHelmet = (Helmet) item;
		}
		itemList.remove(item);
		updateHero();
	}

	private void updateHero() {
		hero.setIntelligence(calculateIntelligence());
		hero.setDexterit(calculateDexterit());
		hero.setStrength(calculateStrenght());

		hero.setDamage(calculateDamage());
		hero.setDefense(calculateDefense());
	}

	private int calculateIntelligence() {
		int intelligence = hero.getBaseIntelligence();
		intelligence += currentlyEquippedArmor != null ? currentlyEquippedArmor.getIntellegenceModifier() : 0;
		intelligence += currentlyEquippedHelmet != null ? currentlyEquippedHelmet.getIntellegenceModifier() : 0;
		intelligence += currentlyEquippedWeapon != null ? currentlyEquippedWeapon.getIntellegenceModifier() : 0;
		intelligence += currentlyEquippedShield != null ? currentlyEquippedShield.getIntellegenceModifier() : 0;
		intelligence += currentlyEquippedLeftShoe != null ? currentlyEquippedLeftShoe.getIntellegenceModifier() : 0;
		intelligence += currentlyEquippedRightShoe != null ? currentlyEquippedRightShoe.getIntellegenceModifier() : 0;
		return intelligence;
	}

	private int calculateDexterit() {
		int dexterit = hero.getBaseDexterit();
		dexterit += currentlyEquippedArmor != null ? currentlyEquippedArmor.getDexterityModifier() : 0;
		dexterit += currentlyEquippedHelmet != null ? currentlyEquippedHelmet.getDexterityModifier() : 0;
		dexterit += currentlyEquippedWeapon != null ? currentlyEquippedWeapon.getDexterityModifier() : 0;
		dexterit += currentlyEquippedShield != null ? currentlyEquippedShield.getDexterityModifier() : 0;
		dexterit += currentlyEquippedLeftShoe != null ? currentlyEquippedLeftShoe.getDexterityModifier() : 0;
		dexterit += currentlyEquippedRightShoe != null ? currentlyEquippedRightShoe.getDexterityModifier() : 0;
		return dexterit;
	}

	private int calculateStrenght() {
		int strength = hero.getBaseStrength();
		strength += currentlyEquippedArmor != null ? currentlyEquippedArmor.getStrengthModifier() : 0;
		strength += currentlyEquippedHelmet != null ? currentlyEquippedHelmet.getStrengthModifier() : 0;
		strength += currentlyEquippedWeapon != null ? currentlyEquippedWeapon.getStrengthModifier() : 0;
		strength += currentlyEquippedShield != null ? currentlyEquippedShield.getStrengthModifier() : 0;
		strength += currentlyEquippedLeftShoe != null ? currentlyEquippedLeftShoe.getStrengthModifier() : 0;
		strength += currentlyEquippedRightShoe != null ? currentlyEquippedRightShoe.getStrengthModifier() : 0;
		return strength;
	}

	private int calculateDamage() {
		int damage = hero.getStrength();
		damage += currentlyEquippedWeapon != null ? currentlyEquippedWeapon.getDamage() : 0;
		return damage;
	}

	private int calculateDefense() {
		int defense = hero.getBaseDefense();
		defense += currentlyEquippedArmor != null ? currentlyEquippedArmor.getDefense() : 0;
		defense += currentlyEquippedHelmet != null ? currentlyEquippedHelmet.getDefense() : 0;
		defense += currentlyEquippedShield != null ? currentlyEquippedShield.getDefense() : 0;
		return defense;
	}

	public List<Item> getItemList() {
		return itemList;
	}

	/**
	 * Picks up an item and puts it into the "bag". Will not put null into the "bag"
	 * 
	 * @param item
	 */
	public void addItem(Item item) {
		if (item != null) {
			itemList.add(item);
		}
	}

	/**
	 * Picks up an item and puts it into the "bag". Will not put null into the "bag"
	 * 
	 * @param item
	 */
	public void addItems(List<Item> items) {
		if (items != null) {
			itemList.addAll(items);
		}
	}

	public Weapon getCurrentlyEquippedWeapon() {
		return currentlyEquippedWeapon;
	}

	public Shield getCurrentlyEquippedShield() {
		return currentlyEquippedShield;
	}

	public Armor getCurrentlyEquippedArmor() {
		return currentlyEquippedArmor;
	}

	public Helmet getCurrentlyEquippedHelmet() {
		return currentlyEquippedHelmet;
	}

	public Shoe getCurrentlyEquippedLeftShoe() {
		return currentlyEquippedLeftShoe;
	}

	public void setCurrentlyEquippedLeftShoe(Shoe currentlyEquippedLeftShoe) {
		this.currentlyEquippedLeftShoe = currentlyEquippedLeftShoe;
	}

	public Shoe getCurrentlyEquippedRightShoe() {
		return currentlyEquippedRightShoe;
	}

	public void setCurrentlyEquippedRightShoe(Shoe currentlyEquippedRightShoe) {
		this.currentlyEquippedRightShoe = currentlyEquippedRightShoe;
	}

	public int getGold() {
		return gold;
	}

	public void addGold(int amount) {
		this.gold += amount;
	}
}
