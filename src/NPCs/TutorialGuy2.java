package NPCs;

import quests.QuestKey;

public class TutorialGuy2 extends AbstractQuestGiver {

	public TutorialGuy2(int x, int y, int height, int width) {
		super(x, y, height, width, QuestKey.CHICKEN_QUEST_2, "Tutorial Franz");
	}
}
