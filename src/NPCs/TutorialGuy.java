package NPCs;

import quests.QuestKey;

public class TutorialGuy extends AbstractQuestGiver {

	public TutorialGuy(int x, int y, int height, int width) {
		super(x, y, height, width, QuestKey.CHICKEN_QUEST, "Tutorial Horst");
	}
}
