package NPCs;

import Interaction.InteractionType;
import engine.Game;
import framework.AbstractGameObject;
import framework.Activatable;
import heroStuff.characterClasses.Hero;
import interactiveMenues.DialogManager;

public abstract class AbstractQuestGiver extends AbstractGameObject implements Activatable {

    protected Game game;
    protected DialogManager dialogManager;
    private String questKey;
    private String name;

    public AbstractQuestGiver(int x, int y, int height, int width, String questKey, String name) {
        super(x, y, height, width);
        this.questKey = questKey;
        this.name = name;
    }

    @Override
    public void tick() {
        // nothing to do here
    }

    public void activate(Hero hero) {
        // nothing to do here
    }

    public InteractionType getInteractionType() {
        return InteractionType.DIALOG;
    }

    public String getQuestKey() {
        return questKey;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}