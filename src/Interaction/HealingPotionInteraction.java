package Interaction;

import gameObjects.HealingPotion;
import guiFrameWork.RenderingLevel;
import guiFrameWork.TheRenderingMachine;
import heroStuff.Inventory;
import heroStuff.characterClasses.Hero;

public class HealingPotionInteraction extends Interaction {

	private HealingPotion healingPotion;
	private Inventory inventory;
	private boolean interactionDone;

	public HealingPotionInteraction(HealingPotion healingPotion, Hero hero, Inventory inventory) {
		super(healingPotion, hero, null);
		this.healingPotion = healingPotion;
		this.inventory = inventory;
		interactionDone = false;
	}

	public void interact(TheRenderingMachine theRenderingMachine) {
		healingPotion.setOnMap(false);
		inventory.addItem(healingPotion);
		interactionDone = true;
		theRenderingMachine.unregisterRenderer(healingPotion, RenderingLevel.ITEMS);
	}

	@Override
	public void endInteraction(TheRenderingMachine theRenderingMachine) {
	}

	@Override
	public boolean isInteractionDone() {
		return interactionDone;
	}

}
