package Interaction;

import java.util.Optional;

import Rooms.Spawner;
import engine.ScreenStateApadter;
import framework.Activatable;
import framework.Unlockable;
import gameObjects.Door;
import gameObjects.Key;
import gameObjects.Items.Item;
import guiFrameWork.TheRenderingMachine;
import heroStuff.characterClasses.Hero;

public class DoorInteraction extends Interaction {

	private Spawner spawner;

	public DoorInteraction(Activatable interactive, Hero hero, Spawner spawner, ScreenStateApadter screenStateApadter) {
		super(interactive, hero, screenStateApadter);
		this.spawner = spawner;

	}

	@Override
	public void interact(TheRenderingMachine theRenderingMachine) {
		if (isOpen())
			spawner.loading((Door) getInteractive());
	}

	private boolean isOpen() {
		if (getInteractive() instanceof Unlockable) {
			Unlockable door = (Unlockable) getInteractive();
			if (door.isLocked()) {
				Optional<Item> code = getHero().getInventory().getItemList().stream().filter(i -> i instanceof Key)
						.filter(k -> ((Key) k).getCode().equals(door.getCode())).findFirst();
				if (code.isPresent()) {
					door.unlock(((Key) code.get()).getCode());
				} else {
					return false;
				}
			}
		}
		return true;
	}

	@Override
	public void endInteraction(TheRenderingMachine theRenderingMachine) {
		// TODO actually use this
	}

	@Override
	public boolean isInteractionDone() {
		return false;
	}
}
