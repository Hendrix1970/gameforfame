package Interaction;

import engine.ScreenStateApadter;
import framework.AbstractMoveableGameObject;
import framework.Activatable;
import framework.Direction;
import guiFrameWork.TheRenderingMachine;
import heroStuff.characterClasses.Hero;

public abstract class Interaction {

	private Activatable interactive;
	private Hero hero;
	private ScreenStateApadter screenStateApadter;

	public Interaction(Activatable interactive, Hero hero, ScreenStateApadter screenStateApadter) {
		this.interactive = interactive;
		this.hero = hero;
		this.screenStateApadter = screenStateApadter;
	}

	protected void stopInteractingObjects() {
		if (interactive instanceof AbstractMoveableGameObject) {
			stopObjectWhileTalking((AbstractMoveableGameObject) interactive);
		}
		if (hero instanceof AbstractMoveableGameObject) {
			stopObjectWhileTalking((AbstractMoveableGameObject) hero);
		}
	}

	public void setObjectKoordinates(int x, int y, AbstractMoveableGameObject moveable) {
		moveable.setX(x);
		moveable.setY(y);
		moveable.setDirection(Direction.NONE);
	}

	public void stopObjectWhileTalking(AbstractMoveableGameObject moveable) {
		setObjectKoordinates(moveable.getX(), moveable.getY(), moveable);
	}

	public Activatable getInteractive() {
		return interactive;
	}

	public Hero getHero() {
		return hero;
	}

	protected ScreenStateApadter getScreenStateApadter() {
		return screenStateApadter;
	}

	public boolean isOutOfRange() {
		return !hero.getExtendedBounds().intersects(interactive.getBounds());
	}

	public abstract void interact(TheRenderingMachine theRenderingMachine);

	public abstract void endInteraction(TheRenderingMachine theRenderingMachine);

	public abstract boolean isInteractionDone();
}
