package Interaction;

import NPCs.AbstractQuestGiver;
import engine.ScreenStateApadter;
import framework.ScreenStates;
import guiFrameWork.RenderingLevel;
import guiFrameWork.TheRenderingMachine;
import heroStuff.characterClasses.Hero;
import interactiveMenues.AbstractOptionManager;
import interactiveMenues.DialogManager;
import quests.Quest;
import quests.QuestLog;
import quests.QuestState;

public class QuestgiverInteraction extends Interaction {

    AbstractQuestGiver questGiver;
    private boolean activated = false;
    private QuestLog questLog;
    private DialogManager dlgMgr;
    private Quest quest;

    public QuestgiverInteraction(AbstractQuestGiver questGiver, QuestLog questLog, Hero hero,
            ScreenStateApadter screenStateApadter) {
        super(questGiver, hero, screenStateApadter);
        this.questGiver = questGiver;
        this.questLog = questLog;
        quest = questLog.getQuest(questGiver.getQuestKey());
    }

    public void interact(TheRenderingMachine theRenderingMachine) {
        dlgMgr = new DialogManager(quest);
        activated = true;
        dlgMgr.setStillTakingOptions(true);
        theRenderingMachine.registerRenderer(dlgMgr, RenderingLevel.INTERFACES);
        stopInteractingObjects();

        getScreenStateApadter().setScreenState(ScreenStates.INTERACTIVE_MENU);
    }

    public void endInteraction(TheRenderingMachine theRenderingMachine) {
        ((DialogManager) getOptionManager()).refreshAnimation();
        if (quest.getQuestState() == QuestState.closed && quest.getReward() != null) {
            getHero().retrieveQuestRewards(quest.getReward());
            quest.setReward(null);
        }
        theRenderingMachine.unregisterRenderer(getOptionManager(), RenderingLevel.INTERFACES);
        getScreenStateApadter().setScreenState(ScreenStates.inGame);
    }

    public boolean isInteractionDone() {
        if (activated && !isStillTakingOptions()) {
            return true;
        }
        return false;
    }

    public AbstractOptionManager<Quest> getOptionManager() {
        return dlgMgr;
    }

    private boolean isStillTakingOptions() {
        return dlgMgr.isStillTakingOptions();
    }
}