package Interaction;

import gameObjects.Chest;
import guiFrameWork.TheRenderingMachine;
import heroStuff.Inventory;
import heroStuff.characterClasses.Hero;

public class ChestInteraction extends Interaction {

    private Chest chest;
    private Inventory inventory;
    private boolean interactionDone;

    public ChestInteraction(Chest chest, Hero hero, Inventory inventory) {
        super(chest, hero, null);
        this.chest = chest;
        this.inventory = inventory;
        interactionDone = false;
    }

    public void interact(TheRenderingMachine theRenderingMachine) {
        if (!chest.isOpen()) {
            chest.setOpen(true);
            inventory.addItems(chest.getContent());
            chest.getContent().clear();
        }
        else {
            chest.setOpen(false);
        }
        interactionDone = true;
    }

    @Override
    public void endInteraction(TheRenderingMachine theRenderingMachine) {
    }

    @Override
    public boolean isInteractionDone() {
        return interactionDone;
    }
}
