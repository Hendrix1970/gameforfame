package Interaction;

import java.util.List;

import engine.ScreenStateApadter;
import fighting.Fight;
import fighting.FightState;
import fighting.FightStateAdapter;
import framework.AbstractFightable;
import framework.ScreenStates;
import guiFrameWork.RenderingLevel;
import guiFrameWork.TheRenderingMachine;
import heroStuff.characterClasses.Hero;
import heroStuff.skills.HeroSkill;
import interactiveMenues.AbstractOptionManager;
import interactiveMenues.FightingManager;

public class FightingInteraction extends Interaction {

	private Fight fight;
	private AbstractOptionManager<List<HeroSkill>> optionManager;

	public FightingInteraction(AbstractFightable fightable, Hero hero, ScreenStateApadter screenStateApadter) {
		super(fightable, hero, screenStateApadter);

		FightStateAdapter fightingStateAdapter = new FightStateAdapter(FightState.INIT);
		fight = new Fight(hero, fightable, fightingStateAdapter);
		optionManager = new FightingManager(fightable, fight, hero.getActiveSkills(), fightingStateAdapter);

		screenStateApadter.setScreenState(ScreenStates.FIGHT);
	}

	public void endInteraction(TheRenderingMachine theRenderingMachine) {
		theRenderingMachine.unregisterRenderer(getOptionManager(), RenderingLevel.INTERFACES);
		getScreenStateApadter().setScreenState(ScreenStates.inGame);

		cleanUpStep();

	}

	private void cleanUpStep() {
		// Here we can implement all the stuff that has to be reverted after a fight.
	}

	public boolean isInteractionDone() {
		return !fight.isStillFighting();
	}

	public AbstractOptionManager<List<HeroSkill>> getOptionManager() {
		return optionManager;
	}

	public void tick() {
		fight.tick();
	}

	@Override
	public void interact(TheRenderingMachine theRenderingMachine) {
		// nothing to do here. Interaction is started in the constructor
	}

	public void reward() {
		getHero().gainXP(((AbstractFightable) getInteractive()).getXpValue());

	}
}
