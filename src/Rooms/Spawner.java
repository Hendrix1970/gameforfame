package Rooms;

import java.awt.Image;
import java.util.ArrayList;
import java.util.List;

import javax.swing.ImageIcon;

import NPCs.TutorialGuy;
import NPCs.TutorialGuy2;
import Villains.Chicken;
import Villains.Ghost;
import Villains.NinjaChicken;
import constants.Constants;
import dataStructure.RoomDts;
import engine.Game;
import engine.GameHandler;
import engine.ScreenStateApadter;
import framework.AbstractGameObject;
import framework.AbstractMoveableGameObject;
import framework.LevelName;
import framework.ScreenStates;
import gameObjects.Chest;
import gameObjects.Door;
import gameObjects.HealingPotion;
import gameObjects.Table;
import guiFrameWork.RenderingLevel;
import guiFrameWork.TheRenderingMachine;
import guiFrameWork.renderer.ChestRenderer;
import guiFrameWork.renderer.ChickenRenderer;
import guiFrameWork.renderer.GhostRenderer;
import guiFrameWork.renderer.SequencedPicturesRenderer;

public class Spawner {

	public static final int OBJECT_STANDARD_SIZE = 15 * Constants.unit;
	public static final int SMALL_STANDARD_SIZE = 10 * Constants.unit;
	public static final int HUMAN_STANDARD_SIZE = 15 * Constants.unit;
	public static final int LARGE_STANDARD_SIZE = 20 * Constants.unit;

	private GameHandler handler;
	private Game game;
	private ScreenStateApadter screenStateApadter;

	private LevelName nextLevel = LevelName.room01;
	private RoomDesigner roomDesigner = new RoomDesigner();
	private List<RoomDts> rooms = new ArrayList<>();
	private Door originDoor;

	public Spawner(GameHandler handler, TheRenderingMachine theRenderingMachine, Game game,
			ScreenStateApadter screenStateApadter) {
		this.handler = handler;
		this.game = game;
		this.screenStateApadter = screenStateApadter;
		createRoomlist();
	}

	/**
	 * Befüllt den Handler mit Objekten, zum Bsp. beim Laden eines neuen Levels
	 */
	public void spawn() {
		handler.newRoom(getNextRoomDts());
		if (originDoor == null) {
			handler.setHeroKoordinates(200, (Constants.HEIGHT - OBJECT_STANDARD_SIZE) / 2);
		} else {
			handler.setHeroKoordinates(originDoor.getNewHeroKoordinatesX(), originDoor.getNewHeroKoordinatesY());
		}
		if (nextLevel == LevelName.loadingscreen) {
			screenStateApadter.setScreenState(ScreenStates.loading);
		}
	}

	/**
	 * Updated den Spawner wenn ein neues Level betreten wird.
	 *
	 * @param die Tür durch die das Level betreten wird
	 */

	public void loading(Door door) {
		screenStateApadter.setScreenState(ScreenStates.loading);
		nextLevel = door.getNextLevel();
		this.originDoor = door;
	}

	private RoomDts getNextRoomDts() {
		for (RoomDts room : rooms) {
			if (room.getName().equals(nextLevel)) {
				return room;
			}
		}
		return new RoomDts();
	}

	private void createRoomlist() {
		Image tableImage = new ImageIcon(getClass().getResource("/images/tisch.png")).getImage();

		{
			// room01
			roomDesigner.newRoom().withName(LevelName.room01).withWalls();

			Image tutorialGuy1Image = new ImageIcon(getClass().getResource("/images/tutorial_guy.png")).getImage();
			TutorialGuy tutorialGuy = new TutorialGuy(OBJECT_STANDARD_SIZE, OBJECT_STANDARD_SIZE + 2 * Constants.unit,
					HUMAN_STANDARD_SIZE, HUMAN_STANDARD_SIZE);
			roomDesigner.getObjectsMap().put(tutorialGuy,
					new SequencedPicturesRenderer(tutorialGuy, RenderingLevel.GAME_OBJECTS, tutorialGuy1Image));
			Table table = new Table(3 * OBJECT_STANDARD_SIZE + 2 * Constants.unit,
					OBJECT_STANDARD_SIZE + 2 * Constants.unit);
			roomDesigner.getObjectsMap().put(table,
					new SequencedPicturesRenderer(table, RenderingLevel.GAME_OBJECTS, tableImage));
			roomDesigner.stairsCentralEast(LevelName.room02, this);
			roomDesigner.fireplaces();

			AbstractGameObject chest = new Chest(7 * OBJECT_STANDARD_SIZE + 2 * Constants.unit,
					OBJECT_STANDARD_SIZE + 2 * Constants.unit, new HealingPotion());
			roomDesigner.getObjectsMap().put(chest, new ChestRenderer(chest));

			rooms.add(roomDesigner.finishRoom());
		}

		{
			// room02
			roomDesigner.newWalledRoomWithName(LevelName.room02);
			roomDesigner.doorCentralWest(LevelName.room01, this);
			roomDesigner.lockedDoorCentralNorth(LevelName.room03, this, "questEins");
			AbstractMoveableGameObject enemyChicken1 = new Chicken(700, 200)
					.withMovementBox(Spawner.SMALL_STANDARD_SIZE);
			roomDesigner.getObjectsMap().put(enemyChicken1, new ChickenRenderer(enemyChicken1));
			AbstractMoveableGameObject enemyChicken2 = new Chicken(400, 200)
					.withMovementBox(Spawner.SMALL_STANDARD_SIZE);
			roomDesigner.getObjectsMap().put(enemyChicken2, new ChickenRenderer(enemyChicken2));
			AbstractMoveableGameObject enemyChicken3 = new Chicken(200, 500)
					.withMovementBox(Spawner.SMALL_STANDARD_SIZE);
			roomDesigner.getObjectsMap().put(enemyChicken3, new ChickenRenderer(enemyChicken3));

			roomDesigner.fireplaces();
			rooms.add(roomDesigner.finishRoom());
		}

		{
			// room03
			roomDesigner.newWalledRoomWithName(LevelName.room03);
			roomDesigner.doorCentralSouth(LevelName.room02, this);
			roomDesigner.doorCentralEast(LevelName.room04, this);

			Image tutorialGuy2Image = new ImageIcon(getClass().getResource("/images/tutorial_guy2.png")).getImage();
			TutorialGuy2 tutorialGuy2 = new TutorialGuy2(3 * OBJECT_STANDARD_SIZE + 7 * Constants.unit,
					OBJECT_STANDARD_SIZE + 2 * Constants.unit, HUMAN_STANDARD_SIZE, HUMAN_STANDARD_SIZE);
			roomDesigner.getObjectsMap().put(tutorialGuy2,
					new SequencedPicturesRenderer(tutorialGuy2, RenderingLevel.GAME_OBJECTS, tutorialGuy2Image));

			// tables
			Table table = new Table(3 * OBJECT_STANDARD_SIZE - 7 * Constants.unit,
					3 * OBJECT_STANDARD_SIZE - 9 * Constants.unit);
			roomDesigner.getObjectsMap().put(table,
					new SequencedPicturesRenderer(table, RenderingLevel.GAME_OBJECTS, tableImage));
			Table table2 = new Table(7 * OBJECT_STANDARD_SIZE + 7 * Constants.unit,
					3 * OBJECT_STANDARD_SIZE - 9 * Constants.unit);
			roomDesigner.getObjectsMap().put(table2,
					new SequencedPicturesRenderer(table2, RenderingLevel.GAME_OBJECTS, tableImage));
			Table table3 = new Table(3 * OBJECT_STANDARD_SIZE - 7 * Constants.unit,
					5 * OBJECT_STANDARD_SIZE + 5 * Constants.unit);
			roomDesigner.getObjectsMap().put(table3,
					new SequencedPicturesRenderer(table3, RenderingLevel.GAME_OBJECTS, tableImage));
			Table table4 = new Table(7 * OBJECT_STANDARD_SIZE + 7 * Constants.unit,
					5 * OBJECT_STANDARD_SIZE + 5 * Constants.unit);
			roomDesigner.getObjectsMap().put(table4,
					new SequencedPicturesRenderer(table4, RenderingLevel.GAME_OBJECTS, tableImage));

			// healing potion
			Image healingPotionImage = new ImageIcon(getClass().getResource("/images/healing_potion.png")).getImage();
			HealingPotion healingPotion = new HealingPotion(3 * OBJECT_STANDARD_SIZE - 7 * Constants.unit,
					5 * OBJECT_STANDARD_SIZE + 5 * Constants.unit);
			roomDesigner.getObjectsMap().put(healingPotion,
					new SequencedPicturesRenderer(healingPotion, RenderingLevel.ITEMS, healingPotionImage));

			roomDesigner.fireplaces();
			rooms.add(roomDesigner.finishRoom());
		}

		{
			// room04
			roomDesigner.newWalledRoomWithName(LevelName.room04);
			roomDesigner.doorCentralWest(LevelName.room03, this);
			roomDesigner.doorCentralSouth(LevelName.room05, this);
			AbstractMoveableGameObject enemyChicken1 = new Chicken(500, 200)
					.withMovementBox(Spawner.SMALL_STANDARD_SIZE);
			roomDesigner.getObjectsMap().put(enemyChicken1, new ChickenRenderer(enemyChicken1));
			AbstractMoveableGameObject enemyChicken2 = new Chicken(400, 400)
					.withMovementBox(Spawner.SMALL_STANDARD_SIZE);
			roomDesigner.getObjectsMap().put(enemyChicken2, new ChickenRenderer(enemyChicken2));
			AbstractMoveableGameObject enemyChicken3 = new Chicken(200, 500)
					.withMovementBox(Spawner.SMALL_STANDARD_SIZE);
			roomDesigner.getObjectsMap().put(enemyChicken3, new ChickenRenderer(enemyChicken3));
			roomDesigner.fireplaces();
			rooms.add(roomDesigner.finishRoom());
		}

		{
			// room05
			roomDesigner.newWalledRoomWithName(LevelName.room05);
			roomDesigner.doorCentralNorth(LevelName.room04, this);
			roomDesigner.doorCentralSouth(LevelName.room06, this);
			AbstractMoveableGameObject enemyChicken1 = new NinjaChicken(500, 200)
					.withMovementBox(Spawner.SMALL_STANDARD_SIZE);
			roomDesigner.getObjectsMap().put(enemyChicken1, new ChickenRenderer(enemyChicken1));
			AbstractMoveableGameObject enemyChicken2 = new NinjaChicken(400, 400)
					.withMovementBox(Spawner.SMALL_STANDARD_SIZE);
			roomDesigner.getObjectsMap().put(enemyChicken2, new ChickenRenderer(enemyChicken2));
			AbstractMoveableGameObject enemyChicken3 = new NinjaChicken(200, 500)
					.withMovementBox(Spawner.SMALL_STANDARD_SIZE);
			roomDesigner.getObjectsMap().put(enemyChicken3, new ChickenRenderer(enemyChicken3));
			roomDesigner.fireplaces();
			rooms.add(roomDesigner.finishRoom());
		}

		{
			// room06
			roomDesigner.newWalledRoomWithName(LevelName.room06);
			roomDesigner.doorCentralNorth(LevelName.room05, this);
			AbstractMoveableGameObject enemyChicken1 = new Chicken(500, 200)
					.withMovementBox(Spawner.SMALL_STANDARD_SIZE);
			roomDesigner.getObjectsMap().put(enemyChicken1, new ChickenRenderer(enemyChicken1));
			AbstractMoveableGameObject enemyChicken2 = new Chicken(400, 400)
					.withMovementBox(Spawner.SMALL_STANDARD_SIZE);
			roomDesigner.getObjectsMap().put(enemyChicken2, new ChickenRenderer(enemyChicken2));
			AbstractMoveableGameObject enemyChicken3 = new Chicken(200, 500)
					.withMovementBox(Spawner.SMALL_STANDARD_SIZE);
			roomDesigner.getObjectsMap().put(enemyChicken3, new ChickenRenderer(enemyChicken3));
			AbstractMoveableGameObject endBoss = new Ghost(700, 400);
			roomDesigner.getObjectsMap().put(endBoss, new GhostRenderer(endBoss));
			roomDesigner.fireplaces();
			rooms.add(roomDesigner.finishRoom());
		}
	}

}
