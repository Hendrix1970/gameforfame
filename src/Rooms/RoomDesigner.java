package Rooms;

import java.awt.Image;
import java.util.HashMap;
import java.util.Map;

import javax.swing.ImageIcon;

import constants.Constants;
import dataStructure.RoomDts;
import framework.AbstractGameObject;
import framework.DoorOrientation;
import framework.LevelName;
import gameObjects.Door;
import gameObjects.FirePlace;
import gameObjects.LockedDoor;
import gameObjects.Wall;
import guiFrameWork.RenderingLevel;
import guiFrameWork.renderer.FirePlaceRenderer;
import guiFrameWork.renderer.Renderer;
import guiFrameWork.renderer.SequencedPicturesRenderer;

public class RoomDesigner {

	private RoomDts room;

	private Map<AbstractGameObject, Renderer> objects;

	private Renderer fireRenderer1;

	private FirePlace firePlace1;

	private FirePlace firePlace2;

	private Renderer fireRenderer2;

	public RoomDesigner() {
		room = new RoomDts();
		objects = new HashMap<>();

		firePlace1 = new FirePlace(Spawner.OBJECT_STANDARD_SIZE, Constants.HEIGHT - 2 * Spawner.OBJECT_STANDARD_SIZE);
		firePlace2 = new FirePlace(Constants.WIDTH - 2 * Spawner.OBJECT_STANDARD_SIZE, Spawner.OBJECT_STANDARD_SIZE);
		fireRenderer1 = new FirePlaceRenderer(firePlace1);
		fireRenderer2 = new FirePlaceRenderer(firePlace2);
	}

	/**
	 * Resetet den RoomDesigner, es kann ein neuer Raum gebaut werden.
	 */
	protected RoomDesigner newRoom() {
		this.room = new RoomDts();
		objects = new HashMap<>();
		return this;
	}

	/**
	 * Resetet den RoomDesigner, erzeugt einen neuen Raum mit Wänden der einen Namen
	 * hat.
	 *
	 * @param der Name des neuen Raums
	 */

	protected void newWalledRoomWithName(LevelName name) {
		this.room = new RoomDts();
		room.setName(name);
		objects = new HashMap<>();
		withWalls();
	}

	protected RoomDesigner withName(LevelName name) {
		room.setName(name);
		return this;
	}

	/**
	 * Erstellt einen RoomDts und gibt diesen zurück.
	 *
	 * @return den Raum in Dts-Form
	 */
	protected RoomDts finishRoom() {
		room.setObjects(objects);
		return room;
	}

	protected RoomDesigner withWalls() {
		objects.put(new Wall(0, 0, Constants.WIDTH, Spawner.OBJECT_STANDARD_SIZE), null);
		objects.put(new Wall(0, Constants.HEIGHT - Spawner.OBJECT_STANDARD_SIZE, Constants.WIDTH,
				Spawner.OBJECT_STANDARD_SIZE), null);
		objects.put(new Wall(0, 0, Spawner.OBJECT_STANDARD_SIZE, Constants.HEIGHT), null);
		objects.put(new Wall(Constants.WIDTH - Spawner.OBJECT_STANDARD_SIZE, 0, Spawner.OBJECT_STANDARD_SIZE,
				Constants.HEIGHT), null);
		return this;
	}

	protected void doorCentralEast(LevelName levelName, Spawner spawner) {
		Image image = new ImageIcon(getClass().getResource("/images/door_r.png")).getImage();
		Door door = new Door(Constants.WIDTH - Spawner.OBJECT_STANDARD_SIZE,
				(Constants.HEIGHT - Spawner.OBJECT_STANDARD_SIZE) / 2, Spawner.OBJECT_STANDARD_SIZE,
				Spawner.OBJECT_STANDARD_SIZE, spawner, levelName, DoorOrientation.east);
		Renderer renderer = new SequencedPicturesRenderer(door, RenderingLevel.GAME_OBJECTS, image);
		objects.put(door, renderer);
	}

	protected void stairsCentralEast(LevelName levelName, Spawner spawner) {
		Image image = new ImageIcon(getClass().getResource("/images/stairs2_r.png")).getImage();
		int height = 121;
		Door door = new Door(Constants.WIDTH - Spawner.OBJECT_STANDARD_SIZE, (Constants.HEIGHT - height) / 2,
				Spawner.OBJECT_STANDARD_SIZE, height, spawner, levelName, DoorOrientation.east);
		Renderer renderer = new SequencedPicturesRenderer(door, RenderingLevel.GAME_OBJECTS, image);
		objects.put(door, renderer);
	}

	protected void doorCentralWest(LevelName levelName, Spawner spawner) {
		Image image = new ImageIcon(getClass().getResource("/images/door_l.png")).getImage();

		Door door = new Door(0, (Constants.HEIGHT - Spawner.OBJECT_STANDARD_SIZE) / 2, Spawner.OBJECT_STANDARD_SIZE,
				Spawner.OBJECT_STANDARD_SIZE, spawner, levelName, DoorOrientation.west);
		Renderer renderer = new SequencedPicturesRenderer(door, RenderingLevel.GAME_OBJECTS, image);
		objects.put(door, renderer);
	}

	protected void doorCentralNorth(LevelName levelName, Spawner spawner) {
		Image image = new ImageIcon(getClass().getResource("/images/door_t.png")).getImage();
		Door door = new Door((Constants.WIDTH - Spawner.OBJECT_STANDARD_SIZE) / 2, 0, Spawner.OBJECT_STANDARD_SIZE,
				Spawner.OBJECT_STANDARD_SIZE, spawner, levelName, DoorOrientation.north);
		Renderer renderer = new SequencedPicturesRenderer(door, RenderingLevel.GAME_OBJECTS, image);
		objects.put(door, renderer);
	}

	protected void lockedDoorCentralNorth(LevelName levelName, Spawner spawner, String code) {
		Image image = new ImageIcon(getClass().getResource("/images/door_t.png")).getImage();
		Door door = new LockedDoor((Constants.WIDTH - Spawner.OBJECT_STANDARD_SIZE) / 2, 0,
				Spawner.OBJECT_STANDARD_SIZE, Spawner.OBJECT_STANDARD_SIZE, spawner, levelName, DoorOrientation.north,
				code);
		Renderer renderer = new SequencedPicturesRenderer(door, RenderingLevel.GAME_OBJECTS, image);
		objects.put(door, renderer);
	}

	protected void doorCentralSouth(LevelName levelName, Spawner spawner) {
		Image image = new ImageIcon(getClass().getResource("/images/door_b.png")).getImage();
		Door door = new Door((Constants.WIDTH - Spawner.OBJECT_STANDARD_SIZE) / 2,
				Constants.HEIGHT - Spawner.OBJECT_STANDARD_SIZE, Spawner.OBJECT_STANDARD_SIZE,
				Spawner.OBJECT_STANDARD_SIZE, spawner, levelName, DoorOrientation.south);
		Renderer renderer = new SequencedPicturesRenderer(door, RenderingLevel.GAME_OBJECTS, image);
		objects.put(door, renderer);
	}

	protected void fireplaces() {
		objects.put(firePlace1, fireRenderer1);
		objects.put(firePlace2, fireRenderer2);
	}

	public Map<AbstractGameObject, Renderer> getObjectsMap() {
		return objects;
	}
}
