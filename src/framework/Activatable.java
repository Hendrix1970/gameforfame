package framework;

import java.awt.Rectangle;

public interface Activatable {

	public Rectangle getBounds();

}
