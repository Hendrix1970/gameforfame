package framework;

import gameObjects.Items.Item;

import java.util.ArrayList;

public class ItemList<I extends Item> extends ArrayList<I> {

	private static final long serialVersionUID = 4632688254528098505L;

	public I getByName(String name) {
		for(I item : this) {
			if(item.getName().equals(name)) {
				return item;
			}
		}
		return null;
	}
}
