package framework;

import constants.Constants;

public abstract class AbstractMoveableGameObject extends AbstractGameObject {

	protected int speed = 5;
	private Collide collide = new Collide();
	protected Direction direction;
	private int continousMovement = 0;
	private int baseSpeed;
	private int squareX;
	private int squareY;
	private boolean movementBox = false;
	private int movementDistance = 0;

	public AbstractMoveableGameObject withMovementBox(int distance) {
		movementBox = true;
		squareX = x;
		squareY = y;
		this.setMovementDistance(distance);
		return this;
	}

	public int getSquareX() {
		return squareX;
	}

	public void setSquareX(int squareX) {
		this.squareX = squareX;
	}

	public int getSquareY() {
		return squareY;
	}

	public void setSquareY(int squareY) {
		this.squareY = squareY;
	}

	public AbstractMoveableGameObject(int x, int y, int height, int width, int baseSpeed) {
		super(x, y, height, width);
		this.baseSpeed = baseSpeed;
	}

	protected boolean isOnScreen() {
		if ((x + getSpeedX()) < 0 || (y + getSpeedY()) < 0 || (x + getSpeedX()) > Constants.WIDTH - width
				|| (y + getSpeedY()) > Constants.HEIGHT - height) {
			return false;
		} else {
			return true;
		}
	}

	public void resetCollisions() {
		collide.reset();
	}

	public void move() {
		if (isOnScreen() && !collide.isHorizontal()) {
			x = x + getSpeedX();
		}
		if (isOnScreen() && !collide.isVertical()) {
			y = y + getSpeedY();
		}
	}

	public void collide(HorizontalVertical direction) {
		collide.collide(direction);
	}

	public int getSpeedX() {
		int speedX = 0;
		if (getDirection() == Direction.WEST) {
			speedX = -baseSpeed;
		} else if (getDirection() == Direction.EAST) {
			speedX = baseSpeed;
		}
		return speedX;
	}

	public int getSpeedY() {
		int speedY = 0;
		if (getDirection() == Direction.NORTH) {
			speedY = -baseSpeed;
		} else if (getDirection() == Direction.SOUTH) {
			speedY = baseSpeed;
		}
		return speedY;
	}

	public Direction getDirection() {
		return direction;
	}

	public void setDirection(Direction direction) {
		this.direction = direction;
	}

	public int getContinousMovement() {
		return continousMovement;
	}

	public void setContinousMovement(int conitousMovement) {
		this.continousMovement = conitousMovement;
	}

	public void reduceContinousMovement() {
		this.continousMovement -= 1;
	}

	public int getMovementDistance() {
		return movementDistance;
	}

	private void setMovementDistance(int movementDistance) {
		this.movementDistance = movementDistance;
	}

	public boolean isMovementBox() {
		return movementBox;
	}

}
