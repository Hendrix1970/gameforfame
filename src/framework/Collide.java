package framework;

public class Collide {
	boolean horizontal = false;
	boolean vertical = false;

	public boolean get(HorizontalVertical direction) {
		switch (direction) {
		case HORIZONTAL:
			return horizontal;
		case VERTICAL:
			return vertical;
		default:
			return false;
		}
	}

	public void collide(HorizontalVertical direction) {
		switch (direction) {
		case HORIZONTAL:
			setHorizontal(true);
			break;
		case VERTICAL:
			setVertical(true);
			break;
		}
	}

	public void reset() {
		horizontal = false;
		vertical = false;
	}

	public boolean isHorizontal() {
		return horizontal;
	}

	public void setHorizontal(boolean horizontal) {
		this.horizontal = horizontal;
	}

	public boolean isVertical() {
		return vertical;
	}

	public void setVertical(boolean vertical) {
		this.vertical = vertical;
	}

}
