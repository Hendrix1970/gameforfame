package framework;

import java.util.ArrayList;
import java.util.List;

import Villains.VillainSkill;
import dataStructure.FightingAttributesDts;
import quests.EnemyType;

public abstract class AbstractFightable extends AbstractMoveableGameObject implements Activatable {

    // Grund Attribute
    private int health;
    private int defense;
    private int maxHealth;

    // Primäre Attribute
    private int strength;
    private int intelligence;
    private int dexterity;

    // depricated
    private int damage;

    private int xpValue;
    protected EnemyType enemyType;

    private boolean alive = true;

    private List<VillainSkill> skills = new ArrayList<>();

    /**
     * 
     * @param x
     * @param y
     * @param height
     * @param width
     * @param fightingDts
     * @param game
     * @param strenght
     * @param intelligence
     * @param dexterity
     */
    public AbstractFightable(int x, int y, int height, int width, FightingAttributesDts fightingDts, int strenght, int intelligence, int dexterity,
            int baseSpeed) {
        super(x, y, height, width, baseSpeed);

        this.health = fightingDts.getHealth();
        this.maxHealth = fightingDts.getHealth();
        this.damage = fightingDts.getDamage();
        this.defense = fightingDts.getDefense();
        this.strength = strenght;
        this.intelligence = intelligence;
        this.dexterity = dexterity;

        init();
    }

    protected abstract void init();

    @Override
    public abstract void tick();

    public void die() {
        alive = false;
    }

    public boolean isAlive() {
        return alive;
    }

    public void setAlive(boolean alive) {
        this.alive = alive;
    }

    public int getHealth() {
        return health;
    }

    public void setHealth(int health) {
        this.health = health;
    }

    public int getDamage() {
        return damage;
    }

    public void setDamage(int strength) {
        this.damage = strength;
    }

    public int getDefense() {
        return defense;
    }

    public void setDefense(int defense) {
        this.defense = defense;
    }

    public int getXpValue() {
        return xpValue;
    }

    public void setXpValue(int xp) {
        this.xpValue = xp;
    }

    public EnemyType getEnemyType() {
        return enemyType;
    }

    public int getStrength() {
        return strength;
    }

    public void setStrength(int strength) {
        this.strength = strength;
    }

    public int getIntelligence() {
        return intelligence;
    }

    public void setIntelligence(int intelligence) {
        this.intelligence = intelligence;
    }

    public int getDexterity() {
        return dexterity;
    }

    public void setDexterity(int dexterity) {
        this.dexterity = dexterity;
    }

    public int getMaxHealth() {
        return maxHealth;
    }

    public void setMaxHealth(int maxHealth) {
        this.maxHealth = maxHealth;
    }

    public List<VillainSkill> getSkills() {
        return skills;
    }

    public void setSkills(List<VillainSkill> skills) {
        this.skills = skills;
    }

    abstract public String retrieveText(TextTile textTile);

}
