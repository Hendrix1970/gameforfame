package framework;

import guiFrameWork.RenderingLevel;
import guiFrameWork.renderer.AbstractRenderer;

public abstract class AbstractMenuRenderer extends AbstractRenderer {

	public AbstractMenuRenderer() {
		super(RenderingLevel.INTERFACES, 0, 0);
	}

}
