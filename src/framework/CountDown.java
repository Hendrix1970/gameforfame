package framework;

public class CountDown {

	long timer;
	private double seconds;
	boolean timeReached;

	public CountDown(int seconds) {
		this.seconds = (double) seconds;
		timer = System.currentTimeMillis();
	}

	public CountDown(double seconds) {
		this.seconds = seconds;
		timer = System.currentTimeMillis();
	}

	public boolean timeReached() {
		// this check is only for performance reasons.
		if (!timeReached) {
			timeReached = (System.currentTimeMillis() - timer) > (seconds * 1000);
		}
		return timeReached;
	}
}
