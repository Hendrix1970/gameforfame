package framework;

public enum Direction {
	NORTH, EAST, SOUTH, WEST, NONE;
}
