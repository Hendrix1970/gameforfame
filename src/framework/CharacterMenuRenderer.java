package framework;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;

import javax.swing.ImageIcon;

import Rooms.Spawner;
import Widgets.TextBound;
import Widgets.TextLength;
import Widgets.TextWidget;
import guiFrameWork.FontUtils;
import guiFrameWork.RenderingLevel;
import guiFrameWork.renderer.AbstractRenderer;
import heroStuff.characterClasses.Hero;

public class CharacterMenuRenderer extends AbstractRenderer {

	private Image menuBackground;
	private Image characterBackground;
	private Hero hero;
	private Font font_42;
	private final int xLeft = 215;
	private final int xMiddle = 505;
	private final int xRight = 900;
	private final int yTop = 301;
	private final int yMid = 377;
	private final int yBot = 454;

	TextWidget healthWidget;
	private TextWidget xpWidget;
	private TextWidget levelWidget;

	public CharacterMenuRenderer(Hero hero) {
		super(RenderingLevel.INTERFACES, Spawner.OBJECT_STANDARD_SIZE, Spawner.OBJECT_STANDARD_SIZE);
		menuBackground = new ImageIcon(getClass().getResource("/images/menu-fenster.png")).getImage();
		characterBackground = new ImageIcon(getClass().getResource("/images/charakter-menu.png")).getImage();
		this.hero = hero;
		font_42 = FontUtils.getStandardFontInSize(42);

		initWidgets();
	}

	private void initWidgets() {
		healthWidget = new TextWidget("" + hero.getHealth(), xRight - TextLength.MEDIUM.length(), yTop)
				.withTextBound(TextBound.RIGHT);
		xpWidget = new TextWidget("" + hero.getXp(), xRight - TextLength.MEDIUM.length(), yMid)
				.withTextBound(TextBound.RIGHT);
		levelWidget = new TextWidget("" + hero.getLevel(), xRight - TextLength.MEDIUM.length(), yBot)
				.withTextBound(TextBound.RIGHT);

	}

	public void render(Graphics g) {
		updateWidgets();
		drawImageFromStdSize(menuBackground, 0, 0, g);
		drawImageFromStdSize(characterBackground, 0, 0, g);
		g.setFont(font_42);
		g.setColor(Color.white);

		drawResponsiveString("" + hero.getStrength(), xLeft, yTop, g);
		drawResponsiveString("" + hero.getDexterity(), xLeft, yMid, g);
		drawResponsiveString("" + hero.getIntelligence(), xLeft, yBot, g);

		drawResponsiveString("" + hero.getDamage(), xMiddle, yTop, g);
		drawResponsiveString("" + hero.getDefense(), xMiddle, yMid, g);

		healthWidget.draw(g);
		xpWidget.draw(g);
		levelWidget.draw(g);
	}

	private void updateWidgets() {
		healthWidget.setText("" + hero.getHealth());
		xpWidget.setText("" + hero.getXp());
		levelWidget.setText("" + hero.getLevel());

	}

	@Override
	protected void initImages() {
	}
}
