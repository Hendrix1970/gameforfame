package framework;

public interface Unlockable {

	public boolean unlock(String code);

	public boolean isLocked();

	public void setLocked(boolean locked);

	public String getCode();

	public void setCode(String code);
}
