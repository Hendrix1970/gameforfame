package framework;

public enum TextTile {
	
	VILLAIN_OPTION, DAMAGE_DEALT, DAMAGE_MISSED, END_FIGHT;

}
