package framework;

public enum ScreenStates {
    inGame,
    loading,
    INTERACTIVE_MENU,
    characterMenu,
    questLog,
    inventar,
    FIGHT,
    dead
}
