package framework;

import java.awt.Graphics;
import java.awt.Image;

import guiFrameWork.RenderingLevel;
import guiFrameWork.renderer.AbstractRenderer;

public class BackroundRenderer extends AbstractRenderer {

	private Image background;

	public BackroundRenderer() {
		super(null, RenderingLevel.BACKGROUND);
	}

	@Override
	public void render(Graphics g) {
		drawImage(background, 0, 0, g);

	}

	@Override
	protected void initImages() {
		background = autoResizeImage("background_01.jpg");
		super.initImages();
	}
}
