package constants;

public class Constants {

    public static final int unit = 5;
    public static final int WIDTH = 255 * unit; // 1275
    public static final int HEIGHT = 150 * unit; // 750

    public static final int SMALL_MENU_X = 0;
    public static final int SMALL_MENU_Y = HEIGHT - 45 * unit;

    public static final int LARGE_MENU_X = 15 * unit;
    public static final int LARGE_MENU_Y = 15 * unit;

    public static final int PICTURE_STANDARD_RES_X = 1920;
    public static final int PICTURE_STANDARD_RES_Y = 1080;

    public static final int DIALOG_OPTION_STANDARD_X = 34 * unit;
    public static final int DIALOG_OPTION_STANDARD_Y = HEIGHT - 24 * unit;
}
