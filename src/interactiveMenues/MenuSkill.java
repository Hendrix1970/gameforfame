package interactiveMenues;

import java.awt.Image;

import heroStuff.skills.HeroSkill;

public class MenuSkill implements Option {

    private int position;
    private HeroSkill skill;

    public MenuSkill(int position, HeroSkill item) {
        this.position = position;
        this.skill = item;
    }

    public boolean isSelectable() {
        return true;
    }

    @Override
    public void execute() {

    }

    @Override
    public int getPosition() {
        return position;
    }

    @Override
    public String getText() {
        return skill.getName();
    }

    public HeroSkill getSkill() {
        return skill;
    }

    public Image getImage() {
        return skill.getPortraitForInventar();
    }

}
