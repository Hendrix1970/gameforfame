package interactiveMenues;

import quests.Quest;
import quests.QuestState;

public class Answer implements Option {
    private String text;
    private int position;
    private int verticeToGoTo;
    private boolean exitPoint = false;
    private Quest quest;
    private String questName = null;

    public Answer(String text, int position, int verticeToGoTo) {
        this.text = text;
        this.position = position;
        this.verticeToGoTo = verticeToGoTo;
    }

    public Answer(String text, int position, int verticeToGoTo, boolean exitPoint) {
        this.text = text;
        this.position = position;
        this.verticeToGoTo = verticeToGoTo;
        this.exitPoint = exitPoint;
    }

    public Answer(String text, int position, int verticeToGoTo, boolean exitPoint, Quest quest, String questName) {
        this.text = text;
        this.position = position;
        this.verticeToGoTo = verticeToGoTo;
        this.exitPoint = exitPoint;
        this.quest = quest;
        this.questName = questName;
    }

    public void execute() {
        if (quest.getQuestState() == QuestState.newQuest) {
            quest.setQuestState(QuestState.inProgress);
        }
        else if (quest.getQuestState() == QuestState.done) {
            quest.setQuestState(QuestState.closed);
        }
    }

    public boolean isSelectable() {
        return true;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public int getVerticeToGoTo() {
        return verticeToGoTo;
    }

    public void setVerticeToGoTo(int dialogVertice) {
        this.verticeToGoTo = dialogVertice;
    }

    public boolean isExitPoint() {
        return exitPoint;
    }

    public void setExitPoint(boolean exitPoint) {
        this.exitPoint = exitPoint;
    }

    public String getQuestName() {
        return questName;
    }

    public void setQuest(String questName) {
        this.questName = questName;
    }
}
