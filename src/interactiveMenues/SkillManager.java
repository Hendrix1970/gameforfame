package interactiveMenues;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.util.List;

import Rooms.Spawner;
import constants.Constants;
import guiFrameWork.ResolutionUtil;
import heroStuff.skills.HeroSkill;

public class SkillManager extends AbstractScrollableOptionManager {

    private Image selctedHeroSkillBG;
    private Image backGround;

    private Color lightBlue = new Color(29, 35, 72);
    private Color darkBlue = new Color(16, 21, 53);

    int indexEquippedHeroSkills = 0;

    private List<HeroSkill> classSkills;

    private List<HeroSkill> activeSkills;

    public SkillManager(List<HeroSkill> classSkills, List<HeroSkill> activeSkills) {
        super(classSkills);
        this.classSkills = classSkills;
        this.activeSkills = activeSkills;
        setUpOptions(null);
    }

    protected void initImages() {
        super.initImages();
        backGround = autoResizeImageFromStandardSize("menu-quests.png");
        unselctedItemBG = autoResizeImageFromStandardSize("menu-inventar-itembox.png");
        selctedHeroSkillBG = autoResizeImageFromStandardSize("menu-inventar-item-selected.png");
    }

    public void selectOption() {
        HeroSkill skill = ((MenuSkill) options.get(index)).getSkill();
        if (!skill.isUnlocked()) {
            return;
        }
        if (activeSkills.contains(skill)) {
            activeSkills.remove(skill);
        }
        else if (activeSkills.size() < 4) {
            activeSkills.add(skill);
        }
    }

    @Override
    public void renderSpecificData(Graphics g) {
        drawImage(backGround, 0, 0, g);
        showHeroSkills(g);
        showData(g);
    }

    protected void showHeroSkills(Graphics g) {
        g.setFont(getFont(48));
        g.setColor(Color.WHITE);
        for (Option option : options) {
            MenuSkill heroSkill = ((MenuSkill) option);
            g.setColor(determineColor(heroSkill.getSkill()));
            int y = Constants.HEIGHT - 98 * Constants.unit + (options.size() - option.getPosition()) * 78;
            int x = Spawner.OBJECT_STANDARD_SIZE + 24 * Constants.unit + 2;
            drawListHeroSkill(heroSkill.getImage(), x, y, g);
            if (option.getPosition() - 1 == index) {
                drawImage(selctedHeroSkillBG, x, y, g);
            }
            else {
                drawImage(unselctedItemBG, x, y, g);
            }
            drawResponsiveString(heroSkill.getText(), x + 68, y + 37, g);
        }
    }

    private Color determineColor(HeroSkill skill) {
        if (!skill.isUnlocked()) {
            return Color.GRAY;
        }
        else if (activeSkills.contains(skill)) {
            return Color.GREEN;
        }
        else {
            return Color.WHITE;
        }
    }

    private void showData(Graphics g) {
        HeroSkill heroSkill = determineHeroSkillForToShowData();

        if (heroSkill == null)
            return;

        // Header
        g.setFont(getFont(30));
        g.setColor(darkBlue);
        drawResponsiveString(heroSkill.getName(), 870, 300, g);

        // Description
        g.setColor(lightBlue);
        g.setFont(getFont(25));
        drawResponsiveStringWithLinebreak(heroSkill.getDescription() != null ? heroSkill.getDescription() : "", 870, 350, 32, g);
    }

    private HeroSkill determineHeroSkillForToShowData() {
        return ((MenuSkill) options.get(index)).getSkill();
    }

    // TODO make it happen: g.getFontMetrics().stringWidth(substring)

    private void drawListHeroSkill(Image image, int x, int y, Graphics g) {
        drawResponsiveImage(ResolutionUtil.manuallyResizeImage(image, 50, 50), x + 5, y + 5, g);
    }

    @Override
    protected void fillOptions(int i) {
        options.add(new MenuSkill(i + 1, (HeroSkill) classSkills.get(classSkills.size() - 1 - (relativeIndex + i))));
    }
}
