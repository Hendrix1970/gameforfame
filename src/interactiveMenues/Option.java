package interactiveMenues;

public interface Option {

	public void execute();

	public int getPosition();

	public String getText();

	public boolean isSelectable();

}
