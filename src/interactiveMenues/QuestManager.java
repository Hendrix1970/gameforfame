package interactiveMenues;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;

import Rooms.Spawner;
import constants.Constants;
import quests.Quest;
import quests.QuestLog;
import quests.QuestState;

public class QuestManager extends AbstractScrollableOptionManager {

    private Image questLogBG;
    private Image selctedItemBG;

    private Quest questToShow;

    public QuestManager(QuestLog questLog) {
        super(questLog.getActiveQuests());
        initImages();
        setUpOptions(null);
        questToShow = options.isEmpty() ? null : ((MenuQuest) options.get(index)).getQuest();
    }

    protected void initImages() {
        super.initImages();
        questLogBG = autoResizeImageFromStandardSize("menu-quests.png");
        unselctedItemBG = autoResizeImageFromStandardSize("menu-quests-box.png");
        selctedItemBG = autoResizeImageFromStandardSize("menu-quests-box-selected.png");
    }

    @Override
    public void optionUp() {
        super.optionUp();
        selectOption();
    }

    @Override
    public void optionDown() {
        super.optionDown();
        selectOption();
    }

    @Override
    public void selectOption() {
        if (!options.isEmpty()) {
            MenuQuest menuQuest = (MenuQuest) options.get(index);
            questToShow = menuQuest.getQuest();
        }
    }

    @Override
    protected void renderSpecificData(Graphics g) {
        showListItems(g);
        drawImage(questLogBG, 0, 0, g);
        showData(g);
        refreshOptions();

    }

    private void showData(Graphics g) {
        g.setColor(Color.black);
        g.setFont(getFont(32));
        int y = Constants.HEIGHT - 98 * Constants.unit;
        if (questToShow != null) {
            drawString(questToShow.getStatusText(), 870, y + 50, g);
        }
        else {
            drawString("Sie haben keine \n aktiven Quests.", 870, y + 50, g);
        }
        g.setFont(getFont(48));
    }

    protected void showListItems(Graphics g) {
        g.setFont(getFont(48));
        for (Option option : options) {
            MenuQuest quest = ((MenuQuest) option);
            if (quest.getQuest().getQuestState() == QuestState.inProgress) {
                g.setColor(Color.WHITE);
            }
            else if (quest.getQuest().getQuestState() == QuestState.done) {
                g.setColor(Color.GREEN);
            }
            else if (quest.getQuest().getQuestState() == QuestState.closed) {
                g.setColor(Color.GRAY);
            }
            int y = Constants.HEIGHT - 98 * Constants.unit + (options.size() - option.getPosition()) * 78;
            int x = Spawner.OBJECT_STANDARD_SIZE + 18 * Constants.unit + 32;
            if (option.getPosition() - 1 == index) {
                drawImage(selctedItemBG, x, y, g);
            }
            else {
                drawImage(unselctedItemBG, x, y, g);
            }
            drawResponsiveString(quest.getQuest().getName(), x + 48, y + 37, g);
        }
    }

    @Override
    protected void fillOptions(int i) {
        options.add(new MenuQuest(i + 1, (Quest) getItemList().get(relativeIndex + i)));
    }
}
