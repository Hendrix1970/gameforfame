package interactiveMenues;

import java.awt.Image;

import gameObjects.Items.Item;

public class MenuItem implements Option {

    private int position;
    private Item item;

    public MenuItem(int position, Item item) {
        this.position = position;
        this.item = item;
    }

    public boolean isSelectable() {
        return true;
    }

    @Override
    public void execute() {

    }

    @Override
    public int getPosition() {
        return position;
    }

    @Override
    public String getText() {
        return item.getName();
    }

    public Item getItem() {
        return item;
    }

    public Image getImage() {
        return item.getPortraitForInventar();
    }

}
