package interactiveMenues;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.util.ArrayList;
import java.util.List;

import Rooms.Spawner;
import Villains.Skill;
import constants.Constants;
import dataStructure.SkillModelBean;
import fighting.Fight;
import fighting.FightState;
import fighting.FightStateAdapter;
import framework.AbstractFightable;
import framework.AbstractMoveableGameObject;
import guiFrameWork.FontUtils;
import guiFrameWork.ResolutionUtil;
import guiFrameWork.renderer.LifeBarRenderer;
import guiFrameWork.renderer.LifeBarRendererVillaine;
import guiFrameWork.renderer.LoadingRenderer;
import guiFrameWork.renderer.MiniGameRenderer;
import heroStuff.skills.HeroSkill;

public class FightingManager extends AbstractOptionManager<List<HeroSkill>> {

    private static final int X_CONSTANT = 3 * Spawner.OBJECT_STANDARD_SIZE - 7 * Constants.unit;
    private static final int Y_CONSTANT = Constants.HEIGHT - (Spawner.OBJECT_STANDARD_SIZE * 2) + 5;
    private List<HeroSkill> skills;
    private AbstractMoveableGameObject villain;
    private Fight fight;
    private FightStateAdapter fightStateAdapter;
    private LoadingRenderer loadingRenderer;
    private LifeBarRenderer heroLife;
    private LifeBarRendererVillaine villaineLife;

    private boolean halfwayDone = false;
    private boolean loadingDone = false;

    private Font font_42;

    private MiniGameRenderer miniGameRenderer = null;

    public FightingManager(AbstractFightable villain, Fight fight, List<HeroSkill> skills,
            FightStateAdapter fightStateAdapter) {
        this.villain = villain;
        this.fight = fight;
        this.fightStateAdapter = fightStateAdapter;
        this.loadingRenderer = new LoadingRenderer();
        heroLife = new LifeBarRenderer(fight.getHero());
        villaineLife = new LifeBarRendererVillaine(villain);

        setUpOptions(skills);

        determineFonts();
    }

    @Override
    protected void initImages() {
        menuBG = autoResizeImageFromStandardSize("dialog.png");
        selectedItemBG = autoResizeImageFromStandardSize("dialog-itembox-selected.png");
        unselctedItemBG = autoResizeImageFromStandardSize("dialog-itembox.png");
        selectedItemBGHalf = ResolutionUtil.factorizeImageWidth(0.45, selectedItemBG);
        unselctedItemBGHalf = ResolutionUtil.factorizeImageWidth(0.45, unselctedItemBG);
    }

    private void determineFonts() {
        font_42 = FontUtils.getStandardFontInSize(42);
    }

    @Override
    public void setUpOptions(List<HeroSkill> skills) {
        if (skills.size() > 4) {
            System.out.println("Der Held hat mehr als 4 Skills.");
        }
        this.skills = skills;
        this.options.addAll(assembleOptions());
    }

    @Override
    public void optionUp() {
        if (fightStateAdapter.getState() == FightState.HERO_OPTION) {
            if (index % 2 != 0 && index > 0) {
                index--;
            }
        }
    }

    @Override
    public void optionDown() {
        if (fightStateAdapter.getState() == FightState.HERO_OPTION) {
            if (index % 2 != 1 && index < options.size() - 1) {
                index++;
            }
        }
    }

    @Override
    public void optionLeft() {
        if (fightStateAdapter.getState() == FightState.HERO_OPTION) {
            if (index > 1)
                index -= 2;
        }
    }

    @Override
    public void optionRight() {
        if (fightStateAdapter.getState() == FightState.HERO_OPTION) {
            if (index < 2 && options.size() > index + 2)
                index += 2;
        }
    }

    @Override
    public void selectOption() {
        fight.registerSelection(index);
    }

    @Override
    public void render(Graphics g) {
        FightState state = fightStateAdapter.getState();

        if (halfwayDone) {
            g.setColor(Color.pink);
            g.fillRect(0, 0, getResolution().getWidth(), getResolution().getHeight());
            drawImage(menuBG, 0, 0, g);
            g.setColor(Color.WHITE);
            g.setFont(FontUtils.getStandardFontInSize(65));
            drawString("Hier könnte eine schöne Animation von Dir sein.\n Du Lumpen!", X_CONSTANT, 350, g);
            g.setFont(font_42);
            heroLife.renderAll(g);
            villaineLife.renderAll(g);
        }

        if (state == FightState.INIT) {
            renderInit(g);
        }
        else if (state == FightState.HERO_OPTION) {
            renderHeroOption(g);
        }
        else if (state == FightState.MINI_GAME) {
            renderMiniGame(g);
        }
        else if (state == FightState.HERO_INFO) {
            renderText(g);
        }
        else if (state == FightState.VILLAIN_OPTION) {
            renderText(g);
        }
        else if (state == FightState.VILLAIN_INFO) {
            renderText(g);
        }
        else if (state == FightState.END_FIGHT) {
            renderText(g);
        }

    }

    private void renderInit(Graphics g) {
        loadingRenderer.tick();
        if (!halfwayDone) {
            halfwayDone = loadingRenderer.load(g);
        }
        else {
            g.setColor(Color.white);
            drawResponsiveString("Ihr wurdet in einen Kampf verwickelt, nun kämpft oder sterbt!", X_CONSTANT + 25,
                    Y_CONSTANT + 40, g);
            if (!loadingDone) {
                loadingDone = !loadingRenderer.load(g);
                fight.startFight();
            }
        }
    }

    private void renderHeroOption(Graphics g) {

        int size = options.size();

        drawHeader("Wähle deine Attacke", g, null);
        for (Option option : options) {
            int x = X_CONSTANT + (option.getPosition() > 2 ? 6 * Spawner.OBJECT_STANDARD_SIZE + Spawner.SMALL_STANDARD_SIZE : 0);
            int y = Y_CONSTANT + ((option.getPosition() - 1) % 2) * 78;
            g.setColor(Color.white);
            if (option.getPosition() - 1 == index) {
                if (size > 2) {
                    drawImage(selectedItemBGHalf, x, y, g);
                }
                else {
                    drawImage(selectedItemBG, x, y, g);
                }
            }
            else {
                if (size > 2) {
                    drawImage(unselctedItemBGHalf, x, y, g);
                }
                else {
                    drawImage(unselctedItemBG, x, y, g);
                }
            }
            if (option.isSelectable()) {
                g.setColor(Color.WHITE);
            }
            else {
                g.setColor(Color.GRAY);
            }
            drawResponsiveString(option.getText(), x + 25, y + 40, g);
        }
    }

    private void renderMiniGame(Graphics g) {
        Color fog = new Color(0, 0, 0, 230);
        g.setColor(fog);
        g.fillRect(0, 0, ResolutionUtil.recalculateX(Constants.WIDTH, resolution),
                ResolutionUtil.recalculateY(Constants.HEIGHT, resolution));
        if (miniGameRenderer == null) {
            miniGameRenderer = fight.getMiniGame() != null ? new MiniGameRenderer(fight.getMiniGame(), getResolution())
                    : null;
        }
        else {
            miniGameRenderer.render(g);
        }
        heroLife.renderAll(g);
        villaineLife.renderAll(g);
        drawImage(menuBG, 0, 0, g);
        g.setColor(Color.white);
        String text = fight.getText();
        drawString(text != null ? text : "", X_CONSTANT + 25, Y_CONSTANT + 40, g);

    }

    private void renderText(Graphics g) {
        miniGameRenderer = null;
        g.setColor(Color.white);
        String text = fight.getText();

        drawString(text != null ? text : "", X_CONSTANT + 25, Y_CONSTANT + 40, g);

    }

    private List<SkillModelBean> assembleOptions() {
        List<SkillModelBean> list = new ArrayList<>();
        int i = 1;
        for (Skill skill : skills) {
            SkillModelBean wrap = new SkillModelBean();
            wrap.setPosition(i);
            wrap.setSkill(skill);
            wrap.setVillain(villain);
            list.add(wrap);
            i++;
        }
        return list;
    }

    public void cancelAttack() {
        if (fightStateAdapter.getState() == FightState.MINI_GAME) {
            miniGameRenderer = null;
            fight.cancelAttack();
        }
    }
}
