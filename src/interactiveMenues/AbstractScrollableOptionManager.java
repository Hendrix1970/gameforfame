package interactiveMenues;

import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import guiFrameWork.FontUtils;
import heroStuff.characterClasses.Hero;

public abstract class AbstractScrollableOptionManager extends AbstractOptionManager<Hero> {

    private Image arrowUpImage;
    private Image arrowDownImage;
    int relativeIndex;
    int scroll;

    Map<Integer, Font> fonts;

    boolean arrowUp;
    boolean arrowDown;

    // TODO adapt to something more general
    List itemList;

    AbstractScrollableOptionManager(List list) {
        super();
        itemList = list;
        determineFonts();
        calculateIndexRelatedValues();
    }

    private void determineFonts() {
        fonts = new HashMap<>();
        fonts.put(25, FontUtils.getStandardFontInSize(25));
        fonts.put(30, FontUtils.getStandardFontInSize(30));
        fonts.put(42, FontUtils.getStandardFontInSize(42));
        fonts.put(48, FontUtils.getStandardFontInSize(48));
    }

    private void calculateIndexRelatedValues() {
        scroll = 3;
        index = Math.min(getItemList().size() - 1, scroll);
        relativeIndex = Math.max(0, getItemList().size() - (scroll + 1));
    }

    @Override
    protected void initImages() {
        menuBG = autoResizeImageFromStandardSize("menu-fenster.png");
        arrowUpImage = autoResizeImageFromStandardSize("pfeil-hoch.png");
        arrowDownImage = autoResizeImageFromStandardSize("pfeil-runter.png");
    }

    @Override
    public void render(Graphics g) {
        showBG(g);
        renderSpecificData(g);
        showArrows(g);
    }

    protected abstract void renderSpecificData(Graphics g);

    protected List getItemList() {
        return itemList;
    }

    protected Font getFont(Integer i) {
        if (fonts.containsKey(i)) {
            return fonts.get(i);
        }
        else {
            return fonts.get(42);
        }
    }

    public void setUpOptions(Hero dts) {
        refreshOptions();
    }

    protected void refreshOptions() {
        options.clear();
        while (index >= getItemList().size() || index > relativeIndex + scroll) {
            index--;
        }
        for (int i = 0; i <= scroll && i < getItemList().size(); i++) {
            fillOptions(i);
        }
        determineArrows();
    }

    protected abstract void fillOptions(int i);

    private void determineArrows() {
        if (0 == relativeIndex) {
            arrowDown = false;
        }
        else {
            arrowDown = true;
        }
        if (relativeIndex + scroll < getItemList().size() - 1) {
            arrowUp = true;
        }
        else {
            arrowUp = false;
        }
    }

    @Override
    public void optionUp() {
        index++;
        int size = getItemList().size();
        if (index >= size) {
            index = size - 1;
            relativeIndex = 0;
        }
        else if (index + relativeIndex >= size) {
            index = scroll;
            relativeIndex = size - 1 - scroll;
        }
        else if (index >= scroll + 1) {
            index--;
            relativeIndex++;
        }
        refreshOptions();
    }

    @Override
    public void optionDown() {
        index--;
        if (index < 0) {
            index = 0;
            relativeIndex--;
            if (relativeIndex < 0) {
                relativeIndex = 0;
            }
        }
        refreshOptions();
    }

    protected void showArrows(Graphics g) {
        if (arrowUp) {
            drawImage(arrowUpImage, 175, 280, g);
        }
        if (arrowDown) {
            drawImage(arrowDownImage, 175, 530, g);
        }

    }

    protected void showBG(Graphics g) {
        drawImage(menuBG, 0, 0, g);
    }
}
