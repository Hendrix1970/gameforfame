package interactiveMenues;

import quests.Quest;

public class MenuQuest implements Option {

	private int position;
	private Quest quest;

	public MenuQuest(int position, Quest quest) {
		this.position = position;
		this.quest = quest;
	}

	@Override
	public void execute() {
		// TODO implement
	}

	@Override
	public int getPosition() {
		return position;
	}

	@Override
	public String getText() {
		return getQuest().getStatusText();
	}

	@Override
	public boolean isSelectable() {
		return true;
	}

	public Quest getQuest() {
		return quest;
	}
}
