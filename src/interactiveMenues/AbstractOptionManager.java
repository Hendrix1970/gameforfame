package interactiveMenues;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.util.ArrayList;
import java.util.List;

import Rooms.Spawner;
import constants.Constants;
import framework.AbstractMenuRenderer;
import guiFrameWork.FontUtils;
import guiFrameWork.ResolutionUtil;

/**
 * An abstract class that provides the functions to create an Option menu where
 * different options can be selected. It also renders this Option.
 * 
 * @author admin
 *
 * @param <D>
 */
public abstract class AbstractOptionManager<D> extends AbstractMenuRenderer {

    protected List<Option> options = new ArrayList<>();
    protected Font fontTalking = FontUtils.getStandardFontInSize(25);
    protected Font fontTitle = FontUtils.getStandardFontInSize(40);

    boolean stillTakingOptions = true;
    protected int index = 0;
    protected Image menuBG;
    protected Image unselctedItemBG;
    protected Image unselctedItemBGHalf;
    protected Image selectedItemBG;
    protected Image selectedItemBGHalf;

    /**
     * Submits all data to the Manager
     * 
     * @param OptionDts
     */
    public abstract void setUpOptions(D dts);

    public abstract void selectOption();

    @Override
    protected void initImages() {
        menuBG = autoResizeImageFromStandardSize("dialog.png");
        selectedItemBG = ResolutionUtil.factorizeImageHight(0.60, autoResizeImageFromStandardSize("dialog-itembox-selected.png"));
        unselctedItemBG = ResolutionUtil.factorizeImageHight(0.60, autoResizeImageFromStandardSize("dialog-itembox.png"));
        selectedItemBGHalf = ResolutionUtil.factorizeImageWidth(0.45, selectedItemBG);
        unselctedItemBGHalf = ResolutionUtil.factorizeImageWidth(0.45, unselctedItemBG);
    }

    public void drawHeader(String text, Graphics g) {
        g.setColor(Color.blue);
        drawString(text, Spawner.OBJECT_STANDARD_SIZE, Constants.HEIGHT - (Spawner.OBJECT_STANDARD_SIZE * 2), g);
    }

    protected void showOptions(Graphics g) {
        g.setFont(fontTalking);
        for (Option option : options) {
            g.setColor(Color.blue);
            if (option.getPosition() - 1 == index) {
                g.setColor(Color.orange);
                g.drawString(" -> ", Spawner.OBJECT_STANDARD_SIZE,
                        Constants.HEIGHT - Spawner.OBJECT_STANDARD_SIZE * 2 + option.getPosition() * 50);
                g.drawString(option.getText(), Spawner.OBJECT_STANDARD_SIZE + 40,
                        Constants.HEIGHT - Spawner.OBJECT_STANDARD_SIZE * 2 + option.getPosition() * 50);
            }
            else {
                g.drawString(option.getText(), Spawner.OBJECT_STANDARD_SIZE + 40,
                        Constants.HEIGHT - Spawner.OBJECT_STANDARD_SIZE * 2 + option.getPosition() * 50);
            }
        }
    }

    public void optionUp() {
        index++;
        if (index >= options.size()) {
            index = 0;
        }
    }

    public void optionDown() {
        index--;
        if (index < 0) {
            index = options.size() - 1;
        }
    }

    public void optionLeft() {
        // implement if needed
    }

    public void optionRight() {
        // implement if needed
    }

    public boolean isStillTakingOptions() {
        return stillTakingOptions;
    }

    public void setStillTakingOptions(boolean stillTakingOptions) {
        this.stillTakingOptions = stillTakingOptions;
    }

    public void drawHeader(String text, Graphics g, String name) {
        g.setColor(Color.white);
        String outputString = name != null ? name + ": " + text : text;
        drawResponsiveStringWithLinebreak(outputString, 3 * Spawner.OBJECT_STANDARD_SIZE - 45,
                Constants.HEIGHT - (Spawner.OBJECT_STANDARD_SIZE * 2) - 30, 160, g);
    }

}
