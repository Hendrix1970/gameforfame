package interactiveMenues;

import heroStuff.characterClasses.Hero;

public interface Interactive {
	public void interact(Hero hero);
}
