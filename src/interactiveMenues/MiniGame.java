package interactiveMenues;

import Villains.Skill;
import constants.Constants;
import framework.AbstractMoveableGameObject;

public class MiniGame {

	private Skill heroSkill;
	private AbstractMoveableGameObject villain;
	private int x = 200;
	private int y = 350;
	boolean fromLeftToRight = true;

	public MiniGame(Skill heroSkill, AbstractMoveableGameObject villain) {
		this.heroSkill = heroSkill;
		this.villain = villain;
	}

	public void tick() {
		if (fromLeftToRight) {
			if (getX() < Constants.WIDTH - 225 - 283) {
				setX(getX() + speedOfX());
			} else if (getX() < Constants.WIDTH - 225) {
				setX(getX() + speedOfX());
			} else {
				fromLeftToRight = false;
			}
		} else {
			if (getX() > Constants.WIDTH - 225 - 270) {
				setX(getX() - speedOfX());
			} else if (getX() > 200) {
				setX(getX() - speedOfX());
			} else {
				fromLeftToRight = true;
			}
		}

	}

	private int speedOfX() {
		return (int) (20 * calculateReturnValue()) + 1;
	}

	public int getSpeedOfY() {
		return (int) -(50 * (1 - (1 - calculateReturnValue()) * (1 - calculateReturnValue())));
	}

	public double select() {
		return calculateReturnValue();
	}

	private double calculateReturnValue() {
		// current x-position - distance between left rimp and "mini game".
		// Devided by the length of "mini game".
		double d = ((double) getX() - 200) / (Constants.WIDTH - 425);
		if (d < 0.5) {
			return 2 * d;
		} else {
			return (1 - d) * 2;
		}
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}
}
