package interactiveMenues;

import framework.Activatable;
import gameObjects.Items.Item;
import heroStuff.characterClasses.Hero;

public abstract class Consumable extends Item implements Activatable {

	public Consumable(int x, int y, int height, int width) {
		super(x, y, height, width);
	}

	abstract public String getText();

	abstract protected void use(Hero hero);

	public void consume(Hero hero) {
		if (canBeConsumed(hero)) {
			use(hero);
		}
	}

	/**
	 * Determines whether or not an item can be consumed, to protect the player from
	 * wasting Items.
	 */
	public boolean canBeConsumed(Hero hero) {
		return true;
	}

	public abstract boolean isOnMap();
}
