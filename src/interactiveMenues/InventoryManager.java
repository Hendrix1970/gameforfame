package interactiveMenues;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;

import Rooms.Spawner;
import constants.Constants;
import gameObjects.Items.DefensiveItem;
import gameObjects.Items.Item;
import gameObjects.Items.Weapon;
import guiFrameWork.ResolutionUtil;
import heroStuff.Inventory;

public class InventoryManager extends AbstractScrollableOptionManager {

    private Inventory iventory;

    private Image inventarBG;
    private Image selctedItemBG;

    private Color lightBlue = new Color(29, 35, 72);
    private Color darkBlue = new Color(16, 21, 53);

    boolean listItems = true;

    int indexEquippedItems = 0;

    public InventoryManager(Inventory iventory) {
        super(iventory.getItemList());
        this.iventory = iventory;
        setUpOptions(null);
    }

    protected void initImages() {
        super.initImages();
        inventarBG = autoResizeImageFromStandardSize("menu-inventar.png");
        unselctedItemBG = autoResizeImageFromStandardSize("menu-inventar-itembox.png");
        selctedItemBG = autoResizeImageFromStandardSize("menu-inventar-item-selected.png");
    }

    public void selectOption() {
        if (listItems) {
            int oldSize = itemList.size();
            MenuItem menuItem = (MenuItem) options.get(index);
            iventory.useItem(menuItem.getItem());
            int newSize = itemList.size();
            if (oldSize > newSize && relativeIndex >= 1) {
                relativeIndex--;
            }
            else if (oldSize > newSize && relativeIndex == 0) {
                relativeIndex = 0;
            }
            refreshOptions();
        }
    }

    @Override
    public void optionUp() {
        if (listItems) {
            super.optionUp();
        }
        else if (indexEquippedItems < 5) {
            indexEquippedItems++;
        }
    }

    @Override
    public void optionDown() {
        if (listItems) {
            super.optionDown();
        }
        else if (indexEquippedItems > 0) {
            indexEquippedItems--;
        }
    }

    @Override
    public void optionLeft() {
        listItems = true;
    }

    @Override
    public void optionRight() {
        listItems = false;
    }

    @Override
    public void renderSpecificData(Graphics g) {
        drawImage(inventarBG, 0, 0, g);

        showListItems(g);
        showData(g);
        showEquippedItems(g);
    }

    private void showEquippedItems(Graphics g) {
        drawEquippedItem(iventory.getCurrentlyEquippedWeapon(), 796, 52 * Constants.unit, !listItems && indexEquippedItems == 0, g);
        drawEquippedItem(iventory.getCurrentlyEquippedHelmet(), 900, 47 * Constants.unit, !listItems && indexEquippedItems == 1, g);
        drawEquippedItem(iventory.getCurrentlyEquippedShield(), 1004, 52 * Constants.unit, !listItems && indexEquippedItems == 2, g);
        drawEquippedItem(iventory.getCurrentlyEquippedArmor(), 900, 72 * Constants.unit, !listItems && indexEquippedItems == 3, g);
        drawEquippedItem(iventory.getCurrentlyEquippedRightShoe(), 838, 2 + 94 * Constants.unit, !listItems && indexEquippedItems == 4, g);
        drawEquippedItem(iventory.getCurrentlyEquippedLeftShoe(), 953, 2 + 94 * Constants.unit, !listItems && indexEquippedItems == 5, g);
    }

    protected void showListItems(Graphics g) {
        g.setFont(getFont(48));
        g.setColor(Color.WHITE);
        for (Option option : options) {
            MenuItem item = ((MenuItem) option);
            int y = Constants.HEIGHT - 98 * Constants.unit + (options.size() - option.getPosition()) * 78;
            int x = Spawner.OBJECT_STANDARD_SIZE + 18 * Constants.unit + 32;
            drawListItem(item.getImage(), x, y, g);
            if (listItems && option.getPosition() - 1 == index) {
                drawImage(selctedItemBG, x, y, g);
            }
            else {
                drawImage(unselctedItemBG, x, y, g);
            }
            drawResponsiveString(item.getText(), x + 68, y + 37, g);
        }
    }

    private void showData(Graphics g) {
        Item item = determineItemForToShowData();

        if (item == null)
            return;

        // Header
        g.setFont(getFont(48));
        g.setColor(darkBlue);
        drawResponsiveString(item.getName(), 542, 300, g);

        // Description
        g.setColor(lightBlue);
        g.setFont(getFont(30));
        drawResponsiveStringWithLinebreak(item.getDiscription() != null ? item.getDiscription() : "", 542, 350, g);

        // Stats
        g.setColor(Color.WHITE);
        if (item instanceof Consumable) {
            drawResponsiveString(((Consumable) item).getText(), 542, 442, g);
        }
        else {
            drawResponsiveString("Stärke:", 542, 442, g);
            drawResponsiveString("" + item.getStrengthModifier(), 721, 442, g);
            drawResponsiveString("Intelligenz:", 542, 471, g);
            drawResponsiveString("" + item.getIntellegenceModifier(), 721, 471, g);
            drawResponsiveString("Geschicklichkeit:", 542, 500, g);
            drawResponsiveString("" + item.getDexterityModifier(), 721, 500, g);
        }
        if (item instanceof Weapon) {
            drawResponsiveString("Schaden:", 542, 529, g);
            drawResponsiveString("" + ((Weapon) item).getDamage(), 721, 529, g);
        }
        if (item instanceof DefensiveItem) {
            drawResponsiveString("Rüstung:", 542, 529, g);
            drawResponsiveString("" + ((DefensiveItem) item).getDefense(), 721, 529, g);
        }

    }

    private Item determineItemForToShowData() {
        Item item = null;
        if (listItems) {
            item = ((MenuItem) options.get(index)).getItem();
        }
        else {
            switch (indexEquippedItems) {
            case 0:
                item = iventory.getCurrentlyEquippedWeapon();
                break;
            case 1:
                item = iventory.getCurrentlyEquippedHelmet();
                break;
            case 2:
                item = iventory.getCurrentlyEquippedShield();
                break;
            case 3:
                item = iventory.getCurrentlyEquippedArmor();
                break;
            case 4:
                item = iventory.getCurrentlyEquippedLeftShoe();
                break;
            case 5:
                item = iventory.getCurrentlyEquippedRightShoe();
                break;
            default:
                break;
            }
        }
        return item;
    }

    // TODO make it happen: g.getFontMetrics().stringWidth(substring)

    private void drawListItem(Image image, int x, int y, Graphics g) {
        drawResponsiveImage(ResolutionUtil.manuallyResizeImage(image, 50, 50), x + 5, y + 5, g);
    }

    private void drawEquippedItem(Item item, int x, int y, boolean selected, Graphics g) {
        if (selected) {
            g.setColor(darkBlue);
            g.fillRect(ResolutionUtil.recalculateX(x, resolution), ResolutionUtil.recalculateY(y, resolution),
                    (int) (resolution.getWidthFactor() * 85),
                    (int) (resolution.getHeightFactor() * 85));
        }
        if (item != null) {
            drawResponsiveImage(ResolutionUtil.manuallyResizeImage(item.getPortraitForInventar(), 75, 75), x + 5, y + 5, g);
        }
    }

    @Override
    protected void fillOptions(int i) {
        options.add(new MenuItem(i + 1, (Item) getItemList().get(relativeIndex + i)));
    }
}
