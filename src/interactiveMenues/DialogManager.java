package interactiveMenues;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.util.ArrayList;
import java.util.List;

import constants.Constants;
import dataStructure.DialogDts;
import guiFrameWork.FontUtils;
import quests.Quest;

public class DialogManager extends AbstractOptionManager<Quest> {

    List<DialogDts> dialogPartList = new ArrayList<>();
    int activeVertice = 0;
    private Font font_25;

    private boolean introAnimation;
    private boolean animationStarted;
    private long startTime;
    private boolean outroAnimation;

    /**
     * The constructor for QuestgiverInteractions
     * 
     * @param quest
     */
    public DialogManager(Quest quest) {
        super();
        initImages();
        determineFonts();

        setUpOptions(quest);
        introAnimation = true;
        animationStarted = false;
    }

    /**
     * The constructor for Dialogs (needs to be implemented properly though)
     * 
     * @param quest
     */
    public DialogManager() {
        super();
        initImages();
        determineFonts();

        introAnimation = true;
        animationStarted = false;
    }

    private void determineFonts() {
        font_25 = FontUtils.getStandardFontInSize(25);

    }

    public void setUpOptions(Quest quest) {
        refreshOptions();
        dialogPartList.addAll(quest.getDialog());

        // Sets up the first shown part of the dialog
        for (DialogDts dts : dialogPartList) {
            if (dts.getVertice() == activeVertice) {
                this.options.addAll(dts.getAnswers());
            }
        }
    }

    public void refreshOptions() {
        dialogPartList.clear();
        options.clear();
    }

    public void selectOption() {
        Answer answer = getCurrentAswer();
        if (answer.isExitPoint()) {
            outroAnimation = true;
            executeExitAnswer();
        }
        else {
            activeVertice = answer.getVerticeToGoTo();
            for (DialogDts dts : dialogPartList) {
                if (dts.getVertice() == activeVertice) {
                    this.options.clear();
                    this.options.addAll(dts.getAnswers());
                }
            }
            index = 0;
        }
    }

    private void executeExitAnswer() {
        // DIelalog wird in der render Methode geschlossen, wegen der Outro Animation
        boolean isChangesQuestState = getCurrentAswer().getQuestName() != null;
        if (isChangesQuestState) {
            getCurrentAswer().execute();
        }
    }

    @Override
    protected void render(Graphics g) {
        if (introAnimation) {
            renderIntroAnimation(g);
        }
        else if (outroAnimation) {
            renderOutroAnimation(g);
        }
        else {
            drawImage(menuBG, 0, 0, g);
            g.setFont(font_25);

            for (DialogDts dts : dialogPartList) {
                if (dts.getVertice() == activeVertice) {
                    drawHeader(dts.getNpcText(), g, dts.getName());
                    for (Option option : options) {
                        int x = Constants.DIALOG_OPTION_STANDARD_X;
                        int y = Constants.DIALOG_OPTION_STANDARD_Y + (options.size() - option.getPosition()) * Constants.unit * 8;
                        g.setColor(Color.white);
                        if (option.getPosition() - 1 == index) {
                            drawImage(selectedItemBG, x, y, g);
                        }
                        else {
                            drawImage(unselctedItemBG, x, y, g);
                        }
                        drawResponsiveString(option.getText(), x + 25, y + 23, g);
                    }
                }
            }
        }
    }

    private void renderIntroAnimation(Graphics g) {
        if (!animationStarted) {
            startTime = System.nanoTime() / 10000000;
            animationStarted = true;
        }
        long time = System.nanoTime() / 10000000;
        long timeDifference = time - startTime;
        int index = (int) timeDifference % 100;

        drawImage(menuBG, 0, (int) (250 - 5 * index), g);
        if (timeDifference > 49) {
            introAnimation = false;
            animationStarted = false;
        }
    }

    /**
     * actually executes the exit answer after rendering
     * 
     * @param g
     */
    private void renderOutroAnimation(Graphics g) {
        if (!animationStarted) {
            startTime = System.nanoTime() / 10000000;
            animationStarted = true;
        }
        long time = System.nanoTime() / 10000000;
        long timeDifference = time - startTime;
        int index = (int) timeDifference % 100;

        drawImage(menuBG, 0, (int) (5 * index), g);
        if (timeDifference > 49) {
            outroAnimation = false;
            animationStarted = false;
            stillTakingOptions = false;
        }
    }

    public void refreshAnimation() {
        this.introAnimation = true;
        this.outroAnimation = false;
        animationStarted = false;
    }

    private Answer getCurrentAswer() {
        return (Answer) options.get(index);
    }
}
