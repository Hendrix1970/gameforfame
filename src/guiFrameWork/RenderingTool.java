package guiFrameWork;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;

import javax.swing.ImageIcon;

public class RenderingTool {

    protected ResolutionModelBean resolution;

    public RenderingTool() {
        resolution = ResolutionUtil.getResolution();
    }

    protected void drawResponsiveImage(Image image, int x, int y, Graphics g) {
        g.drawImage(ResolutionUtil.autoResizeImage(resolution, image), ResolutionUtil.recalculateX(x, resolution),
                ResolutionUtil.recalculateY(y, resolution), null);
    }

    protected void drawImage(Image image, int x, int y, Graphics g) {
        Graphics2D g2 = (Graphics2D) g;
        RenderingHints rh = new RenderingHints(RenderingHints.KEY_TEXT_ANTIALIASING,
                RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
        g.drawImage(image, ResolutionUtil.recalculateX(x, resolution), ResolutionUtil.recalculateY(y, resolution),
                null);
    }

    public void drawResponsiveString(String text, int x, int y, Graphics g) {
        Graphics2D g2 = (Graphics2D) g;
        RenderingHints rh = new RenderingHints(RenderingHints.KEY_TEXT_ANTIALIASING,
                RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
        g2.setRenderingHints(rh);
        g2.drawString(text, ResolutionUtil.recalculateX(x, resolution), ResolutionUtil.recalculateY(y, resolution));
    }

    public void drawResponsiveStringWithLinebreak(String text, int x, int y, Graphics g) {
        drawResponsiveStringWithLinebreak(text, x, y, 21, g);
    }

    /*
     * Cuts off the line after the parameter lineLength chars. Goes back to the last blank and cuts there.
     * The remaining chars will be added to the next line.
     */
    public void drawResponsiveStringWithLinebreak(String text, int x, int y, Integer lineLength, Graphics g) {
        int i = text.length() / lineLength + 1;
        if (i > 1) {
            int shiftLine = 0;
            int endIndex = 0;
            for (int j = 0; endIndex != text.length(); j++) {
                endIndex = Math.min(text.length(), lineLength * (j + 1) - shiftLine);
                String substring = text.substring(lineLength * j - shiftLine, endIndex);
                int lastIndexOfBlank = lineLength;
                if (endIndex != text.length()) {
                    if (text.charAt(endIndex) != " ".toCharArray()[0]) {
                        lastIndexOfBlank = substring.lastIndexOf(" ");
                        substring = substring.substring(0, lastIndexOfBlank);
                    }
                }
                shiftLine += lineLength - lastIndexOfBlank - 1;
                drawResponsiveString(substring, x, y + (g.getFontMetrics().getMaxAscent() + 2) * j, g);
            }
        }
        else

        {
            drawResponsiveString(text, x, y, g);
        }
    }

    protected void drawRightBoundResponsiveString(String text, int x, int y, int width, Graphics g) {
        Graphics2D g2 = (Graphics2D) g;
        RenderingHints rh = new RenderingHints(RenderingHints.KEY_TEXT_ANTIALIASING,
                RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
        g2.setRenderingHints(rh);
        g2.drawString(text, ResolutionUtil.recalculateX(x + width, resolution) - g.getFontMetrics().stringWidth(text),
                ResolutionUtil.recalculateY(y, resolution));
    }

    protected void drawCenteredResponsiveString(String text, int x, int y, int width, Graphics g) {
        Graphics2D g2 = (Graphics2D) g;
        RenderingHints rh = new RenderingHints(RenderingHints.KEY_TEXT_ANTIALIASING,
                RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
        g2.setRenderingHints(rh);
        int stringWidth = g.getFontMetrics().stringWidth(text);
        g2.drawString(text, ResolutionUtil.recalculateX(x + width / 2, resolution) - stringWidth / 2,
                ResolutionUtil.recalculateY(y, resolution));
    }

    protected void drawImageFromStdSize(Image image, int x, int y, Graphics g) {
        // TODO(tobi) Alles antializen?
        Graphics2D g2 = (Graphics2D) g;
        RenderingHints rh = new RenderingHints(RenderingHints.KEY_TEXT_ANTIALIASING,
                RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
        g.drawImage(ResolutionUtil.resizeStdSizeImage(resolution, image), ResolutionUtil.recalculateX(x, resolution),
                ResolutionUtil.recalculateY(y, resolution), null);
    }

    protected Image autoResizeImage(String name) {
        return ResolutionUtil.autoResizeImage(resolution,
                new ImageIcon(getClass().getResource("/images/" + name)).getImage());
    }

    protected Image autoResizeImageFromStandardSize(String name) {
        return ResolutionUtil.resizeStdSizeImage(resolution,
                new ImageIcon(getClass().getResource("/images/" + name)).getImage());
    }

    protected Image autoResizeImage(Image image) {
        return ResolutionUtil.autoResizeImage(resolution, image);
    }

    protected Image autoResizeImageFromStandardSize(Image image) {
        return ResolutionUtil.resizeStdSizeImage(resolution, image);
    }

    protected Image resizeImageByPercent(Image image, double percent) {
        if (percent > 0) {
            return ResolutionUtil.factorizeImage(percent / 100, image);
        }
        else {
            return image;
        }
    }

    public ResolutionModelBean getResolution() {
        return resolution;
    }

    public void setResolution(ResolutionModelBean resolution) {
        this.resolution = resolution;
    }

}
