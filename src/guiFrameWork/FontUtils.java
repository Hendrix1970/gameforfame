package guiFrameWork;

import java.awt.Font;
import java.awt.GraphicsEnvironment;
import java.io.File;

public class FontUtils {

    private static final Font SERIF_FONT = new Font("serif", Font.PLAIN, 24);

    public static Font getStandardFontInSize(int size) {
        Font sourceFont = getFont("MeathFLF.ttf");
        Font font = new Font(sourceFont.getFontName(), Font.PLAIN, size);
        return font;
    }

    public static Font getGothicFontInSize(int size) {
        Font sourceFont = getFont("SketchGothicSchool.ttf");
        Font font = new Font(sourceFont.getFontName(), Font.PLAIN, size);
        return font;
    }

    public static Font getFont(String fontName) {

        Font font = null;

        try {
            File standartFont = new File(fontName);
            font = Font.createFont(Font.TRUETYPE_FONT, standartFont);
            GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();

            ge.registerFont(font);

        } catch (Exception ex) {
            font = SERIF_FONT;
        }
        return font;
    }
}
