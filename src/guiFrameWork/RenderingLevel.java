package guiFrameWork;

import java.util.Arrays;
import java.util.List;

public enum RenderingLevel {

	BACKGROUND(0), BLOOD_AND_GUTS(1), GAME_OBJECTS(2), ITEMS(3), HERO(4), HUD(5), INTERFACES(6);

	private int level;

	private RenderingLevel(int level) {
		this.level = level;
	}

	public int getLevel() {
		return this.level;
	}

	/**
	 * @return a list of all rendering levels, that have room-specific elements.
	 */
	public List<RenderingLevel> getRoomRenderingLevels() {
		return Arrays.asList(BACKGROUND, BLOOD_AND_GUTS, GAME_OBJECTS, INTERFACES);
	}

}