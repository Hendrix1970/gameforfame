package guiFrameWork;

import java.awt.Image;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

import javax.swing.ImageIcon;

import constants.Constants;

public class ResolutionUtil {

    public static ResolutionModelBean getResolution() {
        ResolutionModelBean resolutionModelBean = new ResolutionModelBean();

        try {
            Scanner scan = new Scanner(new File("resolution.txt"));
            if (scan.hasNextInt()) {
                int width = scan.nextInt();
                resolutionModelBean.setWidth(width);
                resolutionModelBean.setWidthFactor((double) width / Constants.WIDTH);
            }
            if (scan.hasNextInt()) {
                int height = scan.nextInt();
                resolutionModelBean.setHeight(height);
                resolutionModelBean.setHeightFactor((double) height / Constants.HEIGHT);
            }
            scan.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return resolutionModelBean;
    }

    public static Image manuallyResizeImage(Image image, int width, int height) {
        Image scaledImage = image.getScaledInstance(width, height, Image.SCALE_DEFAULT);
        ImageIcon icon = new ImageIcon(scaledImage);
        image = icon.getImage();
        return image;
    }

    public static Image autoResizeImage(ResolutionModelBean resolution, Image image) {
        int width = (int) (image.getWidth(null) * resolution.getWidthFactor());
        int height = (int) (image.getHeight(null) * resolution.getHeightFactor());
        Image scaledImage = image.getScaledInstance(width, height, Image.SCALE_DEFAULT);
        ImageIcon icon = new ImageIcon(scaledImage);
        image = icon.getImage();
        return image;
    }

    public static Image factorizeImage(double factor, Image image) {
        int width = (int) (image.getWidth(null) * factor);
        int height = (int) (image.getHeight(null) * factor);
        Image scaledImage = image.getScaledInstance(width, height, Image.SCALE_DEFAULT);
        ImageIcon icon = new ImageIcon(scaledImage);
        image = icon.getImage();
        return image;
    }

    public static Image factorizeImageHight(double factor, Image image) {
        int height = (int) (image.getHeight(null) * factor);
        Image scaledImage = image.getScaledInstance(image.getWidth(null), height, Image.SCALE_DEFAULT);
        ImageIcon icon = new ImageIcon(scaledImage);
        image = icon.getImage();
        return image;
    }

    public static Image factorizeImageWidth(double factor, Image image) {
        int width = (int) (image.getWidth(null) * factor);
        Image scaledImage = image.getScaledInstance(width, image.getHeight(null), Image.SCALE_DEFAULT);
        ImageIcon icon = new ImageIcon(scaledImage);
        image = icon.getImage();
        return image;
    }

    public static Image resizeStdSizeImage(ResolutionModelBean resolution, Image image) {
        int width = (int) ((image.getWidth(null) * Constants.WIDTH * resolution.getWidthFactor())
                / Constants.PICTURE_STANDARD_RES_X);
        int height = (int) ((image.getHeight(null) * Constants.HEIGHT * resolution.getHeightFactor())
                / Constants.PICTURE_STANDARD_RES_Y);
        Image scaledImage = image.getScaledInstance(width, height, Image.SCALE_DEFAULT);
        ImageIcon icon = new ImageIcon(scaledImage);
        image = icon.getImage();
        return image;
    }

    public static int recalculateX(int x, ResolutionModelBean resolution) {
        return (int) (x * resolution.getWidthFactor());
    }

    public static int recalculateY(int y, ResolutionModelBean resolution) {
        return (int) (y * resolution.getHeightFactor());
    }

}
