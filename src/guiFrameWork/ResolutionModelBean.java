package guiFrameWork;

public class ResolutionModelBean {
	
	private int width;
	private int height;
	private double widthFactor;
	private double heightFactor;
	
	public int getWidth() {
		return width;
	}
	public void setWidth(int width) {
		this.width = width;
	}
	public int getHeight() {
		return height;
	}
	public void setHeight(int height) {
		this.height = height;
	}
	public double getWidthFactor() {
		return widthFactor;
	}
	public void setWidthFactor(double widthFactor) {
		this.widthFactor = widthFactor;
	}
	public double getHeightFactor() {
		return heightFactor;
	}
	public void setHeightFactor(double heightFactor) {
		this.heightFactor = heightFactor;
	}
}
