package guiFrameWork;

import java.util.Comparator;

public class RenderingLevelComparator implements Comparator<RenderingLevel> {

	@Override
	public int compare(RenderingLevel o1, RenderingLevel o2) {
		return Integer.compare(o1.getLevel(), o2.getLevel());
	}
}
