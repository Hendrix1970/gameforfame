package guiFrameWork;

import java.awt.Graphics;
import java.util.ArrayList;
import java.util.ConcurrentModificationException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import guiFrameWork.renderer.Renderer;
import guiFrameWork.renderer.TemporaryEffectRenderer;

/**
 * This class does all the rendering ever to happen in this awesome game.
 * 
 * @author tdouque
 *
 */
public class TheRenderingMachine {

    private Map<RenderingLevel, List<Renderer>> renderers = new HashMap<>();

    public TheRenderingMachine() {
        for (RenderingLevel renderingLevel : RenderingLevel.values()) {
            renderers.put(renderingLevel, new ArrayList<Renderer>());
        }
    }

    /**
     * registers a render. Order of registration is irrelevant.
     * 
     * @param renderer
     * @param renderingLevel
     */
    public void registerRenderer(Renderer renderer, RenderingLevel renderingLevel) {
        List<Renderer> list = renderers.get(renderingLevel);
        list.add(renderer);
    }

    public void unregisterRenderer(Renderer renderer, RenderingLevel renderinglevel) {
        renderers.get(renderinglevel).remove(renderer);
    }

    public void registerRenderer(Renderer renderer) {
        List<Renderer> list = renderers.get(renderer.getRenderingLevel());
        list.add(renderer);
    }

    public void unregisterRenderer(Renderer renderer) {
        renderers.get(renderer.getRenderingLevel()).remove(renderer);
    }

    public void unregisterRenderer(Object object, RenderingLevel renderinglevel) {
        Renderer rendererToRemove = null;
        List<Renderer> levelRenderer = renderers.get(renderinglevel);
        for (Renderer renderer : levelRenderer) {
            Object renderedObject = renderer.getRenderedObject();
            if (renderedObject != null && renderedObject.equals(object)) {
                rendererToRemove = renderer;
            }
        }
        levelRenderer.remove(rendererToRemove);
    }

    public void changeBackground(Renderer renderer) {
        List<Renderer> background = renderers.get(RenderingLevel.BACKGROUND);
        if (background != null) {
            background.clear();
            background.add(renderer);
        }
    }

    public void clearRenderingLevels(List<RenderingLevel> renderingLevels) {
        for (RenderingLevel renderingLevel : renderingLevels) {
            renderers.get(renderingLevel).clear();
        }
    }

    public void clearRenderingLevel(RenderingLevel renderingLevel) {
        renderers.get(renderingLevel).clear();
    }

    public void clearTemporaryRenderers() {
        Optional<Renderer> deadEffect = renderers.get(RenderingLevel.INTERFACES).stream()
                .filter(r -> r instanceof TemporaryEffectRenderer)
                .filter(r -> ((TemporaryEffectRenderer) r).isEffectOver())
                .findAny();

        if (deadEffect.isPresent())
            renderers.get(RenderingLevel.INTERFACES).remove(deadEffect.get());
    }

    /**
     * Calls all renderers in the rendering level order. Renderers of a specific
     * rendering level are called in arbitrary order.
     */
    public void render(Graphics g) {
        try {
            List<RenderingLevel> renderingLevels = new ArrayList<RenderingLevel>(renderers.keySet());
            renderingLevels.sort(new RenderingLevelComparator());

            for (int i = 0; i < renderingLevels.size(); i++) {
                List<Renderer> levelSpecificRenderers = renderers.get(renderingLevels.get(i));
                for (Renderer renderer : levelSpecificRenderers) {
                    if (renderer != null) {
                        renderer.renderAll(g);
                    }
                }
            }
        } catch (ConcurrentModificationException e) {

        }
    }
}
