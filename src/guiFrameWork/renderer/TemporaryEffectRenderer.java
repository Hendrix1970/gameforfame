package guiFrameWork.renderer;

import framework.AbstractGameObject;
import guiFrameWork.RenderingLevel;

public abstract class TemporaryEffectRenderer extends AbstractRenderer {

    private long startingTime;

    public TemporaryEffectRenderer(AbstractGameObject object) {
        super(object, RenderingLevel.GAME_OBJECTS);
        startingTime = System.nanoTime() / 1000000;
    }

    public boolean isEffectOver() {
        boolean over = System.nanoTime() / 1000000 - getStartingTime() > getDurationInSeconds() * 1000;
        return over;
    }

    abstract long getDurationInSeconds();

    public long getStartingTime() {
        return startingTime;
    }
}