package guiFrameWork.renderer;

import framework.AbstractMoveableGameObject;

public class ChickenRenderer extends AbstractMoveableRenderer {

	public ChickenRenderer(AbstractMoveableGameObject chicken) {
		super(chicken);
	}

	@Override
	protected void initImages() {
		north = autoResizeImage("enemy_chicken_north.png");
		east = autoResizeImage("enemy_chicken_east.png");
		south = autoResizeImage("enemy_chicken_south.png");
		west = autoResizeImage("enemy_chicken_west.png");
	}
}