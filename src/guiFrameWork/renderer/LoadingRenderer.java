package guiFrameWork.renderer;

import java.awt.Color;
import java.awt.Graphics;

import engine.TickTimer;
import guiFrameWork.RenderingLevel;
import guiFrameWork.ResolutionUtil;

public class LoadingRenderer extends AbstractRenderer {
	private int x = 1;
	private int y = 1;
	boolean midway = false;

	private TickTimer timer = new TickTimer(100.0);

	public LoadingRenderer() {
		super(null, RenderingLevel.INTERFACES);
	}

	public void tick() {
		if (!midway) {
			x += ResolutionUtil.recalculateX(12, getResolution());
			y += ResolutionUtil.recalculateY(12, getResolution());
			if (x >= (getResolution().getWidth() / 2) - 50) {
				midway = true;
			}
		}
		if (midway) {
			x -= ResolutionUtil.recalculateX(12, getResolution());
			y -= ResolutionUtil.recalculateY(12, getResolution());
			if (x <= 5) {
				midway = false;
			}
		}
	}

	public boolean load(Graphics g) {
		if (timer.isTick()) {
			tick();
		}
		render(g);
		return midway;
	}

	public void render(Graphics g) {
		if (!midway) {
			g.setColor(Color.BLACK);
			g.fillRect(0, 0, getResolution().getWidth(), y);
			g.fillRect(0, 0, x, getResolution().getHeight());
			g.fillRect(0, getResolution().getHeight() - y, getResolution().getWidth(), y);
			g.fillRect(getResolution().getWidth() - x, 0, x, getResolution().getHeight());
		}
		if (midway) {
			g.setColor(Color.BLACK);
			g.fillRect(0, 0, getResolution().getWidth(), y);
			g.fillRect(0, 0, x, getResolution().getHeight());
			g.fillRect(0, getResolution().getHeight() - y, getResolution().getWidth(), y);
			g.fillRect(getResolution().getWidth() - x, 0, x, getResolution().getHeight());
		}

	}
}
