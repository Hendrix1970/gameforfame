package guiFrameWork.renderer;

import java.awt.Color;
import java.awt.Graphics;

import engine.Game;
import engine.ScreenStateApadter;
import framework.ScreenStates;
import guiFrameWork.RenderingLevel;
import guiFrameWork.ResolutionUtil;

public class LoadingScreenRenderer extends AbstractRenderer {
	private int x = 1;
	private int y = 1;
	boolean midway = false;
	private Game game;
	private ScreenStateApadter screenStateApadter;

	public LoadingScreenRenderer(Game game, ScreenStateApadter screenStateApadter) {
		super(null, RenderingLevel.INTERFACES);
		this.game = game;
		this.screenStateApadter = screenStateApadter;
	}

	public void tick() {
		if (!midway) {
			x += ResolutionUtil.recalculateX(12, getResolution());
			y += ResolutionUtil.recalculateY(12, getResolution());
			if (x >= (getResolution().getWidth() / 2) - 50) {
				midway = true;
				game.nextLevelIgnoringScreenState();
			}
		}
		if (midway) {
			x -= ResolutionUtil.recalculateX(12, getResolution());
			y -= ResolutionUtil.recalculateY(12, getResolution());
			if (x <= 5) {
				midway = false;
				screenStateApadter.setScreenState(ScreenStates.inGame);
			}
		}
	}

	public void render(Graphics g) {
		if (!midway) {
			g.setColor(Color.BLACK);
			g.fillRect(0, 0, getResolution().getWidth(), y);
			g.fillRect(0, 0, x, getResolution().getHeight());
			g.fillRect(0, getResolution().getHeight() - y, getResolution().getWidth(), y);
			g.fillRect(getResolution().getWidth() - x, 0, x, getResolution().getHeight());
		}
		if (midway) {
			g.setColor(Color.BLACK);
			g.fillRect(0, 0, getResolution().getWidth(), y);
			g.fillRect(0, 0, x, getResolution().getHeight());
			g.fillRect(0, getResolution().getHeight() - y, getResolution().getWidth(), y);
			g.fillRect(getResolution().getWidth() - x, 0, x, getResolution().getHeight());
		}

	}
}
