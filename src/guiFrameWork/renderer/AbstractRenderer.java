package guiFrameWork.renderer;

import java.awt.Graphics;
import java.awt.Image;
import java.util.List;

import framework.AbstractGameObject;
import guiFrameWork.RenderingLevel;
import guiFrameWork.RenderingTool;
import guiFrameWork.ResolutionUtil;

public abstract class AbstractRenderer extends RenderingTool implements Renderer {

	protected AbstractGameObject rendereredObject;

	private RenderingLevel renderingLevel;

	protected int positionX;

	protected int positionY;

	protected int offsetX;

	protected int offsetY;

	protected List<Image> pics;

	public AbstractRenderer(AbstractGameObject abstractGameObject, RenderingLevel renderingLevel) {
		this.resolution = ResolutionUtil.getResolution();
		this.rendereredObject = abstractGameObject;
		this.renderingLevel = renderingLevel;
		initImages();
	}

	public AbstractRenderer(AbstractGameObject abstractGameObject, RenderingLevel renderingLevel, int offsetX,
			int offsetY) {
		this.resolution = ResolutionUtil.getResolution();
		this.rendereredObject = abstractGameObject;
		this.renderingLevel = renderingLevel;
		this.offsetX = offsetX;
		this.offsetY = offsetY;
		initImages();
	}

	public AbstractRenderer(RenderingLevel renderingLevel, int positionX, int positionY) {
		this.resolution = ResolutionUtil.getResolution();
		this.renderingLevel = renderingLevel;
		this.positionX = positionX;
		this.positionY = positionY;
		this.offsetX = 0;
		this.offsetY = 0;
		initImages();
	}

	public AbstractRenderer(RenderingLevel renderingLevel, int positionX, int positionY, int offsetX, int offsetY) {
		this.resolution = ResolutionUtil.getResolution();
		this.renderingLevel = renderingLevel;
		this.positionX = positionX;
		this.positionY = positionY;
		this.offsetX = offsetX;
		this.offsetY = offsetY;
		initImages();
	}

	public void renderAll(Graphics g) {
		if (rendereredObject != null) {
			positionX = rendereredObject.getX();
			positionY = rendereredObject.getY();
		}
		render(g);
	}

	protected abstract void render(Graphics g);

	protected void initImages() {
		// To be implemented in subclasses
	}

	public RenderingLevel getRenderingLevel() {
		return renderingLevel;
	}

	public AbstractGameObject getRenderedObject() {
		return rendereredObject;
	}

	protected int getPositionX() {
		return positionX;
	}

	protected int getPositionY() {
		return positionY;
	}

	public int getOffsetX() {
		return offsetX;
	}

	public int getOffsetY() {
		return offsetY;
	}

    protected void drawString(String text, int x, int y, Graphics g) {
    	boolean initial = true;
    	for (String line : text.split("\n")) {
    		if (initial) {
    			drawResponsiveString(line, x, y, g);
    			initial = false;
    		} else {
    			drawResponsiveString(line, x, y += g.getFontMetrics().getHeight() - 10, g);
    		}
    	}
    }
}
