package guiFrameWork.renderer;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;

import javax.swing.ImageIcon;

import guiFrameWork.RenderingLevel;
import guiFrameWork.ResolutionUtil;
import heroStuff.characterClasses.Hero;

public class HUDRenderer extends AbstractRenderer {

	private Hero hero;
	private LifeBarRenderer lifeBar;
	private Image xpBarImage;

	public HUDRenderer(Hero hero) {
		super(RenderingLevel.HUD, 0, 0);
		this.hero = hero;
		lifeBar = new LifeBarRenderer(hero);
		xpBarImage = ResolutionUtil.autoResizeImage(getResolution(),
				new ImageIcon(getClass().getResource("/images/balken_lineal.png")).getImage());
	}

	@Override
	protected void render(Graphics g) {
		lifeBar.render(g);
		double xpBar = calculateXpBar(hero.getXp(), hero.getLevelManager().getNextLevelXp(),
				hero.getLevelManager().getPreviousLevelXp());
		g.setColor(Color.black);
		g.fillRect(getResolution().getWidth() - fitX(200), fitY(20), fitX(150), fitY(22));
		g.setColor(Color.yellow);
		g.fillRect(getResolution().getWidth() - fitX(200), fitY(20), fitX((int) xpBar), fitY(22));
		g.drawImage(xpBarImage, getResolution().getWidth() - fitX(260), 0, null);
	}

	private double calculateXpBar(int xp, int nextLevel, int previousLevel) {
		double denominator = xp - previousLevel;
		double counter = nextLevel - previousLevel;
		return (denominator / counter) * 200;
	}

	private int fitX(int i) {
		return ResolutionUtil.recalculateX(i, getResolution());
	}

	private int fitY(int i) {
		return ResolutionUtil.recalculateX(i, getResolution());
	}
}
