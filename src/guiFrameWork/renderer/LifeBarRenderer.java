package guiFrameWork.renderer;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;

import javax.swing.ImageIcon;

import guiFrameWork.RenderingLevel;
import guiFrameWork.ResolutionUtil;
import heroStuff.characterClasses.Hero;

public class LifeBarRenderer extends AbstractRenderer {

	private Hero hero;
	Image liveBarImage;
	int x;
	int y;

	public LifeBarRenderer(Hero hero) {
		super(RenderingLevel.HUD, 0, 0);
		this.hero = hero;

		liveBarImage = ResolutionUtil.autoResizeImage(getResolution(),
				new ImageIcon(getClass().getResource("/images/balken_stift.png")).getImage());

		x = fitX(60);
		y = fitY(20);
	}

	@Override
	protected void render(Graphics g) {
		double liveBar = (double) Math.max(hero.getHealth(), 0) / hero.getMaxHealth() * 150;
		g.setColor(Color.black);
		g.fillRect(x, y, fitX(150), fitY(22));
		g.setColor(Color.red);
		g.fillRect(x, y, fitX((int) liveBar), fitY(22));
		g.drawImage(liveBarImage, 0, 0, null);
	}

	private int fitX(int i) {
		return ResolutionUtil.recalculateX(i, getResolution());
	}

	private int fitY(int i) {
		return ResolutionUtil.recalculateX(i, getResolution());
	}

}
