package guiFrameWork.renderer;

import java.awt.Color;
import java.awt.Graphics;

import constants.Constants;
import framework.AbstractGameObject;
import guiFrameWork.FontUtils;

public class LevelUpRenderer extends TemporaryEffectRenderer {

    public LevelUpRenderer(AbstractGameObject hero) {
        super(hero);
    }

    @Override
    protected void initImages() {
    }

    @Override
    protected void render(Graphics g) {
        // the frequency of flashes in milliseconds
        long flashTiming = 200;
        long runningForMilliSeconds = System.nanoTime() / 1000000 - getStartingTime();
        if (runningForMilliSeconds % (2 * flashTiming) > flashTiming) {
            g.setColor(Color.RED);
            g.setFont(FontUtils.getGothicFontInSize(350));
            drawString("Level Up", 50 * Constants.unit, 400, g);
        }
    }

    @Override
    long getDurationInSeconds() {
        return 3;
    }

}
