package guiFrameWork.renderer;

import java.awt.Graphics;
import java.awt.Image;
import java.util.ArrayList;
import java.util.List;

import framework.AbstractGameObject;
import guiFrameWork.RenderingLevel;

public class SequencedPicturesRenderer extends AbstractRenderer {

	List<Image> pics;
	private Image actualPic;

	/**
	 * Offers the opportunity to draw the here given sequence of pictures. The
	 * pictures are drawn within 1 second at the given location.
	 * 
	 * @param pics the pictures to be drawn
	 */
	public SequencedPicturesRenderer(AbstractGameObject abstractGameObject, RenderingLevel renderingLevel,
			List<Image> pics) {
		super(abstractGameObject, renderingLevel);
		this.pics = pics;
		initListImages();
	}

	/**
	 * Offers the opportunity to draw the here given sequence of pictures. The
	 * pictures are drawn within 1 second at the given location.
	 * 
	 * @param pics the pictures to be drawn
	 */
	public SequencedPicturesRenderer(AbstractGameObject abstractGameObject, RenderingLevel renderingLevel,
			List<Image> pics, int offsetX, int offsetY) {
		super(abstractGameObject, renderingLevel, offsetX, offsetY);
		this.pics = pics;
		initListImages();
	}

	/**
	 * Offers the opportunity to draw the here given picture. The picture is drawn
	 * at the given location.
	 * 
	 * @param pic the picture to be drawn
	 */
	public SequencedPicturesRenderer(AbstractGameObject abstractGameObject, RenderingLevel renderingLevel, Image pic) {
		super(abstractGameObject, renderingLevel);
		this.pics = new ArrayList<>();
		pics.add(pic);
		initListImages();
	}

	/**
	 * Offers the opportunity to draw the here given picture. The picture is drawn
	 * at the given location.
	 */
	public SequencedPicturesRenderer(AbstractGameObject abstractGameObject, RenderingLevel renderingLevel, Image pic,
			int offsetX, int offsetY) {
		super(abstractGameObject, renderingLevel, offsetX, offsetY);
		this.pics = new ArrayList<>();
		pics.add(pic);
		initListImages();
	}

	@Override
	protected void render(Graphics g) {
		if (pics.size() > 1) {
			long time = System.nanoTime() / 100000000;
			int index = (int) time % pics.size();
			actualPic = pics.get(index);
		} else {
			actualPic = pics.iterator().next();
		}

		drawImage(actualPic, getPositionX() + getOffsetX(), getPositionY() + getOffsetY(), g);
	}

	protected void initListImages() {
		List<Image> resizedPics = new ArrayList<>();

		for (Image image : pics) {
			resizedPics.add(autoResizeImage(image));
		}

		pics = resizedPics;
		actualPic = pics.get(0);
	}
}
