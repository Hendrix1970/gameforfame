package guiFrameWork.renderer;

import java.awt.Graphics;
import java.awt.Image;

import guiFrameWork.RenderingLevel;
import heroStuff.characterClasses.Hero;

/**
 * special renderer that basically implements the heros render method.
 * 
 * @author tobi
 *
 */
public class HeroRenderer extends AbstractRenderer {

	private Image heroNorth;
	private Image heroEast;
	private Image heroSouth;
	private Image heroWest;
	private Image heroNorthWest;
	private Image heroNorthEast;
	private Image heroSouthWest;
	private Image heroSouthEast;
	private Image heroImage;

	private int oldSpeedX = 0;
	private int oldSpeedY = 0;

	public HeroRenderer(Hero hero) {
		super(hero, RenderingLevel.HERO);
	}

	@Override
	protected void render(Graphics g) {
		calculateDirectionAndSetHeroImage();
		drawImage(heroImage, getPositionX(), getPositionY(), g);
	}

	private void calculateDirectionAndSetHeroImage() {
		Hero hero = (Hero) getRenderedObject();

		int speedX = hero.getSpeedX();
		int speedY = hero.getSpeedY();
		if (horizontalDirectionChanged(hero) && speedX != 0) {
			if (speedX > 0) {
				heroImage = heroEast;
			} else if (speedX < 0) {
				heroImage = heroWest;
			}
		} else if (horizontalDirectionChanged(hero) && speedX == 0) {
			if (speedY > 0) {
				heroImage = heroSouth;
			} else if (speedY < 0) {
				heroImage = heroNorth;
			}
		}
		if (verticalDirectionChanged(hero) && speedY != 0) {
			if (speedY > 0) {
				heroImage = heroSouth;
			} else if (speedY < 0) {
				heroImage = heroNorth;
			}
		} else if (verticalDirectionChanged(hero) && speedY == 0) {
			if (speedX > 0) {
				heroImage = heroEast;
			} else if (speedX < 0) {
				heroImage = heroWest;
			}
		}
		oldSpeedX = speedX;
		oldSpeedY = speedY;
	}

	private boolean verticalDirectionChanged(Hero hero) {
		return hero.getSpeedY() != oldSpeedY;
	}

	private boolean horizontalDirectionChanged(Hero hero) {
		return hero.getSpeedX() != oldSpeedX;
	}

	@Override
	protected void initImages() {

		heroNorth = autoResizeImage("zombie_n.png");
		heroEast = autoResizeImage("zombie_e.png");
		heroSouth = autoResizeImage("zombie_s.png");
		heroWest = autoResizeImage("zombie_w.png");
		heroNorthWest = autoResizeImage("zombie_nw.png");
		heroNorthEast = autoResizeImage("zombie_ne.png");
		heroSouthWest = autoResizeImage("zombie_sw.png");
		heroSouthEast = autoResizeImage("zombie_se.png");

		heroImage = heroNorth;

	}
}
