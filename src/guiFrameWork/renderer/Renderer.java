package guiFrameWork.renderer;

import java.awt.Graphics;

import guiFrameWork.RenderingLevel;

public interface Renderer {

	public void renderAll(Graphics g);

	public Object getRenderedObject();

	public RenderingLevel getRenderingLevel();
}
