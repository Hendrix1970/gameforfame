package guiFrameWork.renderer;

import java.awt.Graphics;
import java.awt.Image;

import javax.swing.ImageIcon;

import guiFrameWork.RenderingTool;
import guiFrameWork.ResolutionModelBean;
import interactiveMenues.MiniGame;

public class MiniGameRenderer extends RenderingTool {

	private MiniGame miniGame;
	private ResolutionModelBean resolution;
	private Image backGroundLine;
	private Image ball;

	public MiniGameRenderer(MiniGame miniGame, ResolutionModelBean resolution) {
		this.miniGame = miniGame;
		this.resolution = resolution;
		backGroundLine = autoResizeImage(new ImageIcon(getClass().getResource("/images/kampf_balken.png")).getImage());
		ball = autoResizeImage(new ImageIcon(getClass().getResource("/images/kampf_balken_kugel.png")).getImage());
	}

	public void render(Graphics g) {
		if (miniGame != null) {
			drawImage(backGroundLine, 200, 220, g);
			drawImage(ball, miniGame.getX() - 80, miniGame.getY() + miniGame.getSpeedOfY() - 80, g);
		}

		// g.setColor(Color.GREEN);
		// g.fillOval(ResolutionUtil.recalculateX(miniGame.getX(), resolution),
		// ResolutionUtil.recalculateY(miniGame.getY() + miniGame.getSpeedOfY(),
		// resolution),
		// ResolutionUtil.recalculateX(25, resolution),
		// ResolutionUtil.recalculateX(25, resolution));
	}
}
