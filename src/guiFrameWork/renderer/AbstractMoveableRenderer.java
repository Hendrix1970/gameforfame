package guiFrameWork.renderer;

import java.awt.Graphics;
import java.awt.Image;

import framework.AbstractMoveableGameObject;
import guiFrameWork.RenderingLevel;

public abstract class AbstractMoveableRenderer extends AbstractRenderer {

    Image north;
    Image east;
    Image south;
    Image west;

    Image image = null;

    public AbstractMoveableRenderer(AbstractMoveableGameObject abstractMoveable) {
        super(abstractMoveable, RenderingLevel.GAME_OBJECTS);
    }

    @Override
    public void render(Graphics g) {
        switch (((AbstractMoveableGameObject) getRenderedObject()).getDirection()) {
        case NORTH:
            image = north;
            break;
        case EAST:
            image = east;
            break;
        case SOUTH:
            image = south;
            break;
        case WEST:
            image = west;
            break;
        case NONE:
        }

        drawImage(image, getPositionX(), getPositionY(), g);
    }
}
