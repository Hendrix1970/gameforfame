package guiFrameWork.renderer;

import java.awt.Graphics;

import Villains.Ghost;
import framework.AbstractMoveableGameObject;

public class GhostRenderer extends AbstractMoveableRenderer {

	public GhostRenderer(AbstractMoveableGameObject ghost) {
		super(ghost);
	}

	@Override
	public void render(Graphics g) {
		Ghost ghost = (Ghost) getRenderedObject();
		if (ghost.isVisible()) {
			super.render(g);
		}
	}

	@Override
	protected void initImages() {
		north = autoResizeImage("ghost_n.png");
		east = autoResizeImage("ghost_e.png");
		south = autoResizeImage("ghost_s.png");
		west = autoResizeImage("ghost_w.png");
	}
}
