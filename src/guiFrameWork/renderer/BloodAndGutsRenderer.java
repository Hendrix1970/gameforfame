package guiFrameWork.renderer;

import java.awt.Graphics;
import java.awt.Image;
import java.util.ArrayList;
import java.util.List;

import javax.swing.ImageIcon;

import guiFrameWork.RenderingLevel;

public class BloodAndGutsRenderer extends AbstractRenderer {

	private Image blood;

	private List<Image> images;

	public BloodAndGutsRenderer(int x, int y) {
		super(RenderingLevel.BLOOD_AND_GUTS, x, y);
		blood = getPuddleOfBlood();
	}

	private Image getPuddleOfBlood() {
		double random = Math.random() * 10;
		double range = (10 / images.size());

		for (int i = 0; i < images.size(); i++) {
			if (random >= i * range && random < (i + 1) * range)
				return images.get(i);
		}
		if (images.iterator().hasNext()) {
			return images.iterator().next();
		}
		return null;
	}

	public void render(Graphics g) {
		drawImage(blood, getPositionX(), getPositionY(), g);
	}

	@Override
	protected void initImages() {
		images = new ArrayList<>();
		images.add(new ImageIcon(getClass().getResource("/images/blood_splat_01.png")).getImage());
		images.add(new ImageIcon(getClass().getResource("/images/blood_splat_02.png")).getImage());
		images.add(new ImageIcon(getClass().getResource("/images/blood_splat_03.png")).getImage());
	}

}
