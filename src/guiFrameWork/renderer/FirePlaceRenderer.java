package guiFrameWork.renderer;

import java.awt.Graphics;
import java.awt.Image;
import java.util.ArrayList;
import java.util.List;

import javax.swing.ImageIcon;

import gameObjects.FirePlace;
import guiFrameWork.RenderingLevel;

public class FirePlaceRenderer extends AbstractRenderer {

	private SequencedPicturesRenderer videoRenderer;
	private List<Image> light;

	int maxHeight;
	int maxWidth;

	int lightIndex = 0;
	double random = 1;
	long lastChangeTime;

	public FirePlaceRenderer(FirePlace firePlace) {
		super(firePlace, RenderingLevel.GAME_OBJECTS);
		videoRenderer = new SequencedPicturesRenderer(firePlace, RenderingLevel.GAME_OBJECTS, pics, -150, -150);
		lastChangeTime = System.nanoTime() / 1000000;
	}

	@Override
	protected void render(Graphics g) {
		drawLightShiver(g);
		videoRenderer.renderAll(g);
	}

	private void drawLightShiver(Graphics g) {
		drawCenteredImageInRectangle(calculateIndex(), g);
	}

	private int calculateIndex() {

		long time = System.nanoTime() / 1000000;
		long smallTimeUnit = time - lastChangeTime;
		if (smallTimeUnit > 80 * random) {
			lightIndex = (int) time % light.size();
			lightIndex = lightIndex < 0 ? lightIndex + light.size() : lightIndex;
			random = Math.random();
			lastChangeTime = System.nanoTime() / 1000000;
		}
		return lightIndex;
	}

	private void drawCenteredImageInRectangle(int index, Graphics g) {
		Image image = light.get(index);

		double height = image.getHeight(null) / getResolution().getHeightFactor();
		double width = image.getWidth(null) / getResolution().getWidthFactor();

		int x = (int) (getPositionX() - 150 + (maxWidth - width) / 2);
		int y = (int) (getPositionY() - 150 + (maxHeight - height) / 2);

		drawImage(image, x, y, g);

	}

	@Override
	protected void initImages() {

		pics = new ArrayList<>();
		pics.add(new ImageIcon(getClass().getResource("/images/feuertonne_1.png")).getImage());
		pics.add(new ImageIcon(getClass().getResource("/images/feuertonne_2.png")).getImage());
		pics.add(new ImageIcon(getClass().getResource("/images/feuertonne_3.png")).getImage());
		pics.add(new ImageIcon(getClass().getResource("/images/feuertonne_4.png")).getImage());
		pics.add(new ImageIcon(getClass().getResource("/images/feuertonne_5.png")).getImage());

		Image tonne1 = new ImageIcon(getClass().getResource("/images/feuertonne_schein.png")).getImage();
		Image outlineImage = resizeImageByPercent(tonne1, 100);

		maxHeight = outlineImage.getHeight(null);
		maxWidth = outlineImage.getWidth(null);

		List<Image> lightInitialiser = new ArrayList<>();
		lightInitialiser.add(outlineImage);
		lightInitialiser.add(resizeImageByPercent(tonne1, 96));
		lightInitialiser.add(resizeImageByPercent(tonne1, 92));
		lightInitialiser.add(resizeImageByPercent(tonne1, 88));
		lightInitialiser.add(resizeImageByPercent(tonne1, 84));
		lightInitialiser.add(resizeImageByPercent(tonne1, 80));
		lightInitialiser.add(resizeImageByPercent(tonne1, 84));
		lightInitialiser.add(resizeImageByPercent(tonne1, 88));
		lightInitialiser.add(resizeImageByPercent(tonne1, 92));
		lightInitialiser.add(resizeImageByPercent(tonne1, 96));

		light = new ArrayList<>();
		for (Image image : lightInitialiser) {
			light.add(autoResizeImage(image));
		}
	}
}