package guiFrameWork.renderer;

import framework.AbstractMoveableGameObject;

public class EndBossRenderer extends AbstractMoveableRenderer {

	public EndBossRenderer(AbstractMoveableGameObject endBoss) {
		super(endBoss);
	}

	@Override
	protected void initImages() {
		north = autoResizeImage("endboss_n.png");
		east = autoResizeImage("endboss_e.png");
		south = autoResizeImage("endboss_s.png");
		west = autoResizeImage("endboss_w.png");
	}
}
