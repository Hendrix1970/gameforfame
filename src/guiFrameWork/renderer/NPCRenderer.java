package guiFrameWork.renderer;

import java.awt.Graphics;
import java.awt.Image;

import NPCs.AbstractQuestGiver;
import guiFrameWork.RenderingLevel;

public class NPCRenderer extends SequencedPicturesRenderer {

	public NPCRenderer(AbstractQuestGiver abstractGameObject, Image image) {
		super(abstractGameObject, RenderingLevel.GAME_OBJECTS, image);
	}

	@Override
	protected void render(Graphics g) {
		super.render(g);
	}
}
