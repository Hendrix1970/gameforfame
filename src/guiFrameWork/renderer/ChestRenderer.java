package guiFrameWork.renderer;

import java.awt.Graphics;
import java.awt.Image;
import java.util.ArrayList;

import framework.AbstractGameObject;
import gameObjects.Chest;
import guiFrameWork.RenderingLevel;

public class ChestRenderer extends AbstractRenderer {

	public ChestRenderer(AbstractGameObject chest) {
		super(chest, RenderingLevel.GAME_OBJECTS);
	}

	@Override
	protected void initImages() {
		pics = new ArrayList<>();
//        pics.add(autoResizeImage("kiste_zu.png"));
		Image autoResizeImage = autoResizeImage("kiste_offen.png");
		autoResizeImage.getHeight(null);
		pics.add(autoResizeImage);
		Image autoResizeImage2 = autoResizeImage("chest.png");
		Image scaledInstance = autoResizeImage2.getScaledInstance(autoResizeImage.getHeight(null) + 20,
				autoResizeImage.getWidth(null) + 20, 0);
		pics.add(scaledInstance);
	}

	@Override
	protected void render(Graphics g) {
		if (((Chest) rendereredObject).isOpen()) {
			drawImage(pics.get(1), getPositionX(), getPositionY(), g);
		} else {
			drawImage(pics.get(0), getPositionX(), getPositionY(), g);
		}
	}

}