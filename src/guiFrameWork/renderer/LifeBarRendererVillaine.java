package guiFrameWork.renderer;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;

import javax.swing.ImageIcon;

import framework.AbstractFightable;
import guiFrameWork.RenderingLevel;
import guiFrameWork.ResolutionUtil;

public class LifeBarRendererVillaine extends AbstractRenderer {

	private AbstractFightable villaine;
	Image liveBarImage;

	public LifeBarRendererVillaine(AbstractFightable villaine) {
		super(RenderingLevel.HUD, 0, 0);
		this.villaine = villaine;

		liveBarImage = ResolutionUtil.autoResizeImage(getResolution(),
				new ImageIcon(getClass().getResource("/images/balken_stift.png")).getImage());
	}

	@Override
	protected void render(Graphics g) {
		double liveBar = (double) Math.max(villaine.getHealth(), 0) / villaine.getMaxHealth() * 150;
		g.setColor(Color.black);
		g.fillRect(getResolution().getWidth() - fitX(200), fitY(20), fitX(150), fitY(22));
		g.setColor(Color.red);
		g.fillRect(getResolution().getWidth() - fitX(200), fitY(20), fitX((int) liveBar), fitY(22));
		g.drawImage(liveBarImage, getResolution().getWidth() - fitX(260), 0, null);
	}

	private int fitX(int i) {
		return ResolutionUtil.recalculateX(i, getResolution());
	}

	private int fitY(int i) {
		return ResolutionUtil.recalculateX(i, getResolution());
	}

}
