package keys;

import java.awt.event.KeyEvent;

import engine.InteractionHandler;
import engine.ScreenStateApadter;
import framework.CharacterMenuRenderer;
import framework.ScreenStates;
import guiFrameWork.RenderingLevel;
import guiFrameWork.TheRenderingMachine;
import heroStuff.characterClasses.Hero;
import interactiveMenues.AbstractOptionManager;
import interactiveMenues.DialogManager;
import interactiveMenues.FightingManager;
import interactiveMenues.InventoryManager;
import interactiveMenues.SkillManager;

public class MenuKeyInput {

    private ScreenStateApadter screenStateApadter;
    private InteractionHandler interactionHandler;
    private TheRenderingMachine theRenderingMachine;
    private Hero hero;

    public MenuKeyInput(Hero hero, ScreenStateApadter screenStateApadter, TheRenderingMachine renderingMachine,
            InteractionHandler interactionHandler) {
        this.hero = hero;
        this.screenStateApadter = screenStateApadter;
        this.theRenderingMachine = renderingMachine;
        this.interactionHandler = interactionHandler;
    }

    public void interactiveMenuKeyRelease(int key) {
        AbstractOptionManager<?> singleActiveOptionManager = interactionHandler.getOptionManager();

        // Base functionality
        if (key == KeyEvent.VK_DOWN) {
            singleActiveOptionManager.optionDown();
        }
        else if (key == KeyEvent.VK_UP) {
            singleActiveOptionManager.optionUp();
        }
        else if (key == KeyEvent.VK_LEFT) {
            singleActiveOptionManager.optionLeft();
        }
        else if (key == KeyEvent.VK_RIGHT) {
            singleActiveOptionManager.optionRight();
        }
        else if (key == KeyEvent.VK_SPACE || key == KeyEvent.VK_ENTER) {
            singleActiveOptionManager.selectOption();
        }

        // Escape buttons
        else if ((key == KeyEvent.VK_I && singleActiveOptionManager instanceof InventoryManager)
                || (key == KeyEvent.VK_S && singleActiveOptionManager instanceof SkillManager)) {
            escapeInteractiveMenu();
        }
        else if (key == KeyEvent.VK_ESCAPE && singleActiveOptionManager != null && !(singleActiveOptionManager instanceof FightingManager)
                && !(singleActiveOptionManager instanceof DialogManager)) {
            escapeInteractiveMenu();
        }
        else {
            menuKeyReleased(key);
        }
        if (screenStateApadter.getScreenState() == ScreenStates.FIGHT && (key == KeyEvent.VK_ESCAPE)) {
            ((FightingManager) singleActiveOptionManager).cancelAttack();
        }
    }

    private void escapeInteractiveMenu() {
        screenStateApadter.setScreenState(ScreenStates.inGame);
        theRenderingMachine.clearRenderingLevel(RenderingLevel.INTERFACES);
    }

    protected void menuKeyReleased(int key) {
        if (key == KeyEvent.VK_Q) {
            theRenderingMachine.clearRenderingLevel(RenderingLevel.INTERFACES);
            screenStateApadter.setScreenState(ScreenStates.questLog);
            interactionHandler.openQuestLog();
        }
        if (key == KeyEvent.VK_C) {
            theRenderingMachine.clearRenderingLevel(RenderingLevel.INTERFACES);
            theRenderingMachine.registerRenderer(new CharacterMenuRenderer(hero), RenderingLevel.INTERFACES);
            screenStateApadter.setScreenState(ScreenStates.characterMenu);
        }
        if (key == KeyEvent.VK_I) {
            theRenderingMachine.clearRenderingLevel(RenderingLevel.INTERFACES);
            screenStateApadter.setScreenState(ScreenStates.INTERACTIVE_MENU);
            interactionHandler.openInventory();
        }
        if (key == KeyEvent.VK_S) {
            theRenderingMachine.clearRenderingLevel(RenderingLevel.INTERFACES);
            screenStateApadter.setScreenState(ScreenStates.INTERACTIVE_MENU);
            interactionHandler.openSkillScreen();
        }
    }
}
