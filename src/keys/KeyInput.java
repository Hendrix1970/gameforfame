package keys;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import engine.InteractionHandler;
import engine.ScreenStateApadter;
import framework.Direction;
import framework.ScreenStates;
import guiFrameWork.RenderingLevel;
import guiFrameWork.TheRenderingMachine;
import heroStuff.characterClasses.Hero;

public class KeyInput extends KeyAdapter {

    private Hero hero;
    private TheRenderingMachine theRenderingMachine;
    private InteractionHandler interactionHandler;
    private ScreenStateApadter screenStateApadter;

    private boolean north = false;
    private boolean south = false;
    private boolean west = false;
    private boolean east = false;
    private boolean entersInteraction = false;

    private MenuKeyInput menuInput;

    public KeyInput(Hero hero, ScreenStateApadter screenStateApadter, TheRenderingMachine renderingMachine,
            InteractionHandler interactionHandler) {
        this.hero = hero;
        this.screenStateApadter = screenStateApadter;
        this.theRenderingMachine = renderingMachine;
        this.interactionHandler = interactionHandler;

        menuInput = new MenuKeyInput(hero, screenStateApadter, renderingMachine, interactionHandler);
    }

    public void keyPressed(KeyEvent keyEvent) {
        int key = keyEvent.getKeyCode();
        if (screenStateApadter.getScreenState() == ScreenStates.inGame) {
            inGameKeyPress(key);
        }
    }

    public void keyReleased(KeyEvent keyEvent) {
        int key = keyEvent.getKeyCode();

        if (screenStateApadter.getScreenState() == ScreenStates.characterMenu) {
            characterMenuButtonReleased(key);
        }
        else if (screenStateApadter.getScreenState() == ScreenStates.inGame) {
            menuInput.menuKeyReleased(key);
            ;
        }
        else if (screenStateApadter.getScreenState() == ScreenStates.questLog) {
            questLogKeyReleased(key);
        }
        else if (screenStateApadter.getScreenState() == ScreenStates.INTERACTIVE_MENU) {
            if (!entersInteraction) {
                menuInput.interactiveMenuKeyRelease(key);
            }
        }
        else if (screenStateApadter.getScreenState() == ScreenStates.FIGHT) {
            if (!entersInteraction) {
                menuInput.interactiveMenuKeyRelease(key);
            }
        }
        if (key == KeyEvent.VK_DOWN) {
            south = false;
            if (west) {
                hero.setDirection(Direction.WEST);
            }
            else if (east) {
                hero.setDirection(Direction.EAST);
            }
            else {
                hero.setDirection(Direction.NONE);
            }
        }
        if (key == KeyEvent.VK_UP) {
            north = false;
            if (west) {
                hero.setDirection(Direction.WEST);
            }
            else if (east) {
                hero.setDirection(Direction.EAST);
            }
            else {
                hero.setDirection(Direction.NONE);
            }
        }
        if (key == KeyEvent.VK_LEFT) {
            west = false;
            if (north) {
                hero.setDirection(Direction.NORTH);
            }
            else if (south) {
                hero.setDirection(Direction.SOUTH);
            }
            else {
                hero.setDirection(Direction.NONE);
            }
        }
        if (key == KeyEvent.VK_RIGHT) {
            east = false;
            if (north) {
                hero.setDirection(Direction.NORTH);
            }
            else if (south) {
                hero.setDirection(Direction.SOUTH);
            }
            else {
                hero.setDirection(Direction.NONE);
            }
        }
        if (key == KeyEvent.VK_SPACE || key == KeyEvent.VK_ENTER) {
            entersInteraction = false;
        }
    }

    public void questLogKeyReleased(int key) {
        if (key == KeyEvent.VK_Q) {
            theRenderingMachine.clearRenderingLevel(RenderingLevel.INTERFACES);
            screenStateApadter.setScreenState(ScreenStates.inGame);
        }
        else {
            menuInput.interactiveMenuKeyRelease(key);
        }
    }

    private void characterMenuButtonReleased(int key) {
        if (key == KeyEvent.VK_C) {
            theRenderingMachine.clearRenderingLevel(RenderingLevel.INTERFACES);
            screenStateApadter.setScreenState(ScreenStates.inGame);
        }
        else {
            menuInput.menuKeyReleased(key);
        }
    }

    private void inGameKeyPress(int key) {
        if (key == KeyEvent.VK_DOWN && north == false) {
            south = true;
            hero.setDirection(Direction.SOUTH);
        }
        if (key == KeyEvent.VK_UP && south == false) {
            north = true;
            hero.setDirection(Direction.NORTH);
        }
        if (key == KeyEvent.VK_LEFT && east == false) {
            west = true;
            hero.setDirection(Direction.WEST);
        }
        if (key == KeyEvent.VK_RIGHT && west == false) {
            east = true;
            hero.setDirection(Direction.EAST);
        }
        if (key == KeyEvent.VK_SPACE || key == KeyEvent.VK_ENTER) {
            entersInteraction = true;
            interactionHandler.interact();
        }
    }
}
