package quests;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import dataStructure.DialogDts;
import dataStructure.QuestRewardDts;

public abstract class Quest {

    protected String name;
    protected String text;
    protected QuestState state = QuestState.newQuest;
    private QuestRewardDts reward;

    public List<DialogDts> getDialog() {
        Map<QuestState, List<DialogDts>> dialog = new HashMap<>();
        newQuest(dialog);
        inProgress(dialog);
        done(dialog);
        closed(dialog);

        return dialog.get(state);
    }

    protected abstract void closed(Map<QuestState, List<DialogDts>> dialog);

    protected abstract void done(Map<QuestState, List<DialogDts>> dialog);

    protected abstract void inProgress(Map<QuestState, List<DialogDts>> dialog);

    protected abstract void newQuest(Map<QuestState, List<DialogDts>> dialog);

    public abstract String getStatusText();

    public abstract void progress();

    public String getName() {
        return name;
    }

    public QuestState getQuestState() {
        return state;
    }

    public void setQuestState(QuestState questState) {
        state = questState;
    }

    public QuestRewardDts getReward() {
        return reward;
    }

    public void setReward(QuestRewardDts reward) {
        this.reward = reward;
    }
}
