package quests;

import java.awt.Graphics;
import java.util.ArrayList;
import java.util.List;

import dataStructure.QuestRewardDts;
import framework.AbstractMenuRenderer;

// Sieht ganz so aus, als wäre diese Klasse nie integriert worden.
public class QuestMenuRenderer extends AbstractMenuRenderer {
    private List<Quest> quests = new ArrayList<>();

    @Override
    public void render(Graphics g) {
        QuestRenderer questRenderer = new QuestRenderer(quests);
        questRenderer.render(g);
    }

    public void activateQuests(String questName) {
        for (Quest quest : quests) {
            if (quest.getName().equals(questName)) {
                quest.setQuestState(QuestState.inProgress);
            }
        }
    }

    public QuestRewardDts finishQuests(String questName) {
        for (Quest quest : quests) {
            if (quest.getName().equals(questName)) {
                quest.setQuestState(QuestState.closed);
                return quest.getReward();
            }
        }
        return new QuestRewardDts(0, 0, null);
    }

    public List<Quest> getQuestList() {
        return quests;
    }

    public boolean hasQuests(String questName) {
        for (Quest tempQuest : quests) {
            if (tempQuest.getName().equals(questName)) {
                if (tempQuest.getQuestState() != QuestState.newQuest)
                    return true;
            }
        }
        return false;
    }

    public Quest getQuest(String questName) {
        for (Quest quest : quests) {
            if (quest.getName().equals(questName)) {
                return quest;
            }
        }
        return null;
    }

    public QuestState getStateOfQuest(String questName) {
        for (Quest quest : quests) {
            if (quest.getName().equals(questName)) {
                return quest.getQuestState();
            }
        }
        return null;

    }

}
