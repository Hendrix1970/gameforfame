package quests;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import quests.quest.FirstQuest;
import quests.quest.KillQuest;
import quests.quest.SecondQuest;

public class QuestLog {

	private List<Quest> quests;

	public QuestLog() {
		initiateLevelOneQuests();
	}

	private void initiateLevelOneQuests() {
		quests = new ArrayList<Quest>();
		quests.add(new FirstQuest());
		quests.add(new SecondQuest());
	}

	public Quest getQuest(String questName) {
		for (Quest tempQuest : quests) {
			if (tempQuest.getName().equals(questName)) {
				return tempQuest;
			}
		}
		return null;
	}

	public QuestState getQuestState(String key) {
		for (Quest tempQuest : quests) {
			if (tempQuest.getName().equals(key)) {
				return tempQuest.getQuestState();
			}
		}
		return QuestState.newQuest;
	}

	public List<Quest> getQuests() {
		return quests;
	}

	public List<Quest> getActiveQuests() {
		List<Quest> activeQuests = quests.stream().filter(q -> q.getQuestState() != QuestState.newQuest)
				.collect(Collectors.toList());
		return activeQuests.isEmpty() ? new ArrayList<>() : activeQuests;
	}

	public void setQuests(List<Quest> quests) {
		this.quests = quests;
	}

	public void activateQuests(String questName) {
		for (Quest quest : quests) {
			if (quest.getName().equals(questName)) {
				quest.setQuestState(QuestState.inProgress);
			}
		}
	}

	public void registerKill(EnemyType enemyType) {
		getActiveQuests().stream().filter(q -> q instanceof KillQuest).map(q -> (KillQuest) q)
				.filter(q -> q.getEnemyType() == enemyType).forEach(q -> q.progress());
	}

}
