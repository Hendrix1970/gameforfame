package quests;

public enum EnemyType {
    chicken;

    public String getNameAsString() {
        if (this.equals(EnemyType.chicken)) {
            return "Hühner";
        }
        return "Unbekannt";
    }
}