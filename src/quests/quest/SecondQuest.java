package quests.quest;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import dataStructure.DialogDts;
import dataStructure.QuestRewardDts;
import interactiveMenues.Answer;
import quests.EnemyType;
import quests.QuestKey;
import quests.QuestState;

public class SecondQuest extends KillQuest {

    public SecondQuest() {
        super(QuestKey.CHICKEN_QUEST_2, EnemyType.chicken, 5, new QuestRewardDts(20, 5, null));
        setQuestGiverName("Tutorial Franz");
    }

    protected void newQuest(Map<QuestState, List<DialogDts>> dialog) {
        List<DialogDts> newQuest = new ArrayList<>();

        List<Answer> answers1 = new ArrayList<>();
        answers1.add(new Answer("Öh ja, ist mir zu Ohren gekommen.", 1, 1));
        answers1.add(new Answer("Lass mich in Ruhe du Irrer! (Exit)", 2, 0, true));
        answers1.add(new Answer("Wadde Hadde Dude Da? (Exit)", 3, 0, true));
        DialogDts dts = new DialogDts(getQuestGiverName(),
                "Hallo Fremder, wie Du vielleicht schon gehört hast sind hier die Hühner los.", answers1, 0);
        newQuest.add(dts);

        List<Answer> answers2 = new ArrayList<>();
        answers2.add(new Answer("Kümmer dich doch selst drum du Hühnerhitler. (Exit)", 1, 0, true));
        answers2.add(new Answer("Warum nicht, hab eh gerade Hunger auf Chicken Wings.", 2, 2, false));
        dts = new DialogDts(getQuestGiverName(),
                "Im Nebenraum ist eine große Horde wildgewordener Hünchen. Seid doch so nett und bereinigt diese für mich.",
                answers2, 1);
        newQuest.add(dts);

        List<Answer> answers3 = new ArrayList<>();
        answers3.add(new Answer("...", 1, 3, false));
        dts = new DialogDts(getQuestGiverName(),
                "Im nächsten Raum weiter wirst du auf Ninja-Hühner treffen. Sie sind besonders schwierig zu treffen. "
                        + "Das liegt daran, dass sie so geschickt sind. Im Kampf wird die eigene Geschicklich mit der des Gegners "
                        + "abgeglichen. Je höher die Differenz, desto einfacher (oder schwieriger) ist es den Gegner zu "
                        + "treffen. Einige Gegenstände haben Eingenschaftsboni. Dein Bleistift ist eine Stichwaffe die, für "
                        + "Federtascheninhalt, verhältnissmäßig viel Schaden macht...",
                answers3, 2);
        newQuest.add(dts);

        List<Answer> answers4 = new ArrayList<>();
        answers4.add(new Answer("Alles klar, ich werd's versuchen. (Exit)", 1, 0, true, this,
                QuestKey.CHICKEN_QUEST_2));
        dts = new DialogDts(getQuestGiverName(),
                "...Aber das Lineal ist besser geeignet um wendige Gegner zu treffen, auch wenn es weniger Schaden macht. "
                        + "Drücke die 'I' Taste um dein Inventar zu öffnen. Dort kannst du dir deine Ausrüstung anschauen, Konsumgüter konsumieren "
                        + "und Ausrüstungsgegenstände Tauschen oder ablegen. Bevor du dich mit den Ninja-Hühnern anlegst, gehe also am besten in dein"
                        + " Inventar und rüste das Lineal aus.",
                answers4, 3);
        newQuest.add(dts);

        dialog.put(QuestState.newQuest, newQuest);
    }

    protected void inProgress(Map<QuestState, List<DialogDts>> dialog) {
        List<DialogDts> inProgress = new ArrayList<>();

        List<Answer> answers1 = new ArrayList<>();
        answers1.add(new Answer("Ich arbeite dran. (Exit)", 1, 1, true));
        DialogDts dts = new DialogDts(getQuestGiverName(), "Kommt Ihr gut voran?", answers1, 0);
        inProgress.add(dts);

        dialog.put(QuestState.inProgress, inProgress);

    }

    protected void done(Map<QuestState, List<DialogDts>> dialog) {
        List<DialogDts> done = new ArrayList<>();
        List<Answer> answers1 = new ArrayList<>();
        answers1.add(new Answer("Gern geschehen. (Exit)", 1, 1, true));
        DialogDts dts = new DialogDts(getQuestGiverName(), "Vielen Dank, endlich kann ich wieder diesen Raum verlassen.",
                answers1, 0);
        done.add(dts);
        dialog.put(QuestState.done, done);
    }

    protected void closed(Map<QuestState, List<DialogDts>> dialog) {
        List<DialogDts> closed = new ArrayList<>();
        List<Answer> answers1 = new ArrayList<>();
        answers1.add(new Answer("Ja ich bin super, ich weiß. (Exit)", 1, 1, true));
        DialogDts dts = new DialogDts(getQuestGiverName(), "Da ist ja unser Held. Vielen Dank nochmal.", answers1, 0);
        closed.add(dts);
        dialog.put(QuestState.closed, closed);
    }
}
