package quests.quest;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import dataStructure.DialogDts;
import dataStructure.QuestRewardDts;
import gameObjects.ChickenWing;
import gameObjects.Key;
import gameObjects.Items.Item;
import interactiveMenues.Answer;
import quests.EnemyType;
import quests.QuestKey;
import quests.QuestState;

public class FirstQuest extends KillQuest {

	String questGiverName = "Tutorial Horst";

	public FirstQuest() {
		super(QuestKey.CHICKEN_QUEST, EnemyType.chicken, 3, questReward());
	}

	private static QuestRewardDts questReward() {
		List<Item> items = new ArrayList<>();
		items.add(new ChickenWing(0, 0));
		items.add(new Key(0, 0, "questEins"));
		return new QuestRewardDts(20, 5, items);
	}

	protected void newQuest(Map<QuestState, List<DialogDts>> dialog) {
		List<DialogDts> newQuest = new ArrayList<>();
		List<Answer> answers1 = new ArrayList<>();
		answers1.add(new Answer("Interessant! Was für ein Problem ist das?", 1, 1));
		answers1.add(new Answer("Aha... (Exit)", 2, 0, true));
		DialogDts dts = new DialogDts(questGiverName, "Hallo Fremder, ich habe ein Hünchenproblem!", answers1, 0);
		newQuest.add(dts);

		List<Answer> answers2 = new ArrayList<>();
		answers2.add(new Answer("Nö (Exit)", 1, 0, true));
		answers2.add(new Answer("Wird Gemacht, ich hasse Hühner.", 2, 2));
		dts = new DialogDts(questGiverName,
				"Im Nebenraum ist eine Horde wildgewordener Hühnchen.\nKönntet Ihr mir behilflich sein diese zu eleminieren?",
				answers2, 1);
		newQuest.add(dts);

		List<Answer> answers3 = new ArrayList<>();
		answers3.add(new Answer("Ok (Exit)", 1, 0, true, this, QuestKey.CHICKEN_QUEST));
		answers3.add(new Answer("Warte, ich soll Hühner verletzen? Was war noch das Problem.", 2, 1));
		dts = new DialogDts(questGiverName,
				"Vielen Dank, meldet euch wieder bei mir wenn die Hühner nurnoch\nBlutlachen sind.", answers3, 2);
		newQuest.add(dts);
		dialog.put(QuestState.newQuest, newQuest);
	}

	protected void inProgress(Map<QuestState, List<DialogDts>> dialog) {
		List<DialogDts> inProgress = new ArrayList<>();
		List<Answer> answers4 = new ArrayList<>();
		answers4.add(new Answer("Ich arbeite dran. (Exit)", 1, 1, true));
		DialogDts dts = new DialogDts(questGiverName, "Kommt Ihr gut voran?", answers4, 0);
		inProgress.add(dts);
		dialog.put(QuestState.inProgress, inProgress);
	}

	protected void done(Map<QuestState, List<DialogDts>> dialog) {
		List<DialogDts> done = new ArrayList<>();
		List<Answer> answers5 = new ArrayList<>();
		answers5.add(new Answer("Gern geschehen. (Exit)", 1, 1, true, this, QuestKey.CHICKEN_QUEST));
		DialogDts dts = new DialogDts(questGiverName, "Vielen Dank, endlich kann ich wieder diesen Raum verlassen.",
				answers5, 0);
		done.add(dts);
		dialog.put(QuestState.done, done);
	}

	protected void closed(Map<QuestState, List<DialogDts>> dialog) {
		List<DialogDts> closed = new ArrayList<>();
		List<Answer> answers6 = new ArrayList<>();
		answers6.add(new Answer("Ja ich bin super, ich weiß. (Exit)", 1, 1, true));
		DialogDts dts = new DialogDts(questGiverName, "Da ist ja unser Held. Vielen Dank nochmal.", answers6, 0);
		closed.add(dts);
		dialog.put(QuestState.closed, closed);
	}
}
