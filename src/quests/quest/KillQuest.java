package quests.quest;

import dataStructure.QuestRewardDts;
import quests.EnemyType;
import quests.Quest;
import quests.QuestState;

public abstract class KillQuest extends Quest {

    protected EnemyType enemyType;
    protected int conditionAmount = 3;
    protected int statusAmount = 0;

    private String questGiverName = "Tutorial Heinz";

    public KillQuest(String questName, EnemyType enemyType, int conditionAmount, QuestRewardDts reward) {
        this.name = questName;
        this.enemyType = enemyType;
        this.conditionAmount = conditionAmount;
        this.setReward(reward);
    }

    public String getStatusText() {
        String anzeigeText = "";
        if (statusAmount < conditionAmount) {
            anzeigeText = "Sie haben " + statusAmount + "/" + conditionAmount + "\n" + enemyType.getNameAsString() + " getötet.";
        }
        else {
            anzeigeText = "Alle " + enemyType.getNameAsString() + " sind \nerledigt. Kehren sie  \nzu " + getQuestGiverName()
                    + " \nzurück und streichen \nIhre Belohnung ein.";
        }
        return anzeigeText;
    }

    public EnemyType getEnemyType() {
        return enemyType;
    }

    public void progress() {
        if (state == QuestState.inProgress) {
            statusAmount += 1;
            checkStatus();
        }
    }

    private void checkStatus() {
        if (statusAmount == conditionAmount) {
            state = QuestState.done;
        }
    }

    public String getQuestGiverName() {
        return questGiverName;
    }

    public void setQuestGiverName(String questGiverName) {
        this.questGiverName = questGiverName;
    }
}
