package quests;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.util.List;

import javax.swing.ImageIcon;

import constants.Constants;
import framework.AbstractMenuRenderer;

public class QuestRenderer extends AbstractMenuRenderer {

    private Font fontTitle = new Font("Courier", Font.BOLD, 35);
    private Font fontName = new Font("Courier", Font.BOLD, 20);
    private Font fontText = new Font("Courier", Font.PLAIN, 20);

    Image questLogImage = new ImageIcon(getClass().getResource("/images/menu.png")).getImage();
    List<Quest> quests;

    public QuestRenderer(List<Quest> quests) {
        this.quests = quests;
    }

    public void render(Graphics g) {
        int xQuestName = 150;
        int xQuestText = 450;
        int y = 260;
        g.drawImage(questLogImage, Constants.LARGE_MENU_X, Constants.LARGE_MENU_X, null);
        g.setFont(fontTitle);
        g.setColor(Color.BLACK);
        g.drawString("Quest Logg", 360, 200);
        for (Quest quest : quests) {
            if (quest.getQuestState().equals(QuestState.inProgress) || quest.getQuestState().equals(QuestState.done)) {
                g.setFont(fontName);
                g.drawString(quest.getName() + ":", xQuestName, y);
                g.setFont(fontText);
                drawString(quest.getStatusText(), xQuestText, y, g);
                y += g.getFontMetrics().getHeight() + 5;
            }
        }
    }
}
