package fighting;

public class FightStateAdapter {

	private FightState state;

	public FightStateAdapter(FightState state) {
		this.state = state;
	}

	public FightState getState() {
		return state;
	}

	public void setState(FightState state) {
		this.state = state;
	}
}
