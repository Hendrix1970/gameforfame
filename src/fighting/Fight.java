package fighting;

import java.util.List;

import Villains.Skill;
import Villains.VillainSkill;
import framework.AbstractFightable;
import framework.CountDown;
import framework.TextTile;
import heroStuff.characterClasses.Hero;
import heroStuff.skills.HeroSkill;
import interactiveMenues.MiniGame;

public class Fight {

    private Hero hero;
    private AbstractFightable villain;

    private FightStateAdapter stateAdapter;

    private List<HeroSkill> heroSkills;
    private List<VillainSkill> villainSkills;
    private HeroSkill activatedSkill;

    private MiniGame miniGame;

    private CountDown countDown;
    private boolean miniGameResolved;
    private boolean skip;
    private String text;
    private boolean canceled;
    private boolean ready = false;

    public Fight(Hero hero, AbstractFightable villain) {
        initialiseConstructor(hero, villain);
        stateAdapter = new FightStateAdapter(FightState.INIT);
    }

    public Fight(Hero hero, AbstractFightable villain, FightStateAdapter stateAdapter) {
        initialiseConstructor(hero, villain);
        this.stateAdapter = stateAdapter;
    }

    private void initialiseConstructor(Hero hero, AbstractFightable villain) {
        this.hero = hero;
        this.villain = villain;

        stateAdapter = new FightStateAdapter(FightState.INIT);
        heroSkills = hero.getActiveSkills();
        villainSkills = villain.getSkills();

        // Reset all cooldowns from other fights
        heroSkills.stream().forEach(skill -> skill.setCooldown(false));
        villainSkills.stream().forEach(skill -> skill.setCooldown(false));
    }

    public void tick() {
        FightState state = stateAdapter.getState();
        if (state == FightState.INIT) {
            init();
        }
        else if (state == FightState.HERO_OPTION) {
            heroOption();
        }
        else if (state == FightState.MINI_GAME) {
            miniGame();
        }
        else if (state == FightState.HERO_INFO) {
            heroInfo();
        }
        else if (state == FightState.VILLAIN_OPTION) {
            villainOption();
        }
        else if (state == FightState.VILLAIN_INFO) {
            villainInfo();
        }
        else if (state == FightState.END_STEP) {
            endStep();
        }
        else if (state == FightState.END_FIGHT) {
            endFight();
        }
    }

    private void init() {
        if (skip == true && ready) {
            countDown = null;
            skip = false;

            // initiativ Wurf
            if (Math.random() > 0.5) {
                stateAdapter.setState(FightState.VILLAIN_OPTION);
            }
            else {
                stateAdapter.setState(FightState.HERO_OPTION);
            }
        }
    }

    private void heroOption() {
        if (activatedSkill != null && !activatedSkill.isCooldownActive()) {
            refreshCooldowns();

            activatedSkill.calculateAndSetHit(villain);
            activatedSkill.activateCooldown();
            if (activatedSkill.isHit()) {
                if (activatedSkill.isMiniGame()) {
                    stateAdapter.setState(FightState.MINI_GAME);
                    miniGame = new MiniGame(activatedSkill, villain);
                }
                else {
                    activatedSkill.executeSkill(villain, 1);
                    stateAdapter.setState(FightState.HERO_INFO);
                }
            }
            else {
                stateAdapter.setState(FightState.HERO_INFO);
            }
        }
    }

    private void refreshCooldowns() {
        if (!canceled) {
            heroSkills.stream().forEach(skill -> skill.updateCooldown());
        }
        canceled = false;
    }

    private void miniGame() {
        if (!miniGameResolved) {
            miniGame.tick();
            text = activatedSkill.getName();
        }
        else {
            activatedSkill.executeSkill(villain, miniGame.select());
            // After the skill resolves it should be set to null again
            miniGameResolved = false;
            miniGame = null;
            stateAdapter.setState(FightState.HERO_INFO);
        }
    }

    private void heroInfo() {
        if (countDown == null) {
            countDown = new CountDown(15);
            if (activatedSkill.isHit()) {
                text = activatedSkill.getSuccessText();
            }
            else {
                text = activatedSkill.getFailureText();
            }

            activatedSkill = null;
        }
        else if (skip == true || countDown.timeReached()) {
            countDown = null;
            skip = false;
            // TODO Implement
            stateAdapter.setState(FightState.VILLAIN_OPTION);
        }
    }

    private void villainOption() {
        if (villain.getHealth() < 0) {
            stateAdapter.setState(FightState.END_STEP);
        }
        else if (countDown == null) {
            countDown = new CountDown(15);
            text = villain.retrieveText(TextTile.VILLAIN_OPTION);
        }
        else if (skip == true || countDown.timeReached()) {
            countDown = null;
            skip = false;
            villainSkills.get(0).executeSkill(hero);
            int damage = villainSkills.get(0).getDamage();
            if (damage > 0) {
                text = villain.retrieveText(TextTile.DAMAGE_DEALT) + damage + " Schaden!";
            }
            else {
                text = villain.retrieveText(TextTile.DAMAGE_MISSED);
            }

            stateAdapter.setState(FightState.VILLAIN_INFO);
        }
    }

    private void villainInfo() {

        // TODO implement it in a way that this works
        // VillainSkill skill = villainSkills.get(0);
        // if (skill != null && !skill.isCooldown()) {
        // skill.executeSkill(hero);
        // }

        if (countDown == null) {
            countDown = new CountDown(15);
        }
        else if (skip == true || countDown.timeReached()) {

            countDown = null;
            skip = false;
            // TODO Implement
            stateAdapter.setState(FightState.END_STEP);
        }

    }

    private void endStep() {
        if (getHero().getHealth() <= 0) {
            text = "Ihr seid gestorben, Ihr seid eine jämmerlige Wurst!";
            stateAdapter.setState(FightState.END_FIGHT);
        }
        else if (villain.getHealth() <= 0) {
            text = villain.retrieveText(TextTile.END_FIGHT) + "\n" + "Seid stolz auf euch und nehmt diese "
                    + villain.getXpValue() + " Erfahrung mit auf eure Reise!";
            stateAdapter.setState(FightState.END_FIGHT);
        }
        else {
            stateAdapter.setState(FightState.HERO_OPTION);
        }
    }

    private void endFight() {
        if (skip == true) {
            if (getHero().getHealth() <= 0) {
                getHero().die();
            }
            if (villain.getHealth() <= 0) {
                villain.die();
            }
        }
    }

    public void registerSelection(int i) {
        if (stateAdapter.getState() == FightState.HERO_OPTION) {
            activatedSkill = heroSkills.get(i);
        }
        else if (stateAdapter.getState() == FightState.MINI_GAME) {
            miniGameResolved = true;
        }
        else {
            skip = true;
        }
    }

    public void startFight() {
        ready = true;
    }

    public MiniGame getMiniGame() {
        return miniGame;
    }

    public boolean isStillFighting() {
        return getHero().isAlive() && villain.isAlive();
    }

    public String getText() {
        return text;
    }

    public void cancelAttack() {
        stateAdapter.setState(FightState.HERO_OPTION);
        canceled = true;
        miniGame = null;
        miniGameResolved = false;
        activatedSkill = null;
    }

    public Hero getHero() {
        return hero;
    }

    public Skill getActivatedSkill() {
        return activatedSkill;
    }

}
