package fighting;

public enum FighteState {
	INIT, HERO_OPTIONS, VILLAIN, _OPTIONS, HERO_RESULT, VILLAIN_RESULT, MINIGAME
}
