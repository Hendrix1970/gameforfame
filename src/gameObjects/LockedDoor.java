package gameObjects;

import Rooms.Spawner;
import framework.DoorOrientation;
import framework.LevelName;
import framework.Unlockable;

public class LockedDoor extends Door implements Unlockable {

	private boolean locked = true;
	private String code;

	public LockedDoor(int x, int y, int height, int width, Spawner spawner, LevelName nextLevel,
			DoorOrientation orientation, String code) {
		super(x, y, height, width, spawner, nextLevel, orientation);
		this.code = code;
	}

	public boolean unlock(String code) {
		if (this.code.equals(code)) {
			setLocked(false);
			return true;
		}
		return false;
	}

	public boolean isLocked() {
		return locked;
	}

	public void setLocked(boolean locked) {
		this.locked = locked;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

}
