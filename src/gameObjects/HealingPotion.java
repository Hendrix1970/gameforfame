package gameObjects;

import javax.swing.ImageIcon;

import heroStuff.characterClasses.Hero;
import interactiveMenues.Consumable;

public class HealingPotion extends Consumable {

	int healingValue = 20;
	static int height = 30;
	static int width = 30;
	boolean onMap = true;

	public HealingPotion(int x, int y) {
		super(x, y, height, width);
		init();
	}

	public HealingPotion() {
		super(0, 0, height, width);
		init();
		onMap = false;
	}

	private void init() {
		setPortraitForInventar(new ImageIcon(getClass().getResource("/images/potion.png")).getImage());
//		setPortraitForInventar(new ImageIcon(getClass().getResource("/images/item-heiltrank.png")).getImage());
		setDescription("Eine rote, trübe Flüssigkeit.");
		setName("Heiltrank");
	}

	@Override
	public void tick() {

	}

	public boolean isOnMap() {
		return onMap;
	}

	public void setOnMap(boolean onMap) {
		this.onMap = onMap;
	}

	@Override
	public String getText() {
		return "Heilt " + healingValue + " Leben.";
	}

	@Override
	public boolean canBeConsumed(Hero hero) {
		return hero.getHealth() < hero.getMaxHealth();
	}

	@Override
	protected void use(Hero hero) {
		hero.heal(healingValue);
	}

}
