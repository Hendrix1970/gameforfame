package gameObjects;

import framework.AbstractGameObject;

public class Wall extends AbstractGameObject {

	public Wall(int x, int y, int height, int width) {
		super(x, y, height, width);
	}

	@Override
	public void tick() {
	}
}