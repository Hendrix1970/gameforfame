package gameObjects;

import Rooms.Spawner;
import framework.AbstractGameObject;

public class FirePlace extends AbstractGameObject {

	int height = 50;
	int width = 50;

	public FirePlace(int x, int y) {
		super(x, y, Spawner.OBJECT_STANDARD_SIZE, Spawner.OBJECT_STANDARD_SIZE);
	}

	@Override
	public void tick() {
		// nothing to do here
	}
}