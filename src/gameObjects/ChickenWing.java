package gameObjects;

import javax.swing.ImageIcon;

import heroStuff.characterClasses.Hero;
import interactiveMenues.Consumable;

public class ChickenWing extends Consumable {

	int healingValue = 10;
	static int height = 30;
	static int width = 30;
	boolean onMap = true;

	public ChickenWing() {
		super(0, 0, height, width);
		setPortraitForInventar(new ImageIcon(getClass().getResource("/images/chickenwing.png")).getImage());
		setDescription("Ein saftiger Wing... hm, lecker.");
		setName("Chicken Wing");
		setOnMap(false);
	}

	public ChickenWing(int x, int y) {
		super(x, y, height, width);
		setPortraitForInventar(new ImageIcon(getClass().getResource("/images/chickenwing.png")).getImage());
		setDescription("Ein saftiger Wing... hm, lecker.");
		setName("Chicken Wing");
	}

	@Override
	public void tick() {

	}

	public boolean isOnMap() {
		return onMap;
	}

	public void setOnMap(boolean onMap) {
		this.onMap = onMap;
	}

	@Override
	public String getText() {
		return "Heilt " + healingValue + " Leben.";
	}

	@Override
	public boolean canBeConsumed(Hero hero) {
		return hero.getHealth() < hero.getMaxHealth();
	}

	@Override
	protected void use(Hero hero) {
		hero.heal(healingValue);
	}

}
