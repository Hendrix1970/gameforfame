package gameObjects;

import Rooms.Spawner;
import constants.Constants;
import framework.AbstractGameObject;
import framework.Activatable;
import framework.DoorOrientation;
import framework.LevelName;

public class Door extends AbstractGameObject implements Activatable {

	private LevelName nextLevel;
	private DoorOrientation orientation;
	private boolean locked = false;

	public Door(int x, int y, int height, int width, Spawner spawner, LevelName nextLevel,
			DoorOrientation orientation) {
		super(x, y, height, width);
		this.nextLevel = nextLevel;
		this.orientation = orientation;
	}

	@Override
	public void tick() {
		// nothing to do here
	}

	public int getNewHeroKoordinatesX() {
		int i = 0;
		if (orientation == DoorOrientation.north || orientation == DoorOrientation.south) {
			i = (Constants.WIDTH - Spawner.HUMAN_STANDARD_SIZE) / 2;
		} else if (orientation == DoorOrientation.west) {
			i = Constants.WIDTH - Spawner.OBJECT_STANDARD_SIZE - Spawner.HUMAN_STANDARD_SIZE;
		} else if (orientation == DoorOrientation.east) {
			i = Spawner.OBJECT_STANDARD_SIZE;
		}
		return i;
	}

	public int getNewHeroKoordinatesY() {
		int i = 0;
		if (orientation == DoorOrientation.east || orientation == DoorOrientation.west) {
			i = (Constants.HEIGHT - Spawner.HUMAN_STANDARD_SIZE) / 2;
		} else if (orientation == DoorOrientation.north) {
			i = Constants.HEIGHT - Spawner.OBJECT_STANDARD_SIZE - Spawner.HUMAN_STANDARD_SIZE;
		} else if (orientation == DoorOrientation.south) {
			i = Spawner.OBJECT_STANDARD_SIZE;
		}
		return i;
	}

	public LevelName getNextLevel() {
		return nextLevel;
	}

	public void setNextLevel(LevelName nextLevel) {
		this.nextLevel = nextLevel;
	}

	public boolean isLocked() {
		return locked;
	}

	public void setLocked(boolean locked) {
		this.locked = locked;
	}
}
