package gameObjects;

import javax.swing.ImageIcon;

import heroStuff.characterClasses.Hero;
import interactiveMenues.Consumable;

public class Key extends Consumable {

	private String code;
	private static int height = 30;
	private static int width = 30;
	private boolean onMap = true;
	private String text;

	public Key(int x, int y, String code) {
		super(x, y, height, width);
		this.code = code;
		if (x == 0 && y == 0)
			onMap = false;
		init();
	}

	private void init() {
		setPortraitForInventar(new ImageIcon(getClass().getResource("/images/item-key.png")).getImage());
		setDescription("Ein Schlüssel, er ist rot.");
		setName("Schlüssel");
		setText("Öffnet eine Tür");
	}

	@Override
	public String getText() {
		return text;
	}

	@Override
	protected void use(Hero hero) {
		// Nothing to do here, is only used when interacting with something locked
	}

	@Override
	public boolean isOnMap() {
		return onMap;
	}

	@Override
	public void tick() {
		// TODO Auto-generated method stub

	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public void setText(String text) {
		this.text = text;
	}

}
