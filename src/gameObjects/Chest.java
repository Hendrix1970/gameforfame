package gameObjects;

import java.awt.Image;
import java.util.ArrayList;
import java.util.List;

import Rooms.Spawner;
import framework.AbstractGameObject;
import framework.Activatable;
import gameObjects.Items.Item;

public class Chest extends AbstractGameObject implements Activatable {
	Image table;
	static int height = Spawner.OBJECT_STANDARD_SIZE;
	static int width = Spawner.OBJECT_STANDARD_SIZE;
	boolean onMap = true;
	private boolean open = false;

	private List<Item> content;

	public Chest(int x, int y, Item... content) {
		super(x, y, height, width);
		List<Item> itemsAsList = new ArrayList<>();
		for (Item i : content)
			itemsAsList.add(i);
		this.content = itemsAsList;
	}

	public void tick() {
	}

	public boolean isOnMap() {
		return this.onMap;
	}

	public void setOnMap(boolean onMap) {
		this.onMap = onMap;
	}

	public List<Item> getContent() {
		return content;
	}

	public void setContent(List<Item> content) {
		this.content = content;
	}

	public boolean isOpen() {
		return open;
	}

	public void setOpen(boolean open) {
		this.open = open;
	}
}