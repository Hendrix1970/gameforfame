package gameObjects;

import framework.Unlockable;
import gameObjects.Items.Item;

public class LockedChest extends Chest implements Unlockable {

	private boolean locked = true;
	private String code;

	public LockedChest(int x, int y, Item[] content, String code) {
		super(x, y, content);
		this.code = code;
	}

	public boolean unlock(String code) {
		if (this.code.equals(code)) {
			setLocked(false);
			return true;
		}
		return false;
	}

	public boolean isLocked() {
		return locked;
	}

	public void setLocked(boolean locked) {
		this.locked = locked;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

}
