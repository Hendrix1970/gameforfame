package gameObjects.Items;

import java.awt.Image;

public class Shoe extends Item {

	private int defense;

	public Shoe(String name, Image image) {
		super(0, 0, 0, 0);
		setName(name);
		setPortraitForInventar(image);
	}

	@Override
	public void tick() {
		// nothing to do here
	}

}
