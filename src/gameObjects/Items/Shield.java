package gameObjects.Items;

import java.awt.Image;

public class Shield extends DefensiveItem {

    public Shield(String name, int defense, Image image) {
        super(name, defense, image);
    }

}
