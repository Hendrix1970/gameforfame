package gameObjects.Items;

import java.awt.Image;

public class Weapon extends Item {

	private int damage;

	public Weapon(String name, String description, int damage, Image image) {
		super(0, 0, 0, 0);
		setName(name);
		setDescription(description);
		setPortraitForInventar(image);
		this.damage = damage;
	}

	public Weapon(String name, int damage, Image image) {
		super(0, 0, 0, 0);
		setName(name);
		setDescription("Eine wahrlich gute Waffe");
		setPortraitForInventar(image);
		this.damage = damage;
	}

	public int getDamage() {
		return damage;
	}

	public void setDamage(int damage) {
		this.damage = damage;
	}

	@Override
	public void tick() {
		// nothing to do here
	}

}
