package gameObjects.Items;

import java.awt.Image;

public abstract class DefensiveItem extends Item {

    private int defense;

    public DefensiveItem(String name, int defense, Image image) {
        super(0, 0, 0, 0);
        setName(name);
        setPortraitForInventar(image);
        this.defense = defense;
    }

    public int getDefense() {
        return defense;
    }

    public void setDefenseWithEquipment(int damage) {
        this.defense = damage;
    }

    @Override
    public void tick() {
        // nothing to do here
    }

}
