package gameObjects.Items;

import java.awt.Image;

public class Armor extends DefensiveItem {

    public Armor(String name, int defense, Image image) {
        super(name, defense, image);
    }

}
