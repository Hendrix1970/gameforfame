package gameObjects.Items;

import java.awt.Image;

public class Helmet extends DefensiveItem {

    public Helmet(String name, int defense, Image image) {
        super(name, defense, image);
    }

}
