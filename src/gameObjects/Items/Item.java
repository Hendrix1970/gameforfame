package gameObjects.Items;

import java.awt.Image;

import javax.swing.ImageIcon;

import framework.AbstractGameObject;

public abstract class Item extends AbstractGameObject {

	public Item(int x, int y, int height, int width) {
		super(x, y, height, width);
	}

	private String name;
	private String description;
	private int IntellegenceModifier;
	private int DexterityModifier;
	private int StrengthModifier;
	private boolean activatable;

	Image portraitForInventar = new ImageIcon(getClass().getResource("/images/stift.png")).getImage();

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getIntellegenceModifier() {
		return IntellegenceModifier;
	}

	public void setIntellegenceModifier(int intellegenceModifier) {
		IntellegenceModifier = intellegenceModifier;
	}

	public int getDexterityModifier() {
		return DexterityModifier;
	}

	public void setDexterityModifier(int dexterityModifier) {
		DexterityModifier = dexterityModifier;
	}

	public int getStrengthModifier() {
		return StrengthModifier;
	}

	public void setStrengthModifier(int strenthModifier) {
		StrengthModifier = strenthModifier;
	}

	public boolean isActivatable() {
		return activatable;
	}

	public void setActivatable(boolean activatable) {
		this.activatable = activatable;
	}

	public Image getPortraitForInventar() {
		return portraitForInventar;
	}

	public void setPortraitForInventar(Image portraitForInventar) {
		this.portraitForInventar = portraitForInventar;
	}

	public String getDiscription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
