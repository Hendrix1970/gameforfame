package gameObjects;

import java.awt.Image;

import Rooms.Spawner;
import constants.Constants;
import framework.AbstractGameObject;

public class Table extends AbstractGameObject {
	Image table;
	static int height = 3 * Spawner.OBJECT_STANDARD_SIZE;
	static int width = 19 * Constants.unit;
	boolean onMap = true;

	public Table(int x, int y) {
		super(x, y, height, width);
	}

	public void tick() {
	}

	public boolean isOnMap() {
		return this.onMap;
	}

	public void setOnMap(boolean onMap) {
		this.onMap = onMap;
	}
}